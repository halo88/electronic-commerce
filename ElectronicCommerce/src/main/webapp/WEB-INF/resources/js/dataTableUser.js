function doDataUser(data) {
    var table = $('#tableUser').DataTable({

        responsive: true, // responsive bootstrap
        lengthMenu: [10, 20, 50, 100], // dropdown entries
        data: data, // data get on server
        dom: '<"top"f>rt<"bottom"lip><"clear">',
        // config position of search paging, entries https://datatables.net/examples/basic_init/dom.html
        pagingType: "full_numbers",
        
        // custome data for column
        //+ data: attribute name
        //+ searchable: not search with this column has searchable = false
        //+ className: as class in html ---> all: when responsive column isn't hidden.
        // + render: design data, button, handle events.
        // +orderable: not sort with this column has orderable = false
        columns: [
            {data: "email", className: 'vertical-middle'},
            {data: "gender", className: 'vertical-middle'},
            {data: "username", className: 'all vertical-middle'},            
            {data: "firstname", className: 'vertical-middle'},
            {data: "lastname", className: 'vertical-middle'},
            {data: "enabled", className: 'vertical-middle'},
            {data: "dob", className: 'vertical-middle'},
            {data: "address", className: 'vertical-middle'},
            {data: "phone", className: 'vertical-middle'},           
            {orderable: false,
                className: 'text-center all table-action-col vertical-middle',
                searchable: true,
                render: function (data, type, full, meta) {
                    var action = '<button type="button" id="editTable" data-toggle="modal" data-target="#editinfo' + full.username + '" class="btn btn-primary" ><span class="glyphicon glyphicon-pencil"></button>' +
                            '<button type="button" id="deleteTable" class="btn btn-warning"><span class="glyphicon glyphicon-trash"></button>';
                    return action;
                }
            }
        ],
        "columnDefs": [
            {
                "targets": [0],
                "visible": false,
                "searchable": false
            },
            {
                "targets": [1],
                "visible": false,
                "searchable": false
            }
        ]
    });

    $('#tableUser tbody').on('click', '#deleteTable', function () {
        var data = table.row($(this).parents('tr')).data();
        var url = window.location.pathname.substring(0, window.location.pathname.lastIndexOf('/'));
        location.href = url + '/delete-user/' + data.username;
    });
    
    $('#tableUser tbody').on('click', '#editTable', function () {
        var data = table.row($(this).parents('tr')).data();
        var url = window.location.pathname.substring(0, window.location.pathname.lastIndexOf('/'));
        location.href = url + '/edit-user/' + data.username;
    });
}