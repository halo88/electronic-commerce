function doDt(data) {
    var table = $('#tableDemo').DataTable({

        responsive: true, // responsive bootstrap
        lengthMenu: [10, 20, 50, 100], // dropdown entries
        data: data, // data get on server
        dom: '<"top"f>rt<"bottom"lip><"clear">',
        // config position of search paging, entries https://datatables.net/examples/basic_init/dom.html
        pagingType: "full_numbers",
        // custome data for column
        //+ data: attribute name
        //+ searchable: not search with this column has searchable = false
        //+ className: as class in html ---> all: when responsive column isn't hidden.
        // + render: design data, button, handle events.
        // +orderable: not sort with this column has orderable = false
        columns: [
            {data: "productID", className: 'vertical-middle'},
            {data: "description", className: 'vertical-middle'},
            {data: "productName", className: 'all vertical-middle'},
            {data: "adddate", className: 'vertical-middle'},
            {data: "unitPrice", className: 'vertical-middle'},
            {data: "warranty", className: 'vertical-middle'},
            {data: "categoryName", className: 'vertical-middle'},
            {data: "manufactureName", className: 'vertical-middle'},
            {data: "promoDesc", className: 'vertical-middle'},
            {data: "stock", className: 'vertical-middle'},
            {orderable: false,
                className: 'text-center all table-action-col vertical-middle',
                searchable: true,
                render: function (data, type, full, meta) {
                    var action = '<button type="button" id="editTable" data-toggle="modal" data-target="#editinfo' + full.productID + '" class="btn btn-primary" > Edit</button>' +
                            '<button type="button" id="deleteTable" class="btn btn-default"> Delete</button>';
                    return action;
                }
            }
        ],
        "columnDefs": [
            {
                "targets": [0],
                "visible": false,
                "searchable": false
            },
            {
                "targets": [1],
                "visible": false,
                "searchable": false
            }
        ]
    });

    $('#tableDemo tbody').on('click', '#deleteTable', function () {
        var data = table.row($(this).parents('tr')).data();
        var url = window.location.pathname.substring(0, window.location.pathname.lastIndexOf('/'));
        location.href = url + '/delete-product/' + data.productID;
    });
}