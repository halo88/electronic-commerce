<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<html>
    <head>
        <title>Spring MVC form submission</title>
    </head>

    <body>
        <h2>Fill your form!</h2>

        <form action="testvalidator2" method="post">
            <table>
                <tr>
                    <td>Enter your name:</td>
                    <td><input type="text" name="username"/></td>
                    <td><form:errors path="users.username"></form:errors></td>
                </tr>
                <tr>
                    <td>Enter your PW</td>
                    <td><input type="password" name="password"/></td>
                    <td><form:errors path="users.password"></form:errors></td>
                </tr>
                <tr>
                    <td><input type="submit" name="submit" value="Submit"></td>
                </tr>
            </table>
        </form>

    </body>
</html>