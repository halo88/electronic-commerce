
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@taglib uri="http://www.springframework.org/security/tags" prefix="sec" %>
<c:set var="contextPath" value="${pageContext.request.contextPath}"/>
<div class="panel panel-primary" style="margin-left: 50px;">
    <div class="panel-heading"><h4 class="text-center">Manage Product</h4></div>
    <div class="panel-body text-center">
        <div class="row">
            <div class="col-xs-12 col-sm-6 col-md-6" style="float: left;padding-bottom: 10px">
                <button class="btn btn-primary" onclick="location.href = '${contextPath}/seller/add-product'" name="btn"><span class="glyphicon glyphicon-plus"></span>Add New Product</button>
            </div>
            <div class="col-xs-12 col-sm-6 col-md-6" style="float: right">
                <form class="form-inline" action="search-product" method="get">
                    <div class="form-group">              
                        <div class="input-group">
                            <input name="search" id="search" type="text" class="form-control" placeholder="Search">
                        </div>
                    </div>
                    <div class="btn-group">
                        <button type="submit" class="btn btn-primary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <span class="glyphicon glyphicon-search"></span>Search <span class="caret"></span>
                        </button>
                        <ul class="dropdown-menu">
                            <li><a href="#">Search by name</a></li>
                            <li><a href="#">Search by Manufacturer</a></li>
                        </ul>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="table-responsive">
                <table class="table table-bordered">
                    <tr>
                        <th class="text-center">Product Name</th>                
                        <th class="text-center">Add Date</th>
                        <th class="text-center">Unit Price</th>
                        <th class="text-center">Warranty</th>
                        <th class="text-center">Quantity</th>
                        <th class="text-center">Manufacturer</th>
                        <th class="text-center">Promotion</th>
                        <th class="text-center">Action</th>
                    </tr>
                    <c:forEach var="each" items="${listProduct}">
                        <form:form action="edit-product" method="get">
                            <tr>
                                <td><span class="glyphicon glyphicon-tags"></span> ${each.productName}</td>                        
                                <td>${each.adddate}</td>
                                <td>${each.unitPrice}</td>
                                <td>${each.warranty}</td>
                                <td>${each.stock}</td>
                                <td>${each.manufacture.manufactureName}</td>
                                <td>${each.promotions.promoName}</td>
                                <td><button class="btn btn-primary" type="submit" value="edit" name="btn"><span class="glyphicon glyphicon-pencil"></span></button>
                                    <button class="btn btn-warning" type="submit" value="remove" name="btn"><span class="glyphicon glyphicon-trash"></span></button></td>
                            </tr>

                        </form:form>

                    </c:forEach>
                </table>
            </div>
        </div>
    </div>
</div>
