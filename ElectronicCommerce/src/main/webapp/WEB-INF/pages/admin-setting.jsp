<%-- 
    Document   : admin-setting
    Created on : May 14, 2017, 5:33:32 PM
    Author     : hell_lock
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@taglib uri="http://www.springframework.org/security/tags" prefix="sec" %>
<div class="row">
    <div class="col-xs-12 col-sm-12  col-md-12 col-lg-12 ">
        <h2 class="page-header" style="text-align: center;">Account overview</h2>
    </div>
</div>
<div class="row" style="padding-top: 50px" >
    <div class=" col-xs-12 col-sm-12 col-md-offset-3 col-md-7 col-lg-7"   >
        <div class="panel panel-info">
            <div class="panel-heading">
                <label>Personal Information</label>
                <!--<span class="pull-right"><a data-toggle="modal" href="#info">Edit</a></span>-->
            </div>
            <div class="panel-body" style="padding-left: 20px">
                <div class="table-responsive">
                    <table class="table">
                        <tr>
                            <td><label>Username: </label></td>
                            <td>${user.username}</td>
                        </tr>
                        <tr>
                            <td style="width:150px"><label><label>Full name:</label> </label></td>
                            <td>${user.userDetails.firstname}  ${user.userDetails.lastname}</td>
                        </tr>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-xs-12 col-sm-12 col-md-offset-3 col-md-7 col-lg-7">
        <div class="panel panel-info"> 
            <div class="panel-heading" >
                <label>Contact Address</label>
                <!--<span class="pull-right"><a href="#">Edit</a></span>-->
            </div>
            <div class="panel-body" style="padding-left: 20px">
                <div class="table-responsive">
                    <table class="table">
                        <tr>
                            <td style="width:150px"><label>Address: </label></td>
                            <td>${user.userDetails.address}</td>
                        </tr>
                        <tr>
                            <td><label>Email:</label></td>
                            <td>${user.userDetails.email}</td>
                        </tr>
                        <tr>
                            <td><label>Phone:</label></td>
                            <td>${user.userDetails.phone}</td>
                        </tr>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="row" >
    <div class="col-xs-12 col-sm-12 col-md-offset-3 col-md-7 col-lg-7"  >
        <div class="panel panel-info">
            <div class="panel-heading">
                <label>More infomation</label>
                <!--<span class="pull-right"><a href="#">Edit</a></span>-->
            </div>
            <div class="panel-body" style="padding-left: 20px">
                <div class="table-responsive" style="margin-bottom: 0px">
                    <table class="table">
                        <tr>
                            <td style="width:150px"><label>Date of birth: </label></td>
                            <td>${user.userDetails.dob}</td>
                        </tr>
                        <tr>
                            <td><label>Gender:</label></td>
                            <td>${user.userDetails.gender}</td>
                        </tr>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-sm-12 col-xs-12 col-md-1 col-lg-1 col-md-offset-10">
        <button class="btn btn-primary" 
                data-toggle="modal" data-target="#editinfo">Edit</button>
    </div>
    <div id="editinfo" class="modal fade" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <form:form class="form-horizontal" action="edit-personal" method="post">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title">Edit Personal Infomation</h4>
                    </div>
                    <div class="modal-body" style="padding-left: 100px">
                        <div class="form-group">
                            <label for="firstname" class="col-sm-2 control-label">Firstname</label>
                            <div class="col-sm-7">
                                <input type="text" name="firstname" value="${user.userDetails.firstname}" class="form-control" id="firstname" placeholder="Firstname">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="lastname" class="col-sm-2 control-label">Lastname</label>
                            <div class="col-sm-7">
                                <input type="text" name="lastname" value="${user.userDetails.lastname}" class="form-control" id="lastname" placeholder="Lastname">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="address" class="col-sm-2 control-label">Address</label>
                            <div class="col-sm-7">
                                <textarea  name="address" 
                                           class="form-control" id="address" placeholder="Address">${user.userDetails.address}</textarea>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="email" class="col-sm-2 control-label">Email</label>
                            <div class="col-sm-7">
                                <input type="email" name="email" value="${user.userDetails.email}" class="form-control" id="email" placeholder="Email">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="phone" class="col-sm-2 control-label">Phone</label>
                            <div class="col-sm-7">
                                <input type="number" name="phone" value="${user.userDetails.phone}" class="form-control" id="phone" placeholder="Phone">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="dob" class="col-sm-2 control-label">Date of birth</label>
                            <div class="col-sm-7">
                                <input type="date" name="dob" value="${user.userDetails.dob}" class="form-control" id="dob" placeholder="Date of birth">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="gender" class="col-sm-2 control-label">Gender</label>
                            <div class="col-sm-7">
                                <select name="gender"  class="form-control" id="gender" name="gender">
                                    <option value="Mr">Mr</option>
                                    <option value="Mrs">Mrs</option>
                                    <option value="Miss">Miss</option>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-primary" >Save</button>
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    </div>
                </form:form>
            </div>
        </div>
    </div>
</div>
<div class="row" style="margin-top:50px">
    <div class="col-xs-12 col-sm-12 col-md-11 col-lg-11 col-md-offset-1 col-lg-offset-1">
        <div class="panel panel-default">
            <div class="panel-heading">
                <label>My Recent Orders</label>
            </div>
            <div class="panel-body">
                <div class="table-responsive">
                    <table class="table">
                        <tr>
                            <th>Order #</th>
                            <th>Placed on</th>
                            <th>Total</th>
                            <th>Status</th>
                            <th></th>
                        </tr>

                        <c:forEach var="each" items="${listOrders}">
                            <tr>
                                <td>${each.orderID}</td>
                                <td>${each.orderdate}</td>
                                <td>$${each.totalprice}</td>
                                <td>${each.status}</td>
                                <td><button class="bth btn-primary" onclick="doShow(${each.orderID});"
                                            data-toggle="modal"
                                            data-target="#vieworder${each.orderID}" >View details</button></td>
                            </tr>
                        </c:forEach>
                    </table>
                </div>
                <c:forEach var="each" items="${listOrders}">
                    <div id="vieworder${each.orderID}" class="modal fade" role="dialog">
                        <div class="modal-dialog">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                                    <div class="modal-title">
                                        <h4>Details for order #${each.orderID}</h4>
                                    </div>
                                    <div class="modal-title">
                                        <strong><h4>Placed on ${each.orderdate}</h4></strong>
                                    </div>
                                </div>
                                <div class="modal-body">
                                    <div class="table-responsive" id="ajaxtable${each.orderID}">
                                    </div>
                                </div>
                                <div class="modal-footer">
                                    <button class="btn btn-default" data-dismiss="modal">Close</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </c:forEach>
            </div>
        </div>
    </div>
</div>
<script>
    function doShow(id) {
        $.ajax({
            dataType: "html",
            type: "GET",
            url: '${pageContext.request.contextPath}/view-order?id=' + id,
            timeout: 100000,
            success: function (data) {
                $('#ajaxtable'+id).html(data);
            },
            error: function (e) {
                alert('Error: ' + e.message);
            }
        })
    }
</script>
