<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<c:set var="contextPath" value="${pageContext.request.contextPath}"/>
<div class="row">
    <div class="col-xs-12 col-sm-12 col-md-12" style="text-align: center">
        <h1>Manage Order</h1>
    </div>
</div>
<div class="row">
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
        <table  class="row-border hover display table table-bordered responsive" width="100%" cellspacing="0">

            <tr>
                <th class="text-center">Username</th>        
                <th class="text-center">Order Date</th>                
                <th class="text-center">Product Name</th>
                <th class="text-center">Quantity</th>
                <th class="text-center">Total Price</th>
                <th class="text-center">Image</th>                        
                <th class="text-center">Action</th>
            </tr>
            <c:forEach var="o" items="${list}">
                <tr>
                    <td class="text-center">${o.orders.users.username}</td>
                    <td class="text-center">${o.orders.orderdate}</td>                    
                    <td class="text-center">${o.products.productName}</td>
                    <td class="text-center">${o.quantity}</td>
                    <td class="text-center"> ${o.orders.totalprice}</td>
                    <td class="text-center"><img class="group list-group-image " src="<c:url value="/resources/images/ImageHost/${o.products.imageurl}"/>" alt="" width="80"  height="80"/></td>
                    <td class="text-center"> <button class="btn btn-warning" type="button" onclick="location.href ='${contextPath}/seller/removeorder/${o.id}'" ><span class="glyphicon glyphicon-trash"></span></button></td>
                </tr>

            </c:forEach>
        </table>
    </div>
</div>
