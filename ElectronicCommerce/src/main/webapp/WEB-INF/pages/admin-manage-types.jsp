<%-- 
    Document   : admin-manage-types
    Created on : May 19, 2017, 9:38:54 AM
    Author     : hell_lock
--%>


<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@taglib uri="http://www.springframework.org/security/tags" prefix="sec" %>

<div class="row">
    <div class="col-xs-12 col-sm-12  col-md-12 col-lg-12 ">
        <h2 class="page-header" style="text-align: center;">${title}</h2>
    </div>
</div>
<div class="row">
    <div class="col-sm-12 col-md-7 col-md-offset-2">
        <div class="table-responsive">
            <table class="table" >
                <tr>
                    <th  style="padding-left: 40px">#</th>
                    <th  style="padding-left: 100px">Name</th>
                    <th  style="padding-left: 100px">Action</th>
                </tr>
                <c:forEach var="each" items="${listTypes}" varStatus="status">
                    <tr>
                        <td style="padding-left: 40px">${status.index + 1}</td>
                        <td id="tdname${status.index + 1}" style="padding-left: 100px">${each.categoryName}</td>
                        <td style="padding-left: 100px">
                            <button id="btnid${status.index + 1}" class="btn btn-primary"
                                    data-toggle="modal" data-target="#btnedit${status.index + 1}"><span class="glyphicon glyphicon-pencil"/></button></td>
                        <td><button onclick="location.href = '${pageContext.request.contextPath}/admin/doremove-type/${each.categoryID}'" class="btn btn-warning"><span class="glyphicon glyphicon-trash"></span></button></td>
                    </tr>

                    <div id="btnedit${status.index + 1}" class="modal fade" role="dialog">
                        <div class="modal-dialog modal-sm">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                                    <h4 class="modal-title">Edit</h4>
                                </div>
                                <div class="modal-body">
                                    <div class="row">
                                        <div class="form-group">
                                            <label for="catname${status.index + 1}" class="col-sm-2 control-label">Name</label>
                                            <div class="col-sm-10">
                                                <input type="text" class="form-control" id="catname${status.index + 1}" value="${each.categoryName}"/>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="modal-footer">
                                    <button onclick="saveType(${each.categoryID}, ${status.index + 1})" data-dismiss="modal" type="button" class="btn btn-success">Save</button>
                                    <button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </c:forEach>
                <script>
                    function saveType(id, statusindex) {
                        var name = $('#catname' + statusindex).val();
                        var typeObj = {
                            "categoryID": id,
                            "categoryName": name
                        }
                        $.ajax({
                            dataType: "html",
                            contentType: 'application/json',
                            type: "POST",
                            url: '${pageContext.request.contextPath}/admin/edit-type',
                            data: JSON.stringify(typeObj),
                            timeout: 10000000,
                            success: function (data) {
                                $('#tdname' + statusindex).html(data);
                            },
                            error: function (e) {
                                alert('Error: ' + e.message);
                            }
                        })
                    }
                </script>
            </table>
        </div>
    </div>
</div><div class="row">
    <div class="col-sm-9 col-md-6 col-md-offset-2">
        <h3>Insert new category:</h3>
    </div>
</div>
<div class="row form-group">
    <div class="col-sm-9 col-md-6 col-md-offset-2">
        <form:form action="edit-types/insert-type" method="post" >
            <label for="categoryName">Category Name:</label>
            <input type="text" name="categoryName" id="categoryName"/>
            <button class="btn btn-primary" type="submit">Insert</button>
        </form:form>
    </div>
</div>