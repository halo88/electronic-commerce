<%-- 
    Document   : add-to-card
    Created on : Jun 2, 2017, 3:03:09 PM
    Author     : PC
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<c:set var="contextPath" value="${pageContext.request.contextPath}"/>
<c:set var="username" value="${pageContext.request.userPrincipal.name}"/>


<div class="container" >
    <div class="row">
        <div class="col-xs-12 col-sm-10 col-md-12">
            <div class="row text-center">
                <h4><strong>ORDER PRODUCTS</strong></h4>
            </div>
            <c:if test="${Size == 0}">
                <p style="color: blue" class="text-center">NO PRODUCTS!!</p>
            </c:if>
            <div class="row">
                <p><strong>User name:</strong> ${username}</p>

                <p><strong>Date order:</strong> ${time}</p>
                <input name="orderdate" value="${time}" type="hidden">
            </div>
            <div class="row">
                <div class="table-responsive">
                    <table class="table table-bordered">
                        <tr>

                            <th class="text-center">Image</th>
                            <th class="text-center">Name</th>
                            <th class="text-center">Quantity</th>
                            <th class="text-center">Price</th>
                            <th class="text-center">Action</th>                            
                        </tr>
                        <c:if test="${Size > 0}">
                            <c:forEach var="pro" items="${cartArr}">
                                <tr>
                                <input type="hidden" name="proid" value="${pro.id}">
                                <td class="text-center"><img class="group list-group-image " src="<c:url value="/resources/images/ImageHost/${pro.imageUrl}"/>" alt="" width="80"  height="80"/></td>
                                <td class="text-center">${pro.name}</td>
                                <td class="text-center">
                                    <form action="${contextPath}/updatequantity/${pro.id}">
                                        <input type="number" name="quantity" min="1" value="${pro.quantity}">
                                        <input type="submit" class="btn btn-primary" value="Update" >
                                    </form>
                                </td>
                                <td class="text-center">${pro.price*pro.quantity}</td>
                                <td class="text-center">

                                    <button class="btn btn-warning" type="submit" onclick="location.href = '${contextPath}/remove/${pro.id}'" ><span class="glyphicon glyphicon-trash"></span></button></td>
                                </td>
                                </tr>
                            </c:forEach>
                        </c:if>
                    </table>
                </div>
            </div>

            <div class="row">
                <p style="float: right; margin-right: 20px;"><strong>Total price:</strong> ${pricett}</p>

            </div>
            <div class="row "  style="float: right">
                <form action="${contextPath}/checkout">
                    <c:if test="${Size > 0}">
                        <input name="username" value="${username}" type="hidden">
                        <input name="totalprice" value="${pricett}" type="hidden">
                        <button style="float: right; "  class="btn btn-warning" type="submit"><span   class="glyphicon glyphicon-ok-circle">Checkout</span></button>
                    </c:if>
                    <c:if test="${Size == 0}">
                        <button style="float: right; " id="myBtn" class="btn btn-warning" type="button"><span   class="glyphicon glyphicon-ok-circle">Checkout</span></button>
                    </c:if>
                    <button style="float: right ; margin-right: 5px;" class="btn btn-success" type="button" onclick="location.href = '${contextPath}/home'" name="btn"><span class="glyphicon glyphicon-shopping-cart">Order</span></button>
                </form>

            </div>
        </div>
    </div>
</div>

<div id="myModal" class="modal">   
    <div class="modal-content">
        <span class="close">&times;</span>
        <h4 style="color: blue" class="text-center">Please order the product!!!</h4>
    </div>
</div>

<script>
    // Get the modal
    var modal = document.getElementById('myModal');

    // Get the button that opens the modal
    var btn = document.getElementById("myBtn");

    // Get the <span> element that closes the modal
    var span = document.getElementsByClassName("close")[0];

    // When the user clicks the button, open the modal 
    btn.onclick = function () {
        modal.style.display = "block";
    }

    // When the user clicks on <span> (x), close the modal
    span.onclick = function () {
        modal.style.display = "none";
    }

    // When the user clicks anywhere outside of the modal, close it
    window.onclick = function (event) {
        if (event.target == modal) {
            modal.style.display = "none";
        }
    }
</script>
