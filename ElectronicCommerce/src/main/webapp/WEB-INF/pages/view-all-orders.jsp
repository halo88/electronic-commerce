<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@taglib uri="http://www.springframework.org/security/tags" prefix="sec" %>
<sec:authorize access="hasRole('ROLE_ADMIN')">
    <c:set var="role" value="admin"/>
</sec:authorize>
<sec:authorize access="hasRole('ROLE_SELLER')">
    <c:set var="role" value="seller"/>
</sec:authorize>
<sec:authorize access="hasRole('ROLE_USER')">
    <c:set var="role" value="user"/>
</sec:authorize>
<div class="row">
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
        <h2 class="page-header" style="text-align: center;">View all orders</h2>
    </div>
</div>
<c:if test="${param.mess != null}">
    <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-5 col-lg-5 col-md-offset- col-lg-offset-1">
            <div class="form-signin alert alert-success" role="alert">${param.mess}</div>
        </div>
    </div>
</c:if>
<c:if test="${param.err != null}">
    <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-5 col-lg-5 col-md-offset- col-lg-offset-1">
            <div class="form-signin alert alert-danger" role="alert">${param.err}</div>
        </div>
    </div>
</c:if>
<div class="row">
    <div class="col-xs-12 col-sm-12 col-md-11 col-lg-11 col-md-offset-1 col-lg-offset-1">
        <div class="panel-body">
            <div class="table-responsive">
                <table class="table">
                    <tr>
                        <th>Order #</th>
                        <th>Placed on</th>
                        <th>Total</th>
                        <th>Status</th>
                        <th></th>
                    </tr>

                    <c:forEach var="each" items="${listOrders}">
                        <tr>
                            <td>${each.orderID}</td>
                            <td>${each.orderdate}</td>
                            <td>$${each.totalprice}</td>
                            <td>${each.status}</td>
                            <td><button class="btn-primary" onclick="doShow(${each.orderID});"
                                        data-toggle="modal"
                                        data-target="#vieworder${each.orderID}" >View details</button>
                                <c:if test="${each.isCancelable()}">
                                    <button class="btn-danger" 
                                            onclick="location.href = '${pageContext.request.contextPath}/cancel-order/${each.orderID}?role=${role}';"
                                            >Cancel</button>
                                </c:if>
                            </td>

                        </tr>
                    </c:forEach>
                </table>
            </div>
            <c:forEach var="each" items="${listOrders}">
                <div id="vieworder${each.orderID}" class="modal fade" role="dialog">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                                <div class="modal-title">
                                    <h4>Details for order #${each.orderID}</h4>
                                </div>
                                <div class="modal-title">
                                    <strong><h4>Placed on ${each.orderdate}</h4></strong>
                                </div>
                            </div>
                            <div class="modal-body">
                                <div class="table-responsive" id="ajaxtable${each.orderID}">
                                </div>
                            </div>
                            <div class="modal-footer">
                                <button class="btn btn-default" data-dismiss="modal">Close</button>
                            </div>
                        </div>
                    </div>
                </div>
            </c:forEach>
        </div>
    </div>
</div>
<script>
    function doShow(id) {
        $.ajax({
            dataType: "html",
            type: "GET",
            url: '${pageContext.request.contextPath}/view-order?id=' + id,
            timeout: 100000,
            success: function (data) {
                $('#ajaxtable' + id).html(data);
            },
            error: function (e) {
                alert('Error: ' + e.message);
            }
        })
    }
</script>
