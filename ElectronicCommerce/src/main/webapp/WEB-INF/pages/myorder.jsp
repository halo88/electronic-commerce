<%-- 
    Document   : add-to-card
    Created on : Jun 2, 2017, 3:03:09 PM
    Author     : PC
--%>

<%@page import="java.util.Date"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<c:set var="contextPath" value="${pageContext.request.contextPath}"/>
<c:set var="username" value="${pageContext.request.userPrincipal.name}"/>
<c:if test="${pageContext.request.userPrincipal.name == null}">
    <c:set var="username" value="Guest"/>
</c:if>

<div class="container" >
    <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="row text-center">
                <h4><strong>ORDER PRODUCTS</strong></h4>
            </div>
            <c:if test="${sessionScope.cart.lstOrderItems.size() == 0}">
                <p style="color: blue" class="text-center">NO PRODUCTS!!</p>
            </c:if>
            <div class="row">
                <p><strong>User: </strong> ${username}</p>

                <p><strong>Date order:</strong> ${sessionScope.cart.orderdate}</p>
                <input name="orderdate" value="${time}" type="hidden">
            </div>
            <div class="row">
                <div class="table-responsive">
                    <table class="table table-bordered">
                        <tr>

                            <th class="text-center">Image</th>
                            <th class="text-center">Name</th>
                            <th class="text-center">Quantity</th>
                            <th class="text-center">Price</th>
                            <th class="text-center">Action</th>                            
                        </tr>
                        <c:if test="${sessionScope.cart.lstOrderItems.size() > 0}">
                            <c:forEach varStatus="status" var="each" items="${sessionScope.cart.lstOrderItems}">
                                <tr>
                                    <td class="text-center"><img class="group list-group-image " src="<c:url value="/resources/images/ImageHost/${each.products.imageurl}"/>" alt="" width="80"  height="80"/></td>
                                    <td class="text-center">${each.products.productName}</td>
                                    <td class="text-center">${each.quantity}</td>
                                    <td class="text-center">$${each.currentPrice * each.quantity}</td>
                                    <td class="text-center">
                                        <a href="${pageContext.request.contextPath}/remove/${each.products.productID}" class="btn btn-warning"><span class="glyphicon glyphicon-trash"></span></a>
                                    </td>
                                </tr>
                            </c:forEach>
                        </c:if>
                    </table>
                </div>
            </div>
            <div class="row">
                <p style="float: right; margin-right: 20px;"><strong>Total price:</strong> $ ${sessionScope.cart.totalprice}</p>
            </div>
            <div class="row">
                <div style="float: right; margin-right: 20px;margin-top:20px" >
                    <a class="btn btn-success" href="${pageContext.request.contextPath}/checkout">Checkout</a>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    function updatePrice(statusindex) {
        $('#quantity' + statusindex).change(function () {

        })
    }
</script>