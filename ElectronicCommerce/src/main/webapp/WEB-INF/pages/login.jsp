<%-- 
    Document   : login
    Created on : May 10, 2017, 3:44:57 PM
    Author     : hell_lock
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@taglib uri="http://www.springframework.org/security/tags" prefix="sec" %>
<jsp:include page="include/headerimport.jsp" flush="true"/> 
<!DOCTYPE html>
<html>
    <head>
        <title>Login Page</title>
        <link rel="stylesheet" href="<c:url value='/resources/css/loginpage.css'/>"/>
    </head>
    <body>
        <jsp:include page="include/navbar.jsp" flush="true"/>
        <div class="container-fluid" style="margin-top: 100px">
            <c:if test="${param.mess != null}">
                <div class="row">
                    <div class="form-signin alert alert-success" role="alert">${param.mess}</div>
                </div>
            </c:if>
            <c:if test="${param.error != null}">
                <div class="row">
                    <div class="form-signin alert alert-danger" role="alert">${param.error}</div>
                </div>
            </c:if>
            <div class="row">
                <form class="form-signin" action="j_spring_security_check" method="post">
                    <h2 class="form-signin-heading">Login</h2>
                    <label for="Username" class="sr-only">Username</label>
                    <input style="margin-bottom: 10px;margin-top: 15px" type="text" name="username" id="Username" class="form-control" placeholder="Username" required autofocus>
                    <label for="Password" class="sr-only">Password</label>
                    <input type="password" name="password" id="Password" class="form-control" placeholder="Password" required>
                    <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>
                    <button class="btn btn-lg btn-primary btn-block" type="submit">Login</button>
                </form>
            </div><br>
            <div class="row">
                <div class="" style="text-align: center;margin-bottom:10px">
                    <a href="forgot-pass">Forgot Password<br> Resend Confimation Email</a>
                </div>
            </div>
            <div class="row">
                <div class="col-md-offset-5">
                    <label>Don't have an account?</label>
                    <a href="register">Register</a>
                </div>
            </div>
        </div>

    </body>
</html>
