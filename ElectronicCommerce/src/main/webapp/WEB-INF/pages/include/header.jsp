<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<%@taglib uri="http://www.springframework.org/security/tags" prefix="scr" %>



<div class="row" style="padding-bottom: 20px">
    <c:set var="contextPath" value="${pageContext.request.contextPath}"/>
    <div class="col-xs-12 col-sm-12 col-md-12">
        <scr:authorize access="hasRole('ROLE_SELLER')">
            <button type="button" class="btn btn-default"
                    onclick="location.href = '${contextPath}/seller/add-product'">Add product</button>
        </scr:authorize>
        <!--<a href="<c:url value="/logout"/>" style="float: right">Logout</a>-->
    </div>
</div>
