<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<script src="<c:url value="/resources/js/jquery.min.js"/>"></script>
<link rel="stylesheet" href="<c:url value='/webjars/bootstrap/3.3.7/css/bootstrap.css'/>"/>
<script src="<c:url value="/webjars/bootstrap/3.3.7/js/bootstrap.js"/>"></script>