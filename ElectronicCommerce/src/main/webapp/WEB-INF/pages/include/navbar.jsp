<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@taglib uri="http://www.springframework.org/security/tags" prefix="sec" %>
<c:set var="contextPath" value="${pageContext.request.contextPath}"/>
<c:set var="username" value="${pageContext.request.userPrincipal.name}"/>
<sec:authorize access="hasRole('ROLE_ADMIN')">
    <c:set var="role" value="admin"/>
</sec:authorize>
<sec:authorize access="hasRole('ROLE_SELLER')">
    <c:set var="role" value="seller"/>
</sec:authorize>
<sec:authorize access="hasRole('ROLE_USER')">
    <c:set var="role" value="user"/>
</sec:authorize>

<nav class="navbar navbar-inverse navbar-fixed-top ">
    <div class="container-fluid">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="${contextPath}/home">ECommerce</a>
        </div>
        <div id="navbar" class="navbar-collapse collapse">
            <ul class="nav navbar-nav navbar-right">
                <li  class="dropdown"><a class="dropdown-toggle" data-toggle="dropdown">
                        <span style="font-size: 16px" class="glyphicon glyphicon-shopping-cart"></span>
                        <span id="cartsize" class="badge">${sessionScope.cart.lstOrderItems.size()}</span>
                    </a>
                    <ul id="dropdowncart"  class="dropdown-menu list-group">
                        <c:forEach var="each" items="${sessionScope.cart.lstOrderItems}">
                            <li><a>${each.products.productName}&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp
                                    <span class='badge' style="position: absolute;right:5px">${each.quantity}</span></a></li>
                        </c:forEach>
<!--                        <span id="dropdowncart"></span>-->
                        <hr>
                        <li style=""><a style="padding-bottom:10px;padding-left:28%;color: #428bca;font-weight: bolder" href="${contextPath}/viewcart">Manage cart</a></li>
                    </ul>
                </li>
                <li class="dropdown">
                    <sec:authorize access="isAuthenticated()">
                        <a href="${contextPath}/${role}/setting" class="dropdown-toggle" 
                           data-toggle="dropdown" role="button" 
                           aria-haspopup="true" 
                           aria-expanded="false">Hello ${username} (${role.toUpperCase()}) <span class="caret"></span>
                        </a>
                    </sec:authorize>
                    <ul class="dropdown-menu list-group">
                        <li><a href="${contextPath}/${role}/setting">My profile</a></li>
                        <li><a href="${contextPath}/${role}/orders">View all orders</a></li>
                        <li role="separator" class="divider"></li>
                            <sec:authorize access="hasRole('ROLE_ADMIN')">
                            <li style="padding:12px;padding-left:20px;color:#428bca;font-size: 14px "><span class="glyphicon glyphicon-cog"></span> ADMIN SECTION</li>
                            <li><a href="${contextPath}/${role}/manage-users">Manage users</a></li>
                            <li><a href="${contextPath}/${role}/manage-cate">Manage categories</a></li>
                            <li><a href="${contextPath}/${role}/manage-manufactures">Manage manufactures</a></li>
                            </sec:authorize>
                            <sec:authorize access="hasAnyRole('ROLE_SELLER','ROLE_ADMIN')">
                            <li style="padding:12px;padding-left:20px;color:#428bca;font-size: 14px "><span class="glyphicon glyphicon-cog"></span> SELLER SECTION</li>
                            <li><a href="${contextPath}/seller/manage-order">Manage Order</a></li>
                            <li><a href="${contextPath}/${role}/manage-products">Manage Product</a></li>
                            </sec:authorize>
                    </ul>
                </li>
                <sec:authorize access="isAnonymous()">
                    <li><a href="${contextPath}/login">Login</a></li>
                    </sec:authorize>
                    <sec:authorize access="isAuthenticated()">
                    <li><a href="${contextPath}/logout">Logout</a></li>
                    </sec:authorize>
                    <sec:authorize access="isAnonymous()">
                    <li><a href="${contextPath}/register">Register</a></li>
                    </sec:authorize>    
            </ul>
            <c:if test="${hideSearch == null}">
                <form class="navbar-form navbar-left" action="${contextPath}/search-result/1/${sessionScope.sort}" method="get">
                    <input type="text" name="search" required class="form-control" placeholder="Search products..." style="margin-left:200px;width:450px">
                    <button type="submit" class="btn btn-default"><span class="glyphicon glyphicon-search"></span></button>
                </form>
            </c:if>
        </div>
    </div>
</nav>