<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<link rel="stylesheet" href="<c:url value="/resources/css/footer.css"/>"/>
<hr/>
<footer class="footer">
    <div class="container-fluid" style="padding-top: 30px">
        <div class="row">
            <div class="col-xs-offset-4 col-md-offset-6 col-xs-4 col-sm-4 col-md-3 col-lg-3">
                <ul class="list-unstyled">
                    <li class="contact"><a href="#">USER GUIDE</a></li>
                    <li><a href="#">How to place an order</a></li>
                    <li><a href="#">How to receive goods</a></li>
                    <li><label>Feedback to our CEO via abcxyz@ecommerce.com</label></li>
                </ul>
            </div>
            <div class="col-xs-4 col-sm-4 col-md-3 col-lg-3">
                <ul class="list-unstyled">
                    <li class="contact"><a href="#"><span>CONTACT US</span></a></li>
                    <li><label>Customer Service Center: floor, Building, 
                            xx abc Steet, District, City</label></li>
                    <li><label>Mon - Sat: 8am - 5pm</label></li>
                </ul>
            </div>
        </div>
    </div>
</footer>