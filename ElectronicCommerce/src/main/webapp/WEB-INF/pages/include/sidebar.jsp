<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@taglib uri="http://www.springframework.org/security/tags" prefix="sec" %>

<c:set var="contextPath" value="${pageContext.request.contextPath}"/>
<sec:authorize access="hasRole('ROLE_ADMIN')">
    <c:set var="role" value="ADMIN"/>
</sec:authorize>
<sec:authorize access="hasRole('ROLE_SELLER')">
    <c:set var="role" value="SELLER"/>
</sec:authorize>
<sec:authorize access="hasRole('ROLE_USER')">
    <c:set var="role" value="USER"/>
</sec:authorize>
<div class="row">
    <div class="col-xs-12 col-sm-12 col-md-12">
        <h4 style="text-align: center">${role}</h4>
    </div>
</div>

<div class="row">
    <div class="col-xs-12 col-sm-12 col-md-12" id="ul">
        <ul class="nav nav-sidebar">
            <hr>
            <li><a href="${contextPath}/${role.toLowerCase()}/setting">Overview</a></li>
            <li><a href="${contextPath}/${role.toLowerCase()}/change-profile-password">Change Password</a></li>
            <li><a href="${contextPath}/${role.toLowerCase()}/orders">View My Orders</a></li>
                <sec:authorize access="hasRole('ROLE_ADMIN')">
                <li><a href="${contextPath}/admin/manage-users">Manage Users</a></li>
                <li><a href="${contextPath}/admin/manage-manufactures">Manage Manufactures</a></li>
                <li><a href="${contextPath}/admin/manage-cate">Manage Categories</a></li>
                </sec:authorize>
                <sec:authorize access="hasAnyRole('ROLE_ADMIN','ROLE_SELLER')">
                <li <c:if test="${addproduct != null}">class="active"</c:if>><a href="${contextPath}/${role.toLowerCase()}/manage-products">Manage Products</a></li>
                </sec:authorize>

        </ul>
        <!--        <ul class="nav nav-sidebar">
                    <li><a href="">Nav item</a></li>
                    <li><a href="">Nav item again</a></li>
                    <li><a href="">One more nav</a></li>
                    <li><a href="">Another nav item</a></li>
                    <li><a href="">More navigation</a></li>
                </ul>
                <ul class="nav nav-sidebar">
                    <li><a href="">Nav item again</a></li>
                    <li><a href="">One more nav</a></li>
                    <li><a href="">Another nav item</a></li>
                </ul>-->
    </div>
</div>
<script>
    var ul = document.getElementById("ul"),
            anchor = ul.getElementsByTagName('li'),
            current = window.location.href;
    for (var i = 0; i < anchor.length; i++) {
        var hr = anchor[i].getElementsByTagName('a')[0].href;
        if (hr == current) {
            anchor[i].className = "active";
        }
    }
</script>