<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<c:set var="contextPath" value="${pageContext.request.contextPath}"/>


<div class="container">
    <div class="row text-center">
        <div class="col-md-12 col-xs-12 col-sm-12">
            <h2 style="color: green">Checkout Success!!!</h2>
            <button class="btn btn-success" onclick="location.href = '${contextPath}/home'" >
                <span class="glyphicon glyphicon-shopping-cart">Continue shopping</span></button>
        </div>
    </div>
    
</div>