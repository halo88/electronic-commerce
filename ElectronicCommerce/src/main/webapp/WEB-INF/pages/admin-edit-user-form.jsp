<%-- 
    Document   : admin-edit-user-form
    Created on : May 14, 2017, 10:25:05 AM
    Author     : hell_lock
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@taglib uri="http://www.springframework.org/security/tags" prefix="sec" %>
<c:set var="contextPath" value="${pageContext.request.contextPath}"/>

<div class="row" style="padding-top: 10px; padding-left: 200px">
    <div class="col-xs-12 col-sm-10 col-md-10 ">

        <fieldset class="scheduler-border">
            <legend class="scheduler-border">
                <c:out value="Edit User"/>
            </legend>
            <form:form action="edit-users" method="post">
                <div class="form-group">
                    <label for="username">Username:</label>
                    <strong>${user.username}</strong>
                    <!--<label>${user.username}</label>-->
                    <input type="hidden" value="${user.username}" name="username"/>
                    <input type="hidden" value="${user.username}" name="userDetails.username"/>
                    <input type="hidden" value="${user.username}" name="auth.username"/>
                </div>
                <div class="form-group">
                    
                    <input type="hidden" name="password" class="form-control" id="password" value="${user.password}">
                </div>
                <div class="form-group">
                    <label>Enabled</label>
                    <select name ="enabled" class="form-control">
                        <option value="${1}">True</option>
                        <option value="${0}">False</option>
                    </select>
                </div>
                <div class="form-group">
                    <label for="firstname">Firstname</label>
                    <input type="text" name="userDetails.firstname" class="form-control" id="firstname" value="${user.userDetails.firstname}">
                </div>
                <div class="form-group">
                    <label for="lastname">Lastname</label>
                    <input type="text" name="userDetails.lastname" class="form-control" id="lastname" value="${user.userDetails.lastname}">
                </div>
                <div class="form-group">
                    <label for="dob">Date of birth</label>
                    <input type="date" name="userDetails.dob" class="form-control" id="lastname" value="${user.userDetails.dob}">
                </div>
                <div class="form-group">
                    <label for="address">Address</label>
                    <input type="text" name="userDetails.address" class="form-control" id="lastname" value="${user.userDetails.address}">
                </div>
                <div class="form-group">
                    <label for="phone">Phone</label>
                    <input type="number" name="userDetails.phone" class="form-control" id="lastname" value="${user.userDetails.phone}">
                </div>
                <div class="form-group">
                    <label>Group Authority</label>
                    <select name ="auth.groupAuth.authorityRole" class="form-control">
                        <c:forEach var="each" items="${listGroupAuth}">
                            <option value="${each.authorityRole}">${each.authorityRole}</option>
                        </c:forEach>
                    </select>                    
                </div>   
                <div class="row" style="margin-right: 1.5px; float: right">
                    <button  type="submit" class="btn btn-success">Save</button>
                    <!--<button  type="button" class="btn btn-primary">Cancel</button>-->
                    <a href="${contextPath}/admin/manage-users" class="btn btn-primary" role="button">Cancel</a>
                </div>  
            </form:form>
        </fieldset>
    </div>
</div>