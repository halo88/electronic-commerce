

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@taglib uri="http://www.springframework.org/security/tags" prefix="sec" %>
        <div class="container" style="margin-top: 100px; width: 35%">
            <div class="panel panel-info text-center">
                <div class="panel-heading"><h4 >Enter your email</h4></div>
                <div class="panel-body">
                    <div class="row text-center" style="width: 90%; margin-left: 18px">
                        <form action="forgot-pass" method="post">
                            <h3></h3>
                            <input type="email" name="email" class="form-control" required>
                            <button style="margin-top: 20px;margin-bottom:20px" class="btn btn-lg btn-info btn-block" type="submit">Send</button>
                        </form>
                    </div>
                    <div class="row text-center">
                        <a href="login">Login with another account</a>
                    </div>
                </div>
            </div>
        </div>
