<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@taglib uri="http://www.springframework.org/security/tags" prefix="sec" %>
<c:set var="contextPath" value="${pageContext.request.contextPath}"/>
<script src="<c:url value="/resources/js/jquery.min.js"/>"></script>
<script>
    function dosub(){
        var amount = $('#amount')[0].value;
        var data = {
            "amount": amount,
            "productID": '${pro.productID}'
        };
        $.ajax({
            type: 'post',
            dataType: 'html',
            contentType: 'application/json',
            url: '${pageContext.request.contextPath}/add-to-cart',
            data: JSON.stringify(data),
            success: function (data) {
                $('#dropdowncart').html(data);
            },
            error: function (error) {
                alert('error');
                console.log(error);
            }
        });
    }
</script>

<div class="container">
    <p><a href="${contextPath}/home">Home</a> > <a href="#">Laptop</a> ></p>
    <br> 
    <div class="row">
        <div class="col-xs-12 col-sm-4 thumbnail">
            <c:if test="${pro.isSaling()}">
                <img class=" promotion_icon" src="<c:url value="/resources/images/ImageHost/onsale.png"/>" />
            </c:if>
            <img style="margin-top: 40px;" class="img-responsive img-rounded" src="<c:url value="/resources/images/ImageHost/${pro.imageurl}"/>"/>
        </div>
        <div class="col-xs-12 col-sm-7" style="margin-left: 10px;">
            <div class="row">
                <h4 style="color: blue">${pro.productName}</h4><br>
                <p><h4><strong> Price: </strong> $${pro.unitPrice}</h4></p>
            </div>
            <br>
            <div class="row">
                <div class="col-xs-6 col-sm-6 col-md-3 col-lg-3">
                    <input id="amount" class="form-control" type="text" value="1" pattern="[0-9]+" title="Enter a valid number" required/>
                </div>
                <div class="col-xs-6 col-sm-6 col-md-3 col-lg-3">
                    <button onclick="dosub();" id="submit" class="btn btn-success">Add to cart</button>
                </div>
            </div>
            <br>
            <strong><h4>Product infomation:</h4></strong>
            <h5>${pro.description}</h5>
            <br>

            <strong><h4 style="color: blue" >Warranty:${pro.warranty}</h4></strong>
            <h5></h5>
        </div>
    </div>

    <div class="row">
        <h4> INFORMATION ABOUT PRODUCT:</h4>
        <table class="table">
            <tr>
                <td> CPU</td>
                <td> ${pro.desc.cpu}</td>
            </tr>        
            <tr>
                <td>RAM</td>                    
                <td> ${pro.desc.ram}</td>
            </tr>
            <tr>
                <td>CHIPSET</td>                    
                <td> ${pro.desc.chipset}</td>
            </tr>
            <tr>
                <td>HDD</td>                
                <td> ${pro.desc.hdd}</td>
            </tr>
            <tr>
                <td>OPTICAL</td>                   
                <td> ${pro.desc.optical}</td>
            </tr>
            <tr>
                <td>DISPLAY</td>                   
                <td> ${pro.desc.display}</td>
            </tr>
            <tr>
                <td>GARAPHIC</td>               
                <td> ${pro.desc.graphic}</td>
            </tr>
            <tr>
                <td>NETWORK</td>                   
                <td> ${pro.desc.network}</td>
            </tr>                
            <tr>
                <td>WEIGHT</td>              
                <td> ${pro.desc.weight}</td>
            </tr>                
            <tr>
                <td>ORTHER</td>          
                <td> ${pro.desc.other}</td>
            </tr>
            <tr>
                <td></td>
                <td></td>
            </tr>

        </table>

    </div>


    <!--<div id="products" class="row list-group">-->
    <div class="row">
        <h4>SIMILAR PRODUCT:</h4>
        <c:forEach var="each" items="${list}">

            <div class="item  col-xs-12 col-sm-3 col-md-3 ">
                <div class="thumbnail">
                    <a href="${pageContext.request.contextPath}/detailproduct/${each.productID}">
                        <c:if test="${each.isSaling()}">
                            <img style="margin-left:15px"  class=" promotion_icon" src="<c:url value="/resources/images/ImageHost/onsale.png"/>" />
                        </c:if>
                        <img class="group list-group-image" src="<c:url value="/resources/images/ImageHost/${each.imageurl}"/>" alt="" width="150"  height="150"/>
                    </a>
                    <div class="caption">
                        <a href="${pageContext.request.contextPath}/detailproduct/${each.productID}">
                            <strong><p class="group inner list-group-item-heading">
                                    ${each.productName}</p></strong>
                        </a>
                        <p class="group inner list-group-item-text">
                            <!--${each.description}-->
                        </p>
                        <div class="row">
                            <div class="col-xs-12 col-sm-6 col-md-6">
                                <p>$ ${each.unitPrice}</p>
                            </div>
                            <div class="col-xs-12 col-sm-5 col-md-6">
                                <a class="btn btn-success" href="${pageContext.request.contextPath}/add-to-cart/${each.productID}">Add to cart</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>


        </c:forEach>
    </div>

</div>