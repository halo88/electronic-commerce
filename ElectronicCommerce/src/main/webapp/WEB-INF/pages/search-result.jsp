<%-- 
    Document   : search-result
    Created on : Jun 6, 2017, 11:18:19 PM
    Author     : hell_lock
--%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@taglib uri="http://www.springframework.org/security/tags" prefix="sec" %>

<link rel="stylesheet" href="<c:url value="/resources/css/productpage.css"/>"/>
<link rel="stylesheet" href="<c:url value="/resources/css/homethumbnail.css"/>"/>
<script src="<c:url value="/resources/js/productpage.js"/>"></script>
<div class="row">
    <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
        <h4>Search result:</h4>
    </div>
</div>

<div class="row well well-sm">
    <div class=" col-xs-6 col-sm-6 col-md-3 col-lg-3">
        <strong>View Mode</strong>
        <div class="btn-group">
            <a href="#" id="list" class="btn btn-default btn-sm"><span class="glyphicon glyphicon-th-list">
                </span>List</a> <a href="#" id="grid" class="btn btn-default btn-sm"><span
                    class="glyphicon glyphicon-th"></span>Grid</a>
        </div>
    </div>
    <div class=" col-xs-6 col-sm-6 col-md-1 col-lg-1 col-md-offset-8">
        <div class="dropdown">
            <a href="#" class="dropdown-toggle" 
               data-toggle="dropdown" role="button" 
               aria-haspopup="true" 
               aria-expanded="false">Sort by<span class="caret"></span>
            </a>
            <ul class="dropdown-menu pull-right list-group">
                <li><a href="${pageContext.request.contextPath}/search-result/1/productName">Name</a></li>
                <li><a href="${pageContext.request.contextPath}/search-result/1/adddate">Add Date</a></li>
                <li><a href="${pageContext.request.contextPath}/search-result/1/categories">Category</a></li>
                <li><a href="${pageContext.request.contextPath}/search-result/1/manufacture">Manufacture</a></li>
                <li><a href="${pageContext.request.contextPath}/search-result/1/unitPrice">Price</a></li>
            </ul>
        </div>
    </div>
</div>
<div id="products" class="flex-row  row list-group">
    <c:forEach var="each" items="${listProduct}">
        <div class="item  col-xs-6 col-sm-6 col-md-3 col-lg-3 ">
            <div class="thumbnail">
                <a href="${pageContext.request.contextPath}/detailproduct/${each.productID}">
                    <c:if test="${each.isSaling()}">
                    <img class=" promotion_icon" src="<c:url value="/resources/images/ImageHost/onsale.png"/>" />
                    </c:if>
                    <img class="group list-group-image" src="<c:url value="/resources/images/ImageHost/${each.imageurl}"/>" alt="" width="150"  height="150"/>
                </a>
                <div class="caption" >
                    <a href="${pageContext.request.contextPath}/detailproduct/${each.productID}">
                        <strong> <p class="group inner list-group-item-heading">
                                ${each.productName}</p></strong></a>
                    <p class="group inner list-group-item-text">
                        <strong>CPU:</strong> ${each.desc.cpu}<br>
                        <strong>Chipset:</strong> ${each.desc.chipset}<br>
                        <strong>RAM:</strong> ${each.desc.ram}<br>
                        <strong>HDD:</strong> ${each.desc.hdd}<br>
                        <strong>Display:</strong> ${each.desc.display}
                    </p>
                    <div class="row bottom">
                        <div class="col-xs-12 col-md-6">
                            <p class="lead">$ ${each.unitPrice}</p>
                        </div>
                        <div class="col-xs-12 col-md-6">
                            <a class="btn btn-success" href="${pageContext.request.contextPath}/add-to-car/${each.productID}">Add to cart</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </c:forEach>
</div>
<script src="<c:url value="/resources/js/jquery-ui.min.js"/>"></script>
<script src="<c:url value="/resources/js/homepagination.js"/>"></script>
<link rel="stylesheet" href="<c:url value="/resources/css/homepagination.css"/>"/>
<script>
    $(document).ready(function () {
        var itemsCount = ${totalItems};
        var itemsOnPage = 8;
        var pagination = new Pagination({
            container: $("#pagination"),
            pageClickUrl: "${pageContext.request.contextPath}/search-result/{{page}}/${sessionScope.sort}",
                        showInput: true,
                        maxVisibleElements: 1,
                        inputTitle: "Go to page"
                    });
                    pagination.make(itemsCount, itemsOnPage, ${page});
                });
</script>      
<div class="row">
    <div class="col-xs-12 col-sm-12 col-md-3 col-md-offset-9">
        <div id="pagination"></div>
    </div>
</div>
