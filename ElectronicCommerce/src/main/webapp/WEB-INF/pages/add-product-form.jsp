<%-- 
    Document   : seller-updateForm
    Created on : May 19, 2017, 11:12:04 PM
    Author     : PC
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<script src="<c:url value="/resources/js/jquery-ui.min.js"/>"></script>
<link rel="stylesheet" href="<c:url value="/resources/css/jquery-ui.css"/>"/>
<c:set var="contextPath" value="${pageContext.request.contextPath}"/>
<style>
    .form-horizontal .control-label{
        text-align:left;
    }
    fieldset .form-group{
        padding-left:20px;
    }
</style>
<div class="row">
    <div class="col-xs-12 col-sm-12  col-md-12 col-lg-12 ">
        <h2 class="page-header" style="text-align: center;">Add new product</h2>
    </div>
</div>
<div class="row">
    <div class="col-xs-12 col-sm-12 col-md-12 text-center">
        <p class="color-error">${message}</p>
    </div>
</div>
<div class="row">
    <div class="col-xs-12 col-sm-12 col-md-8 col-md-offset-2 col-lg-8">
        <form:form cssClass="form-horizontal" action="${contextPath}/seller/insert-product" method="post" enctype="multipart/form-data" acceptCharset="utf-8">
            <div class="row">
                <div class="form-group">
                    <label class="col-sm-3 control-label" for="productname">Product Name</label>
                    <div class="col-sm-7">
                        <input type="text" name="productName" 
                               class="form-control" id="productname">
                    </div>
                </div>
                <div class="form-group">
                    <label  class="col-sm-3 control-label" for="unitprice">Unitprice</label>
                    <div class="col-sm-7">
                        <div class="input-group">
                            <div class="input-group-addon">$</div>
                            <input type="text" name="unitPrice" 
                                   class="form-control" id="unitprice"
                                   pattern="[0-9]+\.?[0-9]*" title="Enter a number">
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <label  class="col-sm-3 control-label" for="warranty">Warranty</label>
                    <div class="col-sm-7">
                        <input type="text" name="warranty" 
                               class="form-control" id="warranty">
                    </div>
                </div>
                <div class="form-group">
                    <label  class="col-sm-3 control-label" for="stock">Quantity</label>
                    <div class="col-sm-7">
                        <input type="text" pattern="[0-9]+" title="Enter a number" name="stock" 
                               class="form-control" id="stock">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label">Category</label>
                    <div class="col-sm-7">
                        <select class="form-control" name="categories.categoryID">
                            <c:forEach var="group" items="${listCat}">
                                <option value="${group.categoryID}">${group.categoryName}</option>
                            </c:forEach>
                        </select>                            
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-sm-3 control-label">Manufacture</label>
                    <div class="col-sm-7"><select class="form-control" name="manufacture.manufactureID">
                            <c:forEach var="group" items="${lstManufactures}">
                                <option value="${group.manufactureID}">${group.manufactureName}</option>
                            </c:forEach>
                        </select>                            
                    </div>
                </div>

                <div class="form-group">
                    <label class="control-label col-sm-3">Promotion</label>
                    <div class="input-group col-sm-4" style="padding-left:15px">
                        <input type="text" name="promotions.promoDesc" value="0"
                               class="form-control" id="custom"
                               pattern="[0-9]+\.?[0-9]*" title="Enter a number">
                        <div class="input-group-addon">%</div>
                    </div>
                    <div class="row" style="margin-top:10px" id="daterange">
                        <div class="col-xs-12 col-sm-12 col-md-8 col-md-offset-2 col-lg-8">
                            <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                                <div class="form-group">
                                    <label class="control-label col-sm-4">Start</label>
                                    <div class="col-sm-8">
                                        <input id="startdate" name="promotions.startdate" type="text" class="form-control startdate" value="${each1.promotions.startdate}">
                                    </div> 
                                </div>
                            </div>
                            <div class="col-xs-12 col-sm-12 col-md-6  col-lg-6">
                                <div class="form-group">
                                    <label class="control-label col-sm-4">End</label>
                                    <div class="col-sm-8">
                                        <input id="enddate" name="promotions.enddate" type="text" class="form-control enddate" value="${each1.promotions.enddate}">
                                    </div> 
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="form-horizontal">
                    <fieldset>
                        <legend>Description</legend>
                        <div class="row" >
                            <div class="col-sm-12 col-xs-12 col-md-6 col-lg-6">
                                <div class="form-group">
                                    <label class="col-sm-3 control-label" for="cpu">CPU</label>
                                    <div class="col-sm-7">
                                        <input id="cpu" class="form-control" type="text" name="desc.cpu">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-3 control-label" for="ram">RAM</label>
                                    <div class="col-sm-7">
                                        <input id="ram" class="form-control" type="text" name="desc.ram">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-3 control-label" for="chipset">Chipset</label>
                                    <div class="col-sm-7">
                                        <input id="chipset" class="form-control" type="text" name="desc.chipset">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-3 control-label" for="hdd">HDD</label>
                                    <div class="col-sm-7">
                                        <input id="hdd" class="form-control" type="text" name="desc.hdd">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-3 control-label" for="optical">Optical</label>
                                    <div class="col-sm-7">
                                        <input id="optical" class="form-control" type="text" name="desc.optical">
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-12 col-xs-12 col-md-6 col-lg-6">
                                <div class="form-group">
                                    <label class="col-sm-3 control-label" for="display">Display</label>
                                    <div class="col-sm-7">
                                        <input id="display" class="form-control" type="text" name="desc.display">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-3 control-label" for="graphic">Graphic</label>
                                    <div class="col-sm-7">
                                        <input id="graphic" class="form-control" type="text" name="desc.graphic">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-3 control-label" for="network">Network</label>
                                    <div class="col-sm-7">
                                        <input id="network" class="form-control" type="text" name="desc.network">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-3 control-label" for="weight">Weight</label>
                                    <div class="col-sm-7">
                                        <input id="weight" class="form-control" type="text" name="desc.weight">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-3 control-label" for="other">Other</label>
                                    <div class="col-sm-7">
                                        <textarea name="desc.other" class="form-control" id="other"></textarea>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </fieldset>
                </div>
            </div>
            <div class="row">
                <div class="form-group">
                    <label class="col-sm-3 control-label" for="image">Feature Image</label>
                    <div class="col-sm-7">
                        <input name ="file" type="file" id="image">
                        <div id="image-holder"> </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <button type="submit" class="btn btn-primary col-md-offset-8" style="width:20%">ADD</button>
            </div>
        </form:form>
    </div>
</div>
<script>
    $(function () {
        var dateFormat = "yy-mm-dd",
                from = $("#startdate")
                .datepicker({
                    defaultDate: "+1w",
                    numberOfMonths: 2,
                    dateFormat: dateFormat,
                    minDate: new Date()
                })
                .on("change", function () {
                    to.datepicker("option", "minDate", getDate(this));
                }),
                to = $("#enddate").datepicker({
            defaultDate: "+1w",
            numberOfMonths: 2,
            dateFormat: dateFormat
        })
                .on("input", function () {
                    from.datepicker("option", "maxDate", getDate(this));
                });
        var desc = $('#custom')[0].value;
        if (desc == 0) {
            $('#daterange')[0].style.display = 'none'
        } else {
            $('#daterange')[0].style.display = 'block'
        }
        $('#custom').on("input", function () {
            var desc = this.value;
            if (desc == 0) {
                $('#daterange')[0].style.display = 'none'
            } else {
                $('#daterange')[0].style.display = 'block'
            }
        });
    });
    function getDate(element) {
        var date;
        var dateFormat = 'yy-mm-dd';
        try {
            date = $.datepicker.parseDate(dateFormat, element.value);
        } catch (error) {
            alert(error)
            date = null;
        }
        return date;
    }
    $("#image").on('change', function () {

        if (typeof (FileReader) != "undefined") {

            var image_holder = $("#image-holder");
            image_holder.empty();

            var reader = new FileReader();
            reader.onload = function (e) {
                $("<img />", {
                    "src": e.target.result,
                    "class": "thumb-image",
                    "width":"200",
                    "height":"200"
                }).appendTo(image_holder);

            }
            image_holder.show();
            reader.readAsDataURL($(this)[0].files[0]);
        } else {
            alert("Error showing preview image!");
        }
    });
</script>