<%-- 
    Document   : admin-view-users
    Created on : May 12, 2017, 7:05:46 PM
    Author     : hell_lock
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@taglib uri="http://www.springframework.org/security/tags" prefix="sec" %>
<!--<link rel="stylesheet" type="text/css"  href="<c:url value="/font-awesome/4.6.3/css/font-awesome.min.css"/>" >
<link rel="stylesheet" type="text/css" href="path/to/font-awesome/css/font-awesome.min.css">-->

<%--<jsp:include page="include/headerimport.jsp" flush="true"/>--%>
<div class="row">
    <div class="col-sm-12 col-xs-12 col-md-12  main">

        <fieldset class="scheduler-border">

            <legend class="scheduler-border">
                <h1 class="page-header" style="margin-top: 0;">${title}</h1>
            </legend>
            <form class="form-inline" action="search-users" method="get">
                <div class="form-group">              

                    <div class="input-group">
                        <input name="search" id="search" type="text" class="form-control" placeholder="Search">
                        <div class="input-group-btn">
                            <button class="btn btn-primary" type="submit">
                                <i class="glyphicon glyphicon-search"></i>
                            </button>
                        </div>
                    </div>
                </div><br>
                <div class="form-group" style="margin:20px;">
                    <label for="advanced"> Search by </label>
                    <select  name="searchType">
                        <option value="${1}">Username</option>
                        <option value="${2}">Real Name</option>
                        <option value="${3}">Enabled</option>
                        <option value="${7}">Disabled</option>
                        <option value="${4}">Phone</option>
                        <option value="${5}">Date of birth</option>
                        <option value="${6}">Role</option>
                    </select>
                </div>
            </form>

            <div class="row">
                <div class="col-sm-12 col-xs-12 col-md-12 col-lg-12">
                    <div class="table-responsive">
                        <table class="table table-bordered">
                            <tr>
                                <th class="text-center">Username</th>                
                                <th class="text-center">Firstname</th>
                                <th class="text-center">Lastname</th>
                                <th class="text-center">Enabled</th>
                                <th class="text-center">Date of birth</th>
                                <th class="text-center">Address</th>
                                <th class="text-center">Phone</th>
                                <th class="text-center">Authority</th>
                                <th class="text-center">Action</th>
                            </tr>
                            <c:forEach var="each" items="${listUsers}">
                                <form:form action="edit-users" method="get">
                                    <tr>
                                        <td>
                                            <c:if test="${each.enabled == true }" >                            
                                                <span style="color: green" class="glyphicon glyphicon-user"></span>
                                            </c:if>
                                            <c:if test="${each.enabled == false }" >                            
                                                <span style="color: red" class="glyphicon glyphicon-user "></span>
                                            </c:if>
                                            ${each.username}

                                        </td>                        
                                        <td class="text-center">${each.userDetails.firstname}</td>
                                        <td class="text-center">${each.userDetails.lastname}</td>
                                        <td class="text-center">
                                            <c:if test="${each.enabled == true }" >                            
                                                <span style="color: green" class="glyphicon glyphicon-ok"></span>
                                            </c:if>
                                            <c:if test="${each.enabled == false }" >                            
                                                <span style="color: red" class="glyphicon glyphicon-remove "></span>
                                            </c:if>
                                        </td>
                                        <td class="text-center">${each.userDetails.dob}</td>
                                        <td class="text-center">${each.userDetails.address}</td>
                                        <td class="text-center">${each.userDetails.phone}</td>
                                        <td class="text-center">${each.auth.groupAuth.authorityRole}</td>
                                    <input type="hidden" name="username" value="${each.username}"/>
                                    <td><button class="btn btn-primary" type="submit" onclick="location.href='${pageContext.request.contextPath}/delete-user/${each.username}'"><span class="glyphicon glyphicon-pencil"></span></button>
                                        <button class="btn btn-warning" type="submit" value="remove" name="btn"><span class="glyphicon glyphicon-trash"></span></button></td>
                                    </tr>

                                </form:form>

                            </c:forEach>
                        </table>
                    </div>
                </div>
            </div>
        </fieldset>
    </div>
</div>