<%-- 
    Document   : add-user-form
    Created on : May 12, 2017, 1:53:14 AM
    Author     : hell_lock
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@taglib uri="http://www.springframework.org/security/tags" prefix="sec" %>
<jsp:include page="include/headerimport.jsp" flush="true"/>
<style>
    .field-required{
        color:red;
        font-weight:bolder;
    }
</style>
<!--<!DOCTYPE html>-->
<!--<html>
    <head>
<%--<jsp:include page="include/headerimport.jsp" flush="true"/>--%> 
<title>Add User Page</title>
</head>
<body>-->
<style>
    .formerror{
        color:red;
        font-size: 10;
    }
</style>
<script>
    $(document).ready(function () {
        $('#submit').click(function () {
            var pw1 = $('#password1').val();
            var pw2 = $('#password2').val();
            if (pw1 != pw2) {
                alert('Re-enter password not match!');
                $('#password1').focus();
                return false;
            }
        });
    });
</script>
<div class="container" style="margin-top: 0px">
    <div class="row">
        <div class="col-md-offset-4 col-xs-12 col-sm-12 col-md-6 col-lg-4">
            <h2 class="page-header" style="text-align: center;margin-top: 10px;">Register</h2>
        </div>
    </div>
    <c:if test="${param.err != null}">
        <div class="row" style="text-align: center;">
            <div class="col-md-offset-4 col-xs-12 col-sm-12 col-md-6 col-lg-4" >
                <div class="alert alert-danger" role="alert">${param.err}</div>
            </div>
        </div>
    </c:if>
    <div class="row">
        <div class="col-md-offset-4 col-xs-12 col-sm-12 col-md-6 col-lg-4 well well-lg" style="background-color: white;" >

            <form  action="register" method="post" id="registerform">
                <input type="hidden" name="auth.groupAuth.authorityRole" value="ROLE_USER"/>
                <div class="form-group">
                    <label for="username">Username  <span class="field-required">(*)</span></label>
                    <input type="text" name="username" class="form-control" id="username" required>
                    <form:errors cssClass="formerror" path="users.username" />
                </div>
                <div class="form-group">
                    <label for="password1">Password  <span class="field-required">(*)</span></label>
                    <input type="password" name="password" class="form-control" id="password1" required>
                    <form:errors cssClass="formerror"  path="users.password"/>
                </div>
                <div class="form-group">
                    <label for="password2">Re-enter password  <span class="field-required">(*)</span></label>
                    <input type="password" name="password2" class="form-control" id="password2" required>
                </div>
                <div class="form-group">
                    <label for="address">Address <span class="field-required">(*)</span></label>
                    <textarea name="userDetails.address" class="form-control" id="address"></textarea>
                </div>
                <div class="form-group">
                    <label for="email">Email <span class="field-required">(*)</span></label>
                    <input type="email" name="userDetails.email" class="form-control" id="email">
                    <form:errors cssClass="formerror" path="users.userDetails.email"/>
                </div>
                <div class="col-md-1 col-md-offset-9">
                    <input id="submit" type="submit" class="btn btn-primary" value="Register">
                </div>
            </form>
        </div>
    </div>
</div>
<script src="/resources/js/jquery.validate.js"></script>
<script>
    $('#registerform').validate({
        rules:{
            username:{
                required:true;
            },
            password:{
                required:true,
                minlength: 6
            },
            password2:{
                required:true,
                minlength: 6,
                equalTo: '#password1'
            }
        }
    })
</script>