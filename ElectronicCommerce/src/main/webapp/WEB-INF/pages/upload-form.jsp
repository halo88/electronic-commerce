<%-- 
    Document   : updateForm
    Created on : Apr 15, 2017, 9:03:10 PM
    Author     : hell_lock
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Update Form Pagea</title>
    </head>
    <body>
        <h3 style="color:red">${filesuccess}</h3>  
        <form:form method="post" action="savefile" enctype="multipart/form-data">  
            <p><label for="image">Choose Image</label></p>  
            <p><input name="file" id="fileToUpload" type="file" /></p>  
            <p><input type="submit" value="Upload"></p>  
            </form:form>  
    </body>
</html>
