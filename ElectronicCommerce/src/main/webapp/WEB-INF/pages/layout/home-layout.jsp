<%@taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title><tiles:insertAttribute name="title" ignore="true" /></title>  
        <!-- Style -->
        <link rel="stylesheet" type="text/css" href="<c:url value="/resources/css/ecommerce.css" />" /> 
        <link rel="stylesheet" type="text/css" href="<c:url value="/webjars/bootstrap/3.3.7/css/bootstrap.min.css" />" />
        <link rel="stylesheet" type="text/css" href="<c:url value="/resources/css/modal.css" />" />
        <!-- java script -->
        <script type="text/javascript" src="<c:url value="/resources/js/jquery.min.js"/>"></script>
        <script src="<c:url value="/webjars/bootstrap/3.3.7/js/bootstrap.min.js"/>" type="text/javascript" ></script>
    </head>

    <body style="background-color: #F5F5F5">
        <div><tiles:insertAttribute name="navbar" /></div>  
        <div class="container" style="float:left; padding-top:80px;padding-bottom:50px;width:100%;border-left:1px solid #CCCCCC;">  
            <tiles:insertAttribute name="body" /></div>  
        <div style="margin-bottom: 0px;clear:both"><tiles:insertAttribute name="footer" /></div>
    </body>
</html>
