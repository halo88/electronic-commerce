<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>


<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title><tiles:insertAttribute name="title" ignore="true" /></title>  
        <link rel="stylesheet" type="text/css" href="<c:url value="/webjars/bootstrap/3.3.7/css/bootstrap.min.css" />" />
        <script type="text/javascript" src="<c:url value="/resources/js/jquery.min.js"/>"></script>
        <script src="<c:url value="/webjars/bootstrap/3.3.7/js/bootstrap.min.js"/>" type="text/javascript" ></script>

        <link rel="stylesheet" type="text/css" href="<c:url value="/resources/css/ecommerce.css" />" />   
    <link rel="stylesheet" href="<c:url value="/resources/css/dashboard.css"/>"/>
    </head>
    <%--<body style="background-color: #F5F5F5">
        <div><tiles:insertAttribute name="navbar" /></div>  
        <div style="float:left;padding-top:70px;width:20%;min-width: 150px"><tiles:insertAttribute name="sidebar" /></div>  
        <div class="container" style="min-width: 300px;float:left;padding-top:80px;padding-left: 20px;padding-bottom:150px;width:80%;border-left:1px solid #CCCCCC;">  
            <tiles:insertAttribute name="body" /></div>  
        <div style="margin-bottom: 0px;clear:both"><tiles:insertAttribute name="footer" /></div>  
    </body>--%>
    <body style="background-color: #F5F5F5">
        <div><tiles:insertAttribute name="navbar" /></div>  

        <div class="container-fluid" style="margin-top:20px;
             padding-bottom:150px;
             border-left:1px solid #CCCCCC;">
            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-2 col-lg-2 sidebar" style="border: 1px solid black;min-width: 110px">
                    <tiles:insertAttribute name="sidebar" />
                </div>
                <div class="col-xs-12 col-sm-12 col-md-10 col-lg-10 col-md-offset-2 col-lg-offset-2 main">
                    <tiles:insertAttribute name="body" />
                </div>  
            </div>  
        </div>
        <div style="margin-bottom: 0px;clear:both"><tiles:insertAttribute name="footer" /></div>  
    </body>
</html>