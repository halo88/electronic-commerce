<%-- 
    Document   : home
    Created on : May 25, 2017, 12:52:34 PM
    Author     : hell_lock
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@taglib uri="http://www.springframework.org/security/tags" prefix="sec" %>

<link rel="stylesheet" href="<c:url value="/resources/css/productpage.css"/>"/>
<link rel="stylesheet" href="<c:url value="/resources/css/homethumbnail.css"/>"/>
<script src="<c:url value="/resources/js/productpage.js"/>"></script>
<script src="<c:url value="/resources/js/slider.js"/>"></script>
<link rel="stylesheet" href="<c:url value="/resources/css/slider.css"/>"/>
<script>
    $(function () {
        $("#slider1").responsiveSlides({
            auto: true,
            pager: true,
            nav: true,
            speed: 500,
            maxwidth: 800,
            namespace: "transparent-btns"
        });
    });

</script>
<div class="row">
    <div class=" col-xs-12 col-sm-12 col-md-12 col-lg-12">
        <div id="wrapper" style="width:100%">
            <div class="rslides_container">
                <ul class="rslides" id="slider1">
                    <li><img src="<c:url value="/resources/images/ImageHost/ad1.jpg"/>" alt="" style="width: 100%">
                        <!--<p class="slidercaption">NOTEBOOK LENOVO IP 110 14 - 80T60055VN</p></li>-->
                    <li><img src="<c:url value="/resources/images/ImageHost/ad2.jpg"/>" alt="" >
                        <!--<p class="slidercaption">NOTEBOOK ACER AS ES1-131.C0GP-NX.G17SV.001</p></li>-->
                    <li><img src="<c:url value="/resources/images/ImageHost/ad3.jpg"/>" alt="" >
                        <!--<p class="slidercaption">NOTEBOOK ACER AS ES1-131.C4GV-NX.MYKSV.001</p></li>-->
                    <li><img src="<c:url value="/resources/images/ImageHost/ad4.jpg"/>" alt="" >
                        <!--<p class="slidercaption">AD31</p></li>-->
                </ul>
            </div>
        </div>
    </div>
</div>
<div class="row well well-sm">
    <div class=" col-xs-6 col-sm-6 col-md-3 col-lg-3">
        <strong>View Mode</strong>
        <div class="btn-group">
            <a href="#" id="list" class="btn btn-default btn-sm"><span class="glyphicon glyphicon-th-list">
                </span>List</a> <a href="#" id="grid" class="btn btn-default btn-sm"><span
                    class="glyphicon glyphicon-th"></span>Grid</a>
        </div>
    </div>
    <div class=" col-xs-6 col-sm-6 col-md-1 col-lg-1 col-md-offset-8">
        <div class="dropdown">
            <a href="#" class="dropdown-toggle" 
               data-toggle="dropdown" role="button" 
               aria-haspopup="true" 
               aria-expanded="false">Sort by<span class="caret"></span>
            </a>
            <ul class="dropdown-menu pull-right list-group">
                <li><a href="${pageContext.request.contextPath}/home/1/productName">Name</a></li>
                <li><a href="${pageContext.request.contextPath}/home/1/adddate">Add Date</a></li>
                <li><a href="${pageContext.request.contextPath}/home/1/categories">Category</a></li>
                <li><a href="${pageContext.request.contextPath}/home/1/manufacture">Manufacture</a></li>
                <li><a href="${pageContext.request.contextPath}/home/1/unitPrice">Price</a></li>
            </ul>
        </div>
    </div>
</div>
<div id="products" class="flex-row  row list-group">
    <c:forEach var="each" items="${listProduct}" varStatus="status">
        <div class="item  col-xs-12 col-sm-12 col-md-3 col-lg-3 ">
            <div class="thumbnail">
                <a href="${pageContext.request.contextPath}/detailproduct/${each.productID}">
                    <c:if test="${each.isSaling()}">
                        <img  class="promotion_icon" src="<c:url value="/resources/images/ImageHost/onsale.png"/>" />
                    </c:if>
                    <img class="group list-group-image" src="<c:url value="/resources/images/ImageHost/${each.imageurl}"/>" alt="" width="150"  height="150"/>
                </a>
                <div class="caption" >
                    <a href="${pageContext.request.contextPath}/detailproduct/${each.productID}">
                        <strong> <p class="group inner list-group-item-heading" style="min-height:50px">
                                ${each.productName}</p></strong></a>
                    <p class="group inner list-group-item-text" style="height:150px;overflow:auto;padding-left:20px;">
                        <strong>CPU:</strong> ${each.desc.cpu}<br>
                        <strong>Chipset:</strong> ${each.desc.chipset}<br>
                        <strong>RAM:</strong> ${each.desc.ram}<br>
                        <strong>HDD:</strong> ${each.desc.hdd}<br>
                        <strong>Display:</strong> ${each.desc.display}
                    </p>
                    <div class="row" style="margin-bottom:55px;margin-left:10px">
                        <div class="col-xs-12 col-md-6">
                           <!--<p class="lead" style="font-size: 25px;padding-left:30px;">$ ${each.unitPrice}</p>-->
                        </div>
                        <div class="col-xs-12 col-md-6">
                            <input id="amount" class="form-control amount" type="text" style="width: 50px;" value="1"/>
                        </div>
                    </div>
                    <div class="row " style="position:absolute;left:40px;bottom:1px">
                        <div class="col-xs-12 col-md-6">
                            <p class="lead" style="font-size: 25px">$ ${each.unitPrice}</p>
                        </div>
                        <div class="col-xs-12 col-md-5 col-md-offset-1" >
                            <button onclick="addCart(${status.index},${each.productID})" class="btn btn-success">Add to cart</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </c:forEach>
    <script>
        function addCart(statusindex, id) {
            var amount = $('.amount')[statusindex].value;
            var data = {
                "amount": amount,
                "productID": id
            };

            $.ajax({
                type: 'post',
                dataType: 'html',
                contentType: 'application/json',
                url: '${pageContext.request.contextPath}/add-to-cart',
                data: JSON.stringify(data),
                success: function (data) {
//                    alert("success");
                    var a = data.split('*');
                    $('#dropdowncart').html(a[0]);
                    $('#cartsize').html(a[1]);
                },
                error: function (error) {
                    alert('error');
                    console.log(error);
                }
            });
        }
    </script>
</div>
<script src="<c:url value="/resources/js/jquery-ui.min.js"/>"></script>
<script src="<c:url value="/resources/js/homepagination.js"/>"></script>
<link rel="stylesheet" href="<c:url value="/resources/css/homepagination.css"/>"/>
<%--<script>
    $(document).ready(function () {
        var itemsCount = ${totalItems};
        var itemsOnPage = 4;

        var pagination = new Pagination({
            container: $("#pagination"),
            maxVisibleElements: 5,
            showInput: true,
            inputTitle: 'Go to page',
            pageClickUrl: "${pageContext.request.contextPath}/home/{{page}}/${sessionScope.sort}"
                    });
                    pagination.make(itemsCount, itemsOnPage);
                });
</script>     --%> 
<script>
        $(document).ready(function () {
            var itemsCount = ${totalItems};
            var itemsOnPage = 8;
            var pagination = new Pagination({
                container: $("#pagination"),
                pageClickUrl: "${pageContext.request.contextPath}/home/{{page}}/${sessionScope.sort}",
                            showInput: true,
                            maxVisibleElements: 1,
                            inputTitle: "Go to page"
                        });
                        pagination.make(itemsCount, itemsOnPage, ${page});
                    });
</script>      
<div class="row">
    <div class="col-xs-12 col-sm-12 col-md-3 col-md-offset-9">
        <div id="pagination"></div>
    </div>
</div>


