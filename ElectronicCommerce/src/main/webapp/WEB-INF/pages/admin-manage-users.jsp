<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form" %>

<link rel="stylesheet" type="text/css" href="<c:url value="/webjars/datatables/1.10.13/css/jquery.dataTables.min.css" />" />
<link rel="stylesheet" type="text/css" href="<c:url value="/resources/css/responsive.dataTables.min.css" />" />
<link rel="stylesheet" type="text/css" href="<c:url value="/resources/css/projectStyle.css" />" />
<script src="<c:url value="/webjars/datatables/1.10.13/js/jquery.dataTables.min.js"/>" type="text/javascript" ></script>
<script src="<c:url value="/resources/js/dataTables.responsive.min.js" />" type="text/javascript" ></script>
<script src="<c:url value="/resources/js/dataTableUser.js"/>" type="text/javascript" ></script>
<script>
    $(document).ready(function () {
        var data = [
    <c:forEach var="each" items="${list}">
            {username: "${each.username}", enabled: "${each.enabled}", password: "${each.password}", firstname: "${each.userDetails.firstname}",
                lastname: "${each.userDetails.lastname}", dob: "${each.userDetails.dob}", address: "${each.userDetails.address}",
                phone: "${each.userDetails.phone}", email: "${each.userDetails.email}",
                gender: "${each.userDetails.gender}"
            },
    </c:forEach>
        ];
        doDataUser(data);
    });
</script>

    <div class="row">
    <div class="col-xs-12 col-sm-12  col-md-12 col-lg-12 ">
        <h2 class="page-header" style="text-align: center;">Manage all users</h2>
    </div>
</div>
    <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
            <table id="tableUser" class="row-border hover display table table-bordered responsive" width="100%" cellspacing="0">
                <thead>
                    <tr>
                        <th>Email</th>        
                        <th>Gender</th>
                        <th>Username</th>        
                        <th>First Name</th>
                        <th>Last Name</th>
                        <th>Enabled</th>
                        <th>Date of birth</th>
                        <th>Address</th>
                        <th>Phone</th>  
                        <th>Action</th>
                    </tr>
                </thead>
            </table>
        </div>
    </div>
