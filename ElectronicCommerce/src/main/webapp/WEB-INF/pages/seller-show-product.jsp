<%-- 
    Document   : seller-show-product
    Created on : May 18, 2017, 9:30:51 AM
    Author     : PC
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@taglib uri="http://www.springframework.org/security/tags" prefix="scr" %>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="stylesheet" type="text/css" href="<c:url value="/webjars/bootstrap/3.3.7/css/bootstrap.min.css" />" />
        <link rel="stylesheet" type="text/css" href="<c:url value="/resources/css/main.css" />" />
        <script type="text/javascript" src="//cdn.jsdelivr.net/jquery/1/jquery.min.js"></script>
        <script src="<c:url value="/webjars/bootstrap/3.3.7/js/bootstrap.min.js"/>" type="text/javascript" ></script>
        <title>JSP Page</title>


        <link rel="stylesheet" type="text/css" href="<c:url value="/resources/css/template.css" />" />
        <link rel="stylesheet" type="text/css"  href="<c:url value="/resources/css/pages.css" />"/>
        <link rel="stylesheet" type="text/css" href="<c:url value="/resources/css/product.css" />" />    

        <link rel="stylesheet" type="text/css" href="<c:url value="/resources/css/menu.css" />" /> 
        <link rel="stylesheet" type="text/css" href="<c:url value="/resources/css/comment.css" />" />    
        <link rel="stylesheet" type="text/css" href="<c:url value="/resources/css/compare.css" />" />
        <link rel="stylesheet" type="text/css" href="<c:url value="/resources/css/responsive.css" />" />

    </head>
    <body>
        <div class="container">
            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-12 text-center">
                    <h2>Show All Product</h2>
                </div>
            </div>            
            <jsp:include page="include/header.jsp" />
            <c:set var="contextPath" value="${pageContext.request.contextPath}"/>
            <div class="row" style="padding-bottom: 20px">
                <div class="col-xs-12 col-sm-6 col-md-6">
                    <form:form action="search" class="form-inline" 
                               method="post">
                        <div class="form-group">
                            <input type="text" class="form-control" 
                                   name="searchName" placeholder="Search">
                        </div>
                        <button type="submit" class="btn btn-default">
                            Search</button>
                        </form:form>
                </div>
            </div>
            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-12">
                    <div class="table-responsive">
                        <table class="table table-bordered table-hover">
                            <tr>

                                <th>Product Name</th>
                                <th>Add Date</th>
                                <th>Description</th>
                                <th>UnitPrice</th>
                                <th>Warranty</th>
                                <th>Stock</th>
                                <th>Seller Name</th>
                                <th>Type</th>
                                <th>Manufacture</th>
                                <th>Promotion</th>
                                <th class="text-center">Action</th>
                            </tr>
                            <c:forEach var="p" items="${lstPro}">
                                <tr>

                                    <td>${p.productName}</td>
                                    <td>${p.adddate}</td>
                                    <td>${p.description}</td>
                                    <td>${p.unitPrice}</td>
                                    <td>${p.warranty}</td>
                                    <td>${p.stock}</td>
                                    <td>${p.users.username}</td>
                                    <td>${p.types.typeName}</td>
                                    <td>${p.manufacture.manufactureName}</td>
                                    <td>${p.promotions.promoName}</td>

                                    <td class="text-center">
                                        <%--<scr:authorize access="hasRole('ROLE_SELLER')">--%>
                                            <button type="button" onclick="location.href = '${contextPath}/edit/${p.productID}'" class="btn btn-primary">Edit</button>
                                            <button type="button" onclick="location.href = '${contextPath}/delete/${p.productID}'" class="btn btn-warning">Delete</button>
                                        <%--</scr:authorize>--%>
                                        <!--                                        <button type="button" class="btn btn-primary">Add Friend</button>-->
                                    </td>
                                </tr>
                            </c:forEach>
                        </table>
                    </div>
                </div>
            </div>
        </div>









        <div class="container">
            <div class="row">
                <div class="home_title no_padding col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="new_product_title">Sản phẩm mới</div>
                    <span class="new_product_more visible-xs">Xem tất cả</span>            
                    <div class="new_product_title_line"></div>
                </div>
            </div>    
            <div class="home_product_bg row">
                <div class="product_item col-lg-2 col-md-3 col-sm-3 col-xs-12">
                    <div class="promotion_icon"> </div>
                    <a href="/#!ProductDetails/SID46368/MOBILE-SAMSUNG-GALAXY-J1-MINI-J105-WHITE.mpx" class="product_img">
                        <img src="/images/GALAXYJ1.jpg" dir="M00024">
                    </a>
                    <div class="product_info_bg">
                        <a href="/#!ProductDetails/SID46368/MOBILE-SAMSUNG-GALAXY-J1-MINI-J105-WHITE.mpx" class="product_name product_name_46368">MOBILE SAMSUNG GALAXY J1 MINI - J105 (WHITE)</a>
                        <div class="product_id">Mã sản phẩm: 46368</div>
                        <div class="product_price">1.750.000 <span>VNÐ</span></div>
                    </div>                
                    <div class="product_hover_bg hidden-xs">                    
                        <a href="/#!ProductDetails/SID46368/MOBILE-SAMSUNG-GALAXY-J1-MINI-J105-WHITE.mpx" class="product_info"><ul><li><span>CPU</span>: Spreadtrum SC8830 4 nhân 32-bit, 1.2 GHz</li><li><span>Màn hình</span>: TFT, 4 , WVGA</li><li><span>OS</span>: Android 5.1 (Lollipop)</li><li><span>Bộ nhớ</span>: RAM 768MP, ROM 8GB</li></ul></a>
                        <div class="product_hover_btn_bg">
                            <a class="product_add_btn" id="46368">Chọn mua</a>
                            <a href="#!Compare/SID46368/MOBILE-SAMSUNG-GALAXY-J1-MINI-J105-WHITE.mpx" class="product_compare_btn">So sánh</a>
                        </div>
                    </div>
                </div> 


                <div class="product_item col-lg-2 col-md-3 col-sm-3 col-xs-12">
                    <a href="/#!ProductDetails/SID46389/MAY-TINH-BANG-MICROSOFT-SURFACE-BOOKCOREI5-6300U8G128G135W10.mpx" class="product_img">
                        <img src="/Images/Products/M00127.png" dir="M00127">
                    </a>
                    <div class="product_info_bg">
                        <a href="/#!ProductDetails/SID46389/MAY-TINH-BANG-MICROSOFT-SURFACE-BOOKCOREI5-6300U8G128G135W10.mpx" class="product_name product_name_46389">MAY TINH BANG MICROSOFT SURFACE BOOK(COREI5 6300U/8G/128G/13.5/W10)</a>
                        <div class="product_id">Mã sản phẩm: 46389</div>
                        <div class="product_price">36.990.000 <span>VNÐ</span></div>
                    </div>                
                    <div class="product_hover_bg hidden-xs">                    
                        <a href="/#!ProductDetails/SID46389/MAY-TINH-BANG-MICROSOFT-SURFACE-BOOKCOREI5-6300U8G128G135W10.mpx" class="product_info"><ul><li><span>CPU</span>: Intel Core i5 6300U(4*2.4Ghz, 3MB L3cache, 14nm, 15W)</li><li><span>Chipset</span>: Intel 8 Series Express</li><li><span>RAM</span>: 8GB LPDDR3 1867 MHz</li><li><span>HDD</span>: 128GB SSD</li></ul></a>

                        <div class="product_hover_btn_bg">


                            <a class="product_add_btn" id="46389">Chọn mua</a>
                            <a href="#!Compare/SID46389/MAY-TINH-BANG-MICROSOFT-SURFACE-BOOKCOREI5-6300U8G128G135W10.mpx" class="product_compare_btn">So sánh</a>

                        </div>
                    </div>
                </div>               

                <div class="product_item col-lg-2 col-md-3 col-sm-3 col-xs-12">
                    <div class="promotion_icon"></div>
                    <a href="/#!ProductDetails/SID50146/NOTEBOOK-ACER-SWITCH-ALPHA-12-SA5-271PI5-6200U4GB256SSD12QHDW10.mpx" class="product_img">
                        <img src="/uploads/Notebook/50146.jpg" dir="M00008">
                    </a>
                    <div class="product_info_bg">
                        <a href="/#!ProductDetails/SID50146/NOTEBOOK-ACER-SWITCH-ALPHA-12-SA5-271PI5-6200U4GB256SSD12QHDW10.mpx" class="product_name product_name_50146">NOTEBOOK ACER SWITCH ALPHA 12 SA5-271P(I5-6200U/4GB/256SSD/12QHD/W10)</a>
                        <div class="product_id">Mã sản phẩm: 50146</div>
                        <div class="product_price">21.590.000 <span>VNÐ</span></div>
                    </div>                
                    <div class="product_hover_bg hidden-xs">                    
                        <a href="/#!ProductDetails/SID50146/NOTEBOOK-ACER-SWITCH-ALPHA-12-SA5-271PI5-6200U4GB256SSD12QHDW10.mpx" class="product_info"><ul><li><span>CPU</span>: Intel Core I5-6200U(4*2.3Ghz, 3MB L3 cache,14nm, 15W)</li><li><span>RAM</span>: 4GB DDR3</li><li><span>HDD</span>: 256GB SSD</li><li><span>Display</span>: 12   QHD TOUCH</li></ul></a>

                        <div class="product_hover_btn_bg">


                            <a class="product_add_btn" id="50146">Chọn mua</a>
                            <a href="#!Compare/SID50146/NOTEBOOK-ACER-SWITCH-ALPHA-12-SA5-271PI5-6200U4GB256SSD12QHDW10.mpx" class="product_compare_btn">So sánh</a>

                        </div>
                    </div>
                </div>               

                <div class="product_item col-lg-2 col-md-3 col-sm-3 col-xs-12">
                    <div class="promotion_icon"></div>
                    <a href="/#!ProductDetails/SID50394/NOTEBOOK-ACER-SWITCH-ALPHA12-SA5-271P39TDI3-6100U4GB128GB12QHDW10.mpx" class="product_img">
                        <img src="/uploads/Notebook/50394.jpg" dir="M00008">
                    </a>
                    <div class="product_info_bg">
                        <a href="/#!ProductDetails/SID50394/NOTEBOOK-ACER-SWITCH-ALPHA12-SA5-271P39TDI3-6100U4GB128GB12QHDW10.mpx" class="product_name product_name_50394">NOTEBOOK ACER SWITCH ALPHA12 SA5-271P39TD(I3-6100U/4GB/128GB/12QHD/W10</a>
                        <div class="product_id">Mã sản phẩm: 50394</div>
                        <div class="product_price">16.890.000 <span>VNÐ</span></div>
                    </div>                
                    <div class="product_hover_bg hidden-xs">                    
                        <a href="/#!ProductDetails/SID50394/NOTEBOOK-ACER-SWITCH-ALPHA12-SA5-271P39TDI3-6100U4GB128GB12QHDW10.mpx" class="product_info"><ul><li><span>CPU</span>: Intel Core i3 6100U(4*2.3Ghz, 3MB L3 cache,14nm, 15W)</li><li><span>RAM</span>: 4GB DDR3</li><li><span>HDD</span>: 128GB SSD</li><li><span>Display</span>: 12   QHD TOUCH</li></ul></a>

                        <div class="product_hover_btn_bg">


                            <a class="product_add_btn" id="50394">Chọn mua</a>
                            <a href="#!Compare/SID50394/NOTEBOOK-ACER-SWITCH-ALPHA12-SA5-271P39TDI3-6100U4GB128GB12QHDW10.mpx" class="product_compare_btn">So sánh</a>

                        </div>
                    </div>
                </div>               

                <div class="product_item col-lg-2 col-md-3 col-sm-3 col-xs-12">

                    <a href="/#!ProductDetails/SID31433/APPLE-IPAD-5-32-GB-WIFI-white.mpx" class="product_img">
                        <img src="/uploads/Mobile/Apple/31433.jpg" dir="M00126">
                    </a>
                    <div class="product_info_bg">
                        <a href="/#!ProductDetails/SID31433/APPLE-IPAD-5-32-GB-WIFI-white.mpx" class="product_name product_name_31433">APPLE IPAD 5 32 GB WIFI (white)</a>
                        <div class="product_id">Mã sản phẩm: 31433</div>
                        <div class="product_price">13.590.000 <span>VNÐ</span></div>
                    </div>                
                    <div class="product_hover_bg hidden-xs">                    
                        <a href="/#!ProductDetails/SID31433/APPLE-IPAD-5-32-GB-WIFI-white.mpx" class="product_info"><ul><li><span>CPU</span>: Lõi kép 1.3 GHz</li><li><span>Chipset</span>: Apple 64 bit  A7 + m7</li><li><span>RAM</span>: 1 Gb</li><li><span>HDD</span>: 32 GB</li></ul></a>

                        <div class="product_hover_btn_bg">


                            <a class="product_add_btn" id="31433">Chọn mua</a>
                            <a href="#!Compare/SID31433/APPLE-IPAD-5-32-GB-WIFI-white.mpx" class="product_compare_btn">So sánh</a>

                        </div>
                    </div>
                </div>               

                <div class="product_item col-lg-2 col-md-3 col-sm-3 col-xs-12">
                    <div class="promotion_icon"></div>
                    <a href="/#!ProductDetails/SID49863/NOTEBOOK-PREDATOR-G9-792I7-6700HQ32GB1TB+256GBDVD173FVGA8GBW10.mpx" class="product_img">
                        <img src="/uploads/Notebook/49863.jpg" dir="M00008">
                    </a>
                    <div class="product_info_bg">
                        <a href="/#!ProductDetails/SID49863/NOTEBOOK-PREDATOR-G9-792I7-6700HQ32GB1TB+256GBDVD173FVGA8GBW10.mpx" class="product_name product_name_49863">NOTEBOOK PREDATOR G9-792(I7-6700HQ/32GB/1TB+256GB/DVD/17.3F/VGA8GB/W10</a>
                        <div class="product_id">Mã sản phẩm: 49863</div>
                        <div class="product_price">69.590.000 <span>VNÐ</span></div>
                    </div>                
                    <div class="product_hover_bg hidden-xs">                    
                        <a href="/#!ProductDetails/SID49863/NOTEBOOK-PREDATOR-G9-792I7-6700HQ32GB1TB+256GBDVD173FVGA8GBW10.mpx" class="product_info"><ul><li><span>CPU</span>: Intel Core i7 6700HQ(8*2.6Ghz, 6MB, Turbo 8*3.5Ghz, 22nm. 45w)</li><li><span>RAM</span>: 32GB DDR4 2133MHz (4 slots)</li><li><span>HDD</span>: 1TB SATA3 (7200rpm) + 256GB SSD (M.2 2280)</li><li><span>Optical</span>: Bluray-RW</li></ul></a>

                        <div class="product_hover_btn_bg">
                            <a class="product_add_btn" id="49863">Chọn mua</a>
                            <a href="#!Compare/SID49863/NOTEBOOK-PREDATOR-G9-792I7-6700HQ32GB1TB+256GBDVD173FVGA8GBW10.mpx" class="product_compare_btn">So sánh</a>

                        </div>
                    </div>
                </div> 
            </div>
        </div>
    </body>
</html>
