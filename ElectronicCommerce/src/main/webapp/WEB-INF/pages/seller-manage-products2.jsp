<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@taglib uri="http://www.springframework.org/security/tags" prefix="sec" %>

<link rel="stylesheet" type="text/css" href="<c:url value="/webjars/datatables/1.10.13/css/jquery.dataTables.min.css" />" />
<link rel="stylesheet" type="text/css" href="<c:url value="/resources/css/responsive.dataTables.min.css" />" />
<link rel="stylesheet" type="text/css" href="<c:url value="/resources/css/projectStyle.css" />" />
<script src="<c:url value="/webjars/datatables/1.10.13/js/jquery.dataTables.min.js"/>" type="text/javascript" ></script>
<script src="<c:url value="/resources/js/dataTables.responsive.min.js" />" type="text/javascript" ></script>
<script src="<c:url value="/resources/js/dataTableProducts.js"/>" type="text/javascript" ></script>
<sec:authorize access="hasRole('ROLE_ADMIN')">
    <c:set var="role" value="admin"/>
</sec:authorize>
<sec:authorize access="hasRole('ROLE_SELLER')">
    <c:set var="role" value="seller"/>
</sec:authorize>
<style>
    .form-group .control-label{
        text-align:left;
    }
    fieldset .form-group{
        padding-left:20px;
    }
</style>
<script>
    $(document).ready(function () {
        var data = [
    <c:forEach var="each" items="${list}">
            {productName: "${each.productName}", adddate: "${each.adddate}", description: "${each.description}",
                unitPrice: "${each.unitPrice}", stock: "${each.stock}", warranty: "${each.warranty}",
                categoryName: "${each.categories.categoryName}", manufactureName: "${each.manufacture.manufactureName}",
                promoDesc: "${each.promotions.promoDesc}" + "%", productID: "${each.productID}"
            },
    </c:forEach>
        ];
        doDt(data);
    });
</script>
<div class="col-xs-12 col-sm-12 col-md-12" style="text-align: center">
    <h2 class="page-header" style="text-align: center;">Manage all products</h2>
</div>
<div clas="row">
    <div class="col-sm-6 col-md-3">
        <a class="btn btn-primary" href="${pageContext.request.contextPath}/${role}/add-product">Add new product</a>
    </div>
</div>
<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
    <table id="tableDemo" class="row-border hover display table table-bordered responsive" width="100%" cellspacing="0">
        <thead>
            <tr>
                <th>ProductID</th>
                <th>Description</th>
                <th>Product name</th>
                <th>Add date</th>
                <th>Unit price</th>
                <th>Warranty</th>
                <th>Category</th>
                <th>Manufacture</th>
                <th>Promotion</th>
                <th>Available</th>
                <th>Action</th>
            </tr>
        </thead>
    </table>
</div>
<c:forEach var="each1" items="${list}"  varStatus="status">
    <div id="editinfo${each1.productID}" class="modal fade" role="dialog">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Edit</h4>
                </div>
                <form:form class="form-horizontal" action="eedit-product" method="post"  enctype="multipart/form-data" acceptCharset="utf-8">
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-xs-12 col-sm-12 col-md-8 col-md-offset-2 col-lg-8">
                                <div class="form-group">
                                    <label class="control-label col-sm-3" for="productName">Product name</label>
                                    <div class="col-sm-9">
                                        <input type="text" class="form-control" id="productName" name="productName" value="${each1.productName}">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-sm-3" for="unitPrice">Unitprice</label>
                                    <div class="col-sm-9"> 
                                        <div class="input-group">
                                            <div class="input-group-addon">$</div>
                                            <input type="text" name="unitPrice" value="${each1.unitPrice}"
                                                   class="form-control" id="unitprice"
                                                   pattern="[0-9]+\.?[0-9]*" title="Enter a number">
                                        </div>                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-sm-3" for="warranty">Warranty</label>
                                    <div class="col-sm-9"> 
                                        <input type="text" class="form-control" id="warranty" name="warranty" value="${each1.warranty}">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-sm-3" for="stock">Available</label>
                                    <div class="col-sm-9"> 
                                        <input type="text" class="form-control" id="stock" name="stock" pattern="[0-9]+" value="${each1.stock}">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-sm-3">Category</label>
                                    <div class="col-sm-9"> 
                                        <select class="form-control" name="categories.categoryID">
                                            <c:forEach var="group" items="${listCat}">
                                                <option value="${group.categoryID}">${group.categoryName}</option>
                                            </c:forEach>
                                        </select>                            
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-sm-3">Manufacture</label>
                                    <div class="col-sm-9"> 
                                        <select class="form-control" name="manufacture.manufactureID">
                                            <c:forEach var="group" items="${lstManufactures}">
                                                <option value="${group.manufactureID}">${group.manufactureName}</option>
                                            </c:forEach>
                                        </select>                            
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-sm-3">Promotion</label>
                                    <div class="col-sm-9"> 
                                        <select class="form-control" name="promotions.promoDesc" onchange="showhideAll(${each1.productID});">
                                            <option 
                                                >Another %</option>
                                            <option <c:if test="${each1.promotions.promoDesc == 0}">
                                                    selected</c:if> 
                                                value="${0}">0%</option>
                                            <option <c:if test="${each1.promotions.promoDesc == 5}">
                                                    selected</c:if>
                                                value="${5}">5%</option>
                                            <option <c:if test="${each1.promotions.promoDesc == 10}">
                                                    selected</c:if>
                                                value="${10}">10%</option>
                                            <option <c:if test="${each1.promotions.promoDesc == 25}">
                                                    selected</c:if>
                                                value="${25}">25%</option>
                                            <option <c:if test="${each1.promotions.promoDesc == 50}">
                                                    selected</c:if>
                                                value="${50}">50%</option>

                                        </select>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-xs-12 col-sm-12 col-md-10 col-md-offset-2 col-lg-10">
                                        <div class="row customvalue" id="customvalue${status}" style="display:none">
                                            <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6" >
                                                <div class="form-group">
                                                    <label for="custom" class="control-label col-sm-4">Custom value</label>
                                                    <div class="input-group col-sm-6" style="padding-left:15px">
                                                        <input type="text" name="promotions.promoDesc" value="${each1.promotions.promoDesc}"
                                                               class="form-control" id="custom"
                                                               pattern="[0-9]+\.?[0-9]*" title="Enter a number">
                                                        <div class="input-group-addon">%</div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row choosedate" id="choosedate${status}"  style="display:none">
                                            <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                                                <div class="form-group">
                                                    <label class="control-label col-sm-4">Start</label>
                                                    <div class="col-sm-8">
                                                        <input name="promotions.startdate" type="date" class="form-control" value="${each1.promotions.startdate}">
                                                    </div> 
                                                </div>
                                            </div>
                                            <div class="col-xs-12 col-sm-12 col-md-6  col-lg-6">
                                                <div class="form-group">
                                                    <label class="control-label col-sm-4">End</label>
                                                    <div class="col-sm-8">
                                                        <input name="promotions.enddate" type="date" class="form-control" value="${each1.promotions.enddate}">
                                                    </div> 
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-xs-12 col-sm-12 col-md-9 col-md-offset-2 col-lg-9">
                                <div class="form-horizontal">
                                    <fieldset>
                                        <legend>Description</legend>
                                        <div class="row" >
                                            <div class="col-sm-12 col-xs-12 col-md-6 col-lg-6">
                                                <div class="form-group">
                                                    <label class="col-sm-3 control-label" for="cpu">CPU</label>
                                                    <div class="col-sm-7">
                                                        <input id="cpu" class="form-control" type="text" name="desc.cpu" value="${each1.desc.cpu}">
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="col-sm-3 control-label" for="ram">RAM</label>
                                                    <div class="col-sm-7">
                                                        <input id="ram" class="form-control" type="text" name="desc.ram" value="${each1.desc.ram}">
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="col-sm-3 control-label" for="chipset">Chipset</label>
                                                    <div class="col-sm-7">
                                                        <input id="chipset" class="form-control" type="text" name="desc.chipset" value="${each1.desc.chipset}">
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="col-sm-3 control-label" for="hdd">HDD</label>
                                                    <div class="col-sm-7">
                                                        <input id="hdd" class="form-control" type="text" name="desc.hdd" value="${each1.desc.hdd}">
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="col-sm-3 control-label" for="optical">Optical</label>
                                                    <div class="col-sm-7">
                                                        <input id="optical" class="form-control" type="text" name="desc.optical" value="${each1.desc.optical}">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-sm-12 col-xs-12 col-md-6 col-lg-6">
                                                <div class="form-group">
                                                    <label class="col-sm-3 control-label" for="display">Display</label>
                                                    <div class="col-sm-7">
                                                        <input id="display" class="form-control" type="text" name="desc.display" value="${each1.desc.display}">
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="col-sm-3 control-label" for="graphic">Graphic</label>
                                                    <div class="col-sm-7">
                                                        <input id="graphic" class="form-control" type="text" name="desc.graphic" value="${each1.desc.graphic}">
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="col-sm-3 control-label" for="network">Network</label>
                                                    <div class="col-sm-7">
                                                        <input id="network" class="form-control" type="text" name="desc.network" value="${each1.desc.network}">
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="col-sm-3 control-label" for="weight">Weight</label>
                                                    <div class="col-sm-7">
                                                        <input id="weight" class="form-control" type="text" name="desc.weight" value="${each1.desc.weight}">
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="col-sm-3 control-label" for="other">Other</label>
                                                    <div class="col-sm-7">
                                                        <textarea name="desc.other" class="form-control" id="other">${each1.desc.other}</textarea>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </fieldset>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-xs-12 col-sm-12 col-md-8 col-md-offset-2 col-lg-8">
                                <div class="form-group">
                                    <label class="control-label col-sm-3" for="image">Feature Image</label>
                                    <img class="group list-group-image"
                                         src="<c:url value="/resources/images/ImageHost/${each1.imageurl}"/>" alt="" width="150"  height="150"/>
                                    <div class="col-sm-9"> 
                                        <input type="hidden" name="productID" value="${each1.productID}"/>
                                        <input type="hidden" name="adddate" value="${each1.adddate}"/>
                                        <input type="hidden" name="currentimage" value="${each1.imageurl}"/>
                                        <input name ="file" type="file" id="image">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button  type="submit" class="btn btn-default">Save</button>
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    </div>
                </form:form>
            </div>
        </div>
    </div>
</c:forEach>
<script>
    function showhideCustom(a, id) {
        $('#editinfo' + id + ' .customvalue')[0].style.display = a;
    }
    function showhideDate(a, id) {
        $('#editinfo' + id + ' .choosedate')[0].style.display = a;
    }
    function showhideAll(id) {
        var value = $('#editinfo' + id + ' select')[2].value;
        if (value == 0) {
            showhideCustom('none', id);
            showhideDate('none', id);
            return;
        }
        if (value == 5 || value == 10 || value == 25 || value == 50) {
            showhideCustom('none', id);
            showhideDate('block', id);
            return;
        }
        if (value != 0 || value != 5 || value != 10 || value != 25 || value != 50) {
            showhideCustom('block', id);
            showhideDate('block', id);
            return;
        }
    }
</script>