<%-- 
    Document   : add-user-form
    Created on : May 12, 2017, 1:53:14 AM
    Author     : hell_lock
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@taglib uri="http://www.springframework.org/security/tags" prefix="sec" %>
<jsp:include page="include/headerimport.jsp" flush="true"/>
<style>
    .field-required{
        color:red;
        font-weight:bolder;
    }
</style>
<!--<!DOCTYPE html>-->
<!--<html>
    <head>
<%--<jsp:include page="include/headerimport.jsp" flush="true"/>--%> 
<title>Add User Page</title>
</head>
<body>-->
<style>
    .formerror{
        color:red;
        font-size: 10;
    }
</style>
<div class="container" style="margin-top: 0px">
    <div class="row">
        <div class="col-md-offset-4 col-xs-12 col-sm-12 col-md-6 col-lg-4">
            <h2 class="page-header" style="text-align: center;margin-top: 10px;">Please fill in the form</h2>
        </div>
    </div>
    <c:if test="${param.err != null}">
        <div class="row" style="text-align: center;">
            <div class="col-md-offset-4 col-xs-12 col-sm-12 col-md-6 col-lg-4" >
                <div class="alert alert-danger" role="alert">${param.err}</div>
            </div>
        </div>
    </c:if>
    <div class="row">
        <div class="col-md-offset-4 col-xs-12 col-sm-12 col-md-6 col-lg-4 well well-lg" style="background-color: white;" >

            <form  action="after-checkout-ok" method="post" id="registerform">
                <div class="form-group">
                    <label for="address">Address <span class="field-required">(*)</span></label>
                    <textarea name="userDetails.address" class="form-control" id="address" required></textarea>
                </div>
                <div class="form-group">
                    <label for="email">Email <span class="field-required">(*)</span></label>
                    <input type="email" name="userDetails.email" class="form-control" id="email" required>
                    <form:errors cssClass="formerror" path="users.userDetails.email"/>
                </div>
                <div class="col-md-1 col-md-offset-9">
                    <input id="submit" type="submit" class="btn btn-primary" value="Register">
                </div>
            </form>
        </div>
    </div>
</div>