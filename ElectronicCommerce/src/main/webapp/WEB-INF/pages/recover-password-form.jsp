

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@taglib uri="http://www.springframework.org/security/tags" prefix="sec" %>
<script>
    $(document).ready(function () {
        $('#submit').click(function () {
            var pw1 = $('#pw1').val();
            var pw2 = $('#pw2').val();
            if (pw1 != pw2) {
                alert('Re-enter password not match!');
                $('#pw1').focus();
                return false;
            }
        });
    });
</script>
<div class="row">
    <div class="col-sm-12 col-xs-12 col-md-7 col-lg-7 col-md-offset-3">
        <div class="panel panel-info text-center">
            <div class="panel-heading">
                <h4 >Reset Password</h4>
            </div>
            <form class="form-horizontal" action="change-password" method="post">
                <div class="panel-body">
                    <div class="form-group">
                        <label class="control-label col-sm-3"></label>
                        <div class="col-sm-6 col-sm-offset-1">
                            <input type="text" value="${username1}" name="username" class="form-control" readonly/>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-3">New Password</label>
                        <div class="col-sm-6 col-sm-offset-1">
                            <input type="password" id="pw1" name="password" class="form-control"/>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-3">Re-enter Password</label>
                        <div class="col-sm-6 col-sm-offset-1">
                            <input type="password" id="pw2" class="form-control"/>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-sm-offset-6">
                            <button type="submit" id="submit" class="btn btn-primary">Submit</button>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
