<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<script>
    $(document).ready(function () {
        $('#submit').click(function () {
            var pw1 = $('#pw1').val();
            var pw2 = $('#pw2').val();
            if (pw1 != pw2) {
                alert('Re-enter password not match!');
                $('#pw1').focus();
                return false;
            }
        });
    });
</script>
<div class="row">
    <div class="col-xs-12 col-sm-12  col-md-12 col-lg-12 ">
        <h2 class="page-header" style="text-align: center;">Change password</h2>
    </div>
</div>
<c:if test="${param.mess != null}">
    <div class="row">
        <div class="col-sm-12 col-xs-12 col-md-7 col-lg-7 col-md-offset-2" style="margin-top:20px">
            <div class="alert alert-success">
                ${param.mess}
            </div>
        </div>
    </div>
</c:if>
<c:if test="${param.err != null}">
    <div class="row">
        <div class="col-sm-12 col-xs-12 col-md-7 col-lg-7 col-md-offset-2" style="margin-top:20px">
            <div class="alert alert-danger">
                ${param.err}
            </div>
        </div>
    </div>
</c:if>
<div class="row">
    <div class="col-sm-12 col-xs-12 col-md-7 col-lg-7 col-md-offset-2" style="margin-top:20px">
        <form class="form-horizontal" action="change-profile-password" method="post">
            <div class="form-group">
                <label class="control-label col-sm-3">Curent password</label>
                <div class="col-sm-6 col-sm-offset-1">
                    <input type="password" name="currentpw" class="form-control"/>
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-sm-3">New password</label>
                <div class="col-sm-6 col-sm-offset-1">
                    <input type="password" id="pw1" name="password" class="form-control"/>
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-sm-3">Re-enter new password</label>
                <div class="col-sm-6 col-sm-offset-1">
                    <input type="password" id="pw2" class="form-control"/>
                </div>
            </div>
            <div class="form-group">
                <div style="text-align: right">
                    <button type="submit" id="submit" class="btn btn-primary">Submit</button>
                </div>
            </div>
        </form>
    </div>
</div>