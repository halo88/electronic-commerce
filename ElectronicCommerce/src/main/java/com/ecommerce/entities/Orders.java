/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ecommerce.entities;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import org.springframework.format.annotation.DateTimeFormat;

/**
 *
 * @author PC
 */
@Entity
@Table(name = "orders")

public class Orders implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int orderID;

    @DateTimeFormat(pattern = "yyyy-MM-dd")
    @Temporal(TemporalType.DATE)
    private Date orderdate;

    private String status;//Delivered / Not delivered/ Canceled
    private Double totalprice;

    @OneToMany(mappedBy = "orders", fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    private List<OrderItems> lstOrderItems;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "username")
    private Users users;

    public Orders() {
    }

    public boolean isCancelable() {
        return this.status.equalsIgnoreCase("not delivered");
    }

    public Double getTotalprice() {
        return totalprice;
    }

    public void setTotalprice(Double totalprice) {
        this.totalprice = totalprice;
    }

    public Users getUsers() {
        return users;
    }

    public void setUsers(Users users) {
        this.users = users;
    }

    public List<OrderItems> getLstOrderItems() {
        return lstOrderItems;
    }

    public void setLstOrderItems(List<OrderItems> lstOrderItems) {
        this.lstOrderItems = lstOrderItems;
    }

    public int getOrderID() {
        return orderID;
    }

    public void setOrderID(int orderID) {
        this.orderID = orderID;
    }

    public Date getOrderdate() {
        return orderdate;
    }

    public void setOrderdate(Date orderdate) {
        this.orderdate = orderdate;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public void calTotalprice() {
        for (OrderItems item : lstOrderItems) {
            item.calCurrentPrice();
        }
        this.totalprice = 0d;
        if (lstOrderItems != null) {
            for (int i = 0; i < lstOrderItems.size(); i++) {
                totalprice += lstOrderItems.get(i).getCurrentPrice()
                        * lstOrderItems.get(i).getQuantity();
            }
        }
    }
}
