/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ecommerce.entities;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;

/**
 *
 * @author hell_lock
 */
@Entity
@Table(name = "group_authorities")
public class GroupAuthorities implements Serializable {

    @Id
    @Column(name = "authority_role", unique = true, columnDefinition = "VARCHAR(50)")
    private String authorityRole;

    @OneToMany(mappedBy = "groupAuth")
    @LazyCollection(LazyCollectionOption.FALSE)
    private List<Authorities> listAuth;

    public GroupAuthorities() {
    }

    public GroupAuthorities(String role) {
        this.authorityRole = role;
    }

    public String getAuthorityRole() {
        return authorityRole;
    }

    public void setAuthorityRole(String authorityRole) {
        this.authorityRole = authorityRole;
    }

    public List<Authorities> getListAuth() {
        return listAuth;
    }

    public void setListAuth(List<Authorities> listAuth) {
        this.listAuth = listAuth;
    }

}
