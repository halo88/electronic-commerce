/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ecommerce.entities;

import java.io.Serializable;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

/**
 *
 * @author PC
 */
@Entity
@Table(name = "manufactures")
public class Manufactures implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int manufactureID;
    
    private String manufactureName;
    
    @OneToMany(mappedBy = "manufacture", fetch = FetchType.EAGER,cascade = CascadeType.ALL)    
    private List<Products> lstProducts;

    public Manufactures() {
    }

    public List<Products> getLstProducts() {
        return lstProducts;
    }

    public void setLstProducts(List<Products> lstProducts) {
        this.lstProducts = lstProducts;
    }

    public int getManufactureID() {
        return manufactureID;
    }

    public void setManufactureID(int manufactureID) {
        this.manufactureID = manufactureID;
    }

    public String getManufactureName() {
        return manufactureName;
    }

    public void setManufactureName(String manufactureName) {
        this.manufactureName = manufactureName;
    }
    
            
}
