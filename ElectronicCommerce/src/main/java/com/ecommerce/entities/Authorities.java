/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ecommerce.entities;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.Table;

/**
 *
 * @author hell_lock
 */
@Entity
@Table(name = "authorities")
public class Authorities implements Serializable{

    @Id
    @Column(name = "username",unique=true,columnDefinition="VARCHAR(50)")
    private String username;
    
    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "authority")
    private GroupAuthorities groupAuth;

    @OneToOne(fetch = FetchType.EAGER)
    @PrimaryKeyJoinColumn
    private Users users;

    public Authorities() {
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public GroupAuthorities getGroupAuth() {
        return groupAuth;
    }

    public void setGroupAuth(GroupAuthorities groupAuth) {
        this.groupAuth = groupAuth;
    }

    public Users getUsers() {
        return users;
    }

    public void setUsers(Users user) {
        this.users = users;
    }

}
