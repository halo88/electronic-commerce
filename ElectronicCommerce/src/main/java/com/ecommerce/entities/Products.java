/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ecommerce.entities;

import java.io.Serializable;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.DateTimeException;
import java.util.Date;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;
import org.springframework.format.annotation.DateTimeFormat;

/**
 *
 * @author hell_lock
 */
@Entity
@Table(name = "products")
public class Products implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int productID;
    private String productName;

    @DateTimeFormat(pattern = "yyyy-MM-dd")
    @Temporal(TemporalType.DATE)
    private Date adddate;
    private String description;
    private long unitPrice;
    private String warranty;
    private int stock;
    private String imageurl;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "categoryID")
    private Categories categories;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "manufactureID")
    private Manufactures manufacture;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "promotionID")
    private Promotions promotions;

    @OneToMany(mappedBy = "products")
    @LazyCollection(LazyCollectionOption.FALSE)
    private List<OrderItems> lstOrderItems;

    @OneToOne(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    @PrimaryKeyJoinColumn
    private Productdescription desc;

    public Products() {
    }

    public boolean isSaling() {
        Date min = this.promotions.getStartdate();
        Date max = this.promotions.getEnddate();
        Date date = new Date();
        try {
            if (min == null || max == null) {
                return false;
            } else {
                return date.after(min) && date.before(max);
            }
        } catch (DateTimeException ex) {
            ex.printStackTrace();
            return false;
        }
    }

    public int getProductID() {
        return productID;
    }

    public void setProductID(int productID) {
        this.productID = productID;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public Date getAdddate() {
        return adddate;
    }

    public void setAdddate(Date adddate) {
        this.adddate = adddate;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public long getUnitPrice() {
        return unitPrice;
    }

    public void setUnitPrice(long unitPrice) {
        this.unitPrice = unitPrice;
    }

    public String getWarranty() {
        return warranty;
    }

    public void setWarranty(String warranty) {
        this.warranty = warranty;
    }

    public int getStock() {
        return stock;
    }

    public void setStock(int stock) {
        this.stock = stock;
    }

    public String getImageurl() {
        return imageurl;
    }

    public void setImageurl(String imageurl) {
        this.imageurl = imageurl;
    }

    public Categories getCategories() {
        return categories;
    }

    public void setCategories(Categories categories) {
        this.categories = categories;
    }

    public Manufactures getManufacture() {
        return manufacture;
    }

    public void setManufacture(Manufactures manufacture) {
        this.manufacture = manufacture;
    }

    public Promotions getPromotions() {
        return promotions;
    }

    public void setPromotions(Promotions promotions) {
        this.promotions = promotions;
    }

    public List<OrderItems> getLstOrderItems() {
        return lstOrderItems;
    }

    public void setLstOrderItems(List<OrderItems> lstOrderItems) {
        this.lstOrderItems = lstOrderItems;
    }

    public Productdescription getDesc() {
        return desc;
    }

    public void setDesc(Productdescription desc) {
        this.desc = desc;
    }

}
