/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ecommerce.model;

import java.io.Serializable;

/**
 *
 * @author PC
 */
public class InformationProductsModel implements Serializable {

    private String CPU;
    private String RAM;
    private String HDD;
    private String OPTICAL;
    private String DISPLAY;
    private String GARAPHIC;
    private String NETWORK;
    private String IO;
    private String WEIGHT;
    private String OS;
    private String WARRANTY;

    public InformationProductsModel() {
    }

    public InformationProductsModel(String CPU, String RAM, String HDD, String OPTICAL, String DISPLAY, String GARAPHIC, String NETWORK, String IO, String WEIGHT, String OS, String WARRANTY) {
        this.CPU = CPU;
        this.RAM = RAM;
        this.HDD = HDD;
        this.OPTICAL = OPTICAL;
        this.DISPLAY = DISPLAY;
        this.GARAPHIC = GARAPHIC;
        this.NETWORK = NETWORK;
        this.IO = IO;
        this.WEIGHT = WEIGHT;
        this.OS = OS;
        this.WARRANTY = WARRANTY;
    }

    public String getCPU() {
        return CPU;
    }

    public void setCPU(String CPU) {
        this.CPU = CPU;
    }

    public String getRAM() {
        return RAM;
    }

    public void setRAM(String RAM) {
        this.RAM = RAM;
    }

    public String getHDD() {
        return HDD;
    }

    public void setHDD(String HDD) {
        this.HDD = HDD;
    }

    public String getOPTICAL() {
        return OPTICAL;
    }

    public void setOPTICAL(String OPTICAL) {
        this.OPTICAL = OPTICAL;
    }

    public String getDISPLAY() {
        return DISPLAY;
    }

    public void setDISPLAY(String DISPLAY) {
        this.DISPLAY = DISPLAY;
    }

    public String getGARAPHIC() {
        return GARAPHIC;
    }

    public void setGARAPHIC(String GARAPHIC) {
        this.GARAPHIC = GARAPHIC;
    }

    public String getNETWORK() {
        return NETWORK;
    }

    public void setNETWORK(String NETWORK) {
        this.NETWORK = NETWORK;
    }

    public String getIO() {
        return IO;
    }

    public void setIO(String IO) {
        this.IO = IO;
    }

    public String getWEIGHT() {
        return WEIGHT;
    }

    public void setWEIGHT(String WEIGHT) {
        this.WEIGHT = WEIGHT;
    }

    public String getOS() {
        return OS;
    }

    public void setOS(String OS) {
        this.OS = OS;
    }

    public String getWARRANTY() {
        return WARRANTY;
    }

    public void setWARRANTY(String WARRANTY) {
        this.WARRANTY = WARRANTY;
    }
    
    

}
