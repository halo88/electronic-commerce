/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ecommerce.service;

import com.ecommerce.entities.Users;
import java.util.Date;
import java.util.List;

/**
 *
 * @author hell_lock
 */
public interface EService {

    public boolean insertUpdateUsers(Users users);

    public List<Users> searchUsersByUsername(String search);

    public List<Users> searchUsersByRealName(String search);

    public List<Users> searchUsersByPhone(String search);

    public List<Users> searchUsersByDob(String search);

    public List<Users> searchUsersByRole(String search);

    public void sendConfirmEmail(String recipientAddress, String subject, String body);
    
    public void sendOrderEmail(String recipientAddress, String subject, StringBuffer body);
}
