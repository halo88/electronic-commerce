/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ecommerce.service.impl;

import com.ecommerce.dao.OrderDao;
import com.ecommerce.entities.Orders;
import com.ecommerce.service.OrderService;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author PC
 */
@Service
public class OrderServiceImplement  implements OrderService{
    
    @Autowired
    private OrderDao orderDao;
    
    @Override
    public Orders insertAndUpdateOrder(Orders orders) {
        return orderDao.insertAndUpdateOrder(orders);
    }

    @Override
    public void delete(int id) {
    }

    @Override
    public Orders findProductsById(int id) {
        return null;
    }

    @Override
    public List<Orders> findAllOrder() {
        return null;
    }

    @Override
    public List<Orders> findByNameUser() {
        return null;
    }

    @Override
    public List<Orders> findByDate() {
        return null;
    }
}
