/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ecommerce.service.impl;

import com.ecommerce.dao.EDao;
import com.ecommerce.entities.Authorities;
import com.ecommerce.entities.GroupAuthorities;
import com.ecommerce.entities.UserDetails;
import com.ecommerce.entities.Users;
import com.ecommerce.service.EService;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.stereotype.Service;

@Service
public class EServiceImpl implements EService {

    @Autowired
    EDao eDao;
    @Autowired
    JavaMailSender jmailSender;

    @Override
    public boolean insertUpdateUsers(Users users) {
        if (users.getUsername().equals("admin")) {
            return false;
        }
        return eDao.insertUpdateUsers(users);
    }

    @Override
    public List<Users> searchUsersByUsername(String search) {
        return eDao.searchUsersByUsername(search);
    }

    @Override
    public List<Users> searchUsersByRealName(String search) {
        List<UserDetails> listUserDetails = eDao.searchUserDetailsByRealName(search);
        List<Users> listUsers = new ArrayList<>();
        for (UserDetails each : listUserDetails) {
            listUsers.add(each.getUsers());
        }
        return listUsers;
    }


    @Override
    public List<Users> searchUsersByPhone(String search) {
        List<UserDetails> listUserDetails = eDao.searchUserDetailsByPhone(search);
        List<Users> listUsers = new ArrayList<>();
        for (UserDetails each : listUserDetails) {
            listUsers.add(each.getUsers());
        }
        return listUsers;
    }

    @Override
    public List<Users> searchUsersByDob(String search) {
// Parsing String to java.util.Date
        String startDateString = search;
        DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
        Date startDate = null;
        try {
            startDate = df.parse(startDateString);
            String newDateString = df.format(startDate);
            System.out.println(newDateString);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        List<Users> listUsers = new ArrayList<>();
        if (startDate != null) {
            java.sql.Date dbDate = new java.sql.Date(startDate.getTime());
            List<UserDetails> listUserDetails = eDao.searchUserDetailsByDob(dbDate);
            for (UserDetails each : listUserDetails) {
                listUsers.add(each.getUsers());
            }
        }
        return listUsers;
    }

    @Override
    public List<Users> searchUsersByRole(String search) {
        GroupAuthorities groupAuth = eDao.searchGroupAuthoritiesByRole(search);
        List<Users> listUsers = new ArrayList<>();
        if (groupAuth != null) {
            List<Authorities> listAuthorities = groupAuth.getListAuth();
            for (Authorities each : listAuthorities) {
                listUsers.add(each.getUsers());
            }
        }
        return listUsers;
    }

    @Override
    public void sendConfirmEmail(String recipientAddress, String subject, String body) {
        SimpleMailMessage mailMess = new SimpleMailMessage();
        mailMess.setTo(recipientAddress);
        mailMess.setSubject(subject);
        mailMess.setText(body+"\n\n\nECommerce project");
        jmailSender.send(mailMess);
    }

    @Override
    public void sendOrderEmail(String recipientAddress, String subject, StringBuffer body) {
        SimpleMailMessage mailMess = new SimpleMailMessage();
        mailMess.setTo(recipientAddress);
        mailMess.setSubject(subject);
        mailMess.setText(body+"\n\n\nECommerce project");
        jmailSender.send(mailMess);
    }
}
