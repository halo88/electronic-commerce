/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ecommerce.service.impl;

import com.ecommerce.controller.GuestController;
import com.ecommerce.dao.ProductsDao;
import com.ecommerce.entities.Categories;
import com.ecommerce.entities.Manufactures;
import com.ecommerce.entities.Products;
import com.ecommerce.repository.ProductsRepository;
import com.ecommerce.service.ProductsService;
import java.io.BufferedOutputStream;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.http.HttpSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.commons.CommonsMultipartFile;

/**
 *
 * @author PC
 */
@Service
public class ProductServiceImplement implements ProductsService {

    @Autowired
    private ProductsDao productsDao;
    @Autowired
    private ProductsRepository productsRepo;

    @Override
    public Products insertAndUpdateProduct(Products product) {
        return productsDao.insertAndUpdateProduct(product);
    }

    @Override
    public void delete(int id) {
        productsDao.delete(id);
    }

    @Override
    public Products findProductsById(int id) {
        return productsDao.findProductsById(id);
    }

    @Override
    public List<Products> findAllProducts() {
        return productsDao.findAllProducts();
    }

    @Override
    public List<Products> findByNameProducts(String name) {
        return productsDao.findByNameProducts(name);
    }

    @Override
    public List<Products> findByUnitPriceProducts(double price) {
        return null;
    }

    @Override
    public String doUpload(CommonsMultipartFile file, HttpSession session) {
        Properties p = new Properties();
        String path = session.getServletContext().getRealPath("/WEB-INF/");
        String filename = file.getOriginalFilename();
        String extension = filename.split("\\.")[filename.split("\\.").length - 1]; //get file extension
        String nametosave = "";
        String projectdir = "";
        String host = "";
        boolean t = false;
        FileInputStream in = null;
        FileOutputStream out = null;
        try {
            in = new FileInputStream(path + "static-prop.properties");
            p.load(in);//load the first time to get host dir
            projectdir = p.getProperty("projectdir");
            in = new FileInputStream(projectdir + "uploadfile.properties");
            p.load(in);
            nametosave = p.getProperty("imagecount");
//            host = projectdir + "ImageHost/";
            host = p.getProperty("host");
            p.replace("imagecount", Integer.parseInt(nametosave) + 1 + "");
            out = new FileOutputStream(projectdir + "uploadfile.properties");
            p.store(out, null);
            t = true;
        } catch (FileNotFoundException ex) {
            ex.printStackTrace();
        } catch (IOException | NumberFormatException ex) {
            ex.printStackTrace();
        } finally {
            try {
                if (in != null) {
                    in.close();
                }
                if (out != null) {
                    out.close();
                }
            } catch (IOException ex) {
                Logger.getLogger(GuestController.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        byte barr[] = file.getBytes();
        BufferedOutputStream bout;
        try {
            t = false;
            filename = nametosave + "." + extension;
            bout = new BufferedOutputStream(
                    new FileOutputStream(host + filename));
            bout.write(barr);
            bout.flush();
            bout.close();
            t = true;
        } catch (FileNotFoundException ex) {
            ex.printStackTrace();
        } catch (IOException ex) {
            ex.printStackTrace();
        }
        if (t) {
            return filename;
        } else {
            return null;
        }
    }

    @Override
    public List<Products> getProductPage(List<Products> list, int page, int size) {
        List<Products> result = new ArrayList<>();
        if ((page - 1) * size > list.size()
                || page < 1) {
            return null;
        }
        int startindex = (page - 1) * size;
        int endindex = startindex + size;
        int remain = list.size() % size;
        if (remain > 0) {
            if (page == list.size() / size + 1) {
                endindex = startindex + remain;
            }
        }
        for (int i = startindex; i < endindex; i++) {
            if (list.get(i) != null) {
                result.add(list.get(i));
            }
        }
        return result;
    }

    @Override
    public List<Products> getAllProducts(String search) {
        List<Products> result = new ArrayList<>();
        result.addAll(productsDao.findByProductName(search));
        List<Manufactures> listMan = productsDao.findByManufactureName(search);
        for (int i = 0; i < listMan.size(); i++) {
            result.addAll(listMan.get(i).getLstProducts());
        }
        List<Categories> listCat = productsDao.findByCategoryName(search);
        for (int i = 0; i < listCat.size(); i++) {
            result.addAll(listCat.get(i).getLstProducts());
        }
        result = filterdDistinctProducts(result);
        
        return result;
    }

    @Override
    public List<Products> filterdDistinctProducts(List<Products> list) {
        for (int i = 0; i < list.size() - 1; i++) {
            for (int j = i + 1; j < list.size(); j++) {
                if (list.get(i).getProductID() == list.get(j).getProductID()) {
                    list.remove(j);
                    j--;
                }
            }
        }
        return list;
    }
}
