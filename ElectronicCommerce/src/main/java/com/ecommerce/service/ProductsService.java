/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ecommerce.service;

import com.ecommerce.entities.Products;
import java.util.List;
import javax.servlet.http.HttpSession;
import org.springframework.web.multipart.commons.CommonsMultipartFile;

/**
 *
 * @author PC
 */
public interface ProductsService {

    public Products insertAndUpdateProduct(Products products);

    public void delete(int id);

    public Products findProductsById(int id);

    public List<Products> findAllProducts();

    public List<Products> findByNameProducts(String name);

    public List<Products> findByUnitPriceProducts(double price);

    public String doUpload(CommonsMultipartFile file, HttpSession session);

    public List<Products> getProductPage(List<Products> list, int page, int size);

    public List<Products> getAllProducts(String search);

    public List<Products> filterdDistinctProducts(List<Products> list);
}
