/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ecommerce.service;

import com.ecommerce.entities.Orders;
import java.util.List;

/**
 *
 * @author PC
 */
public interface OrderService {
    public Orders insertAndUpdateOrder(Orders orders);
    
    public void delete(int id);
    
    public Orders findProductsById(int id);
    
    public  List<Orders> findAllOrder();
    
    public  List<Orders> findByNameUser();
    
    public  List<Orders> findByDate();
}
