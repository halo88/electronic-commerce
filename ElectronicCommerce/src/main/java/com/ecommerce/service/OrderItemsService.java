/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ecommerce.service;

import com.ecommerce.entities.OrderItems;
import java.util.List;

/**
 *
 * @author PC
 */
public interface OrderItemsService {
    public OrderItems insertAndUpdateOrder(OrderItems orderItems);
    
    public void delete(int id);
    
    public OrderItems findProductsById(int id);
    
    public  List<OrderItems> findAllOrderItems();
    
    public  List<OrderItems> findByNameUser();
    
    public  List<OrderItems> findByDate();
}
