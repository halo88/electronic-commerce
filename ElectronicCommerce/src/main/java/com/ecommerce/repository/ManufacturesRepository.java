/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ecommerce.repository;

import com.ecommerce.entities.Manufactures;
import java.util.List;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

/**
 *
 * @author PC
 */
@Repository
public interface ManufacturesRepository extends CrudRepository<Manufactures, Integer> {

    public List<Manufactures> findByManufactureNameContaining(String name);

    public List<Manufactures> findByManufactureName(String name);
}
