/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ecommerce.repository;

import com.ecommerce.entities.Products;
import java.util.List;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.domain.Page;

import org.springframework.stereotype.Repository;

/**
 *
 * @author PC
 */
@Repository
public interface ProductsRepository extends JpaRepository<Products, Integer> {

    public List<Products> findByProductNameStartingWith(String name);

    public List<Products> findByProductNameContaining(String name);

    public Page<Products> findByProductNameContainingOrDescriptionContainingOrManufactureManufactureNameContainingOrCategoriesCategoryNameContaining(
            String name1, String name2, String name3, String name4, Pageable pageable);
}
