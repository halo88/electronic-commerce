/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ecommerce.repository;

import com.ecommerce.entities.OrderItems;
import java.io.Serializable;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author PC
 */
@Repository
public interface OrderItemsRepository extends CrudRepository<OrderItems, Integer> {

    @Transactional
    @Modifying
    @Query(value = "delete from OrderItems u where u.id=?1", nativeQuery = false)
    public void deleteNative(int id);
}
