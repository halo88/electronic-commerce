/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ecommerce.repository;

import com.ecommerce.entities.UserDetails;
import com.ecommerce.entities.Users;
import java.util.List;
import org.springframework.data.repository.CrudRepository;

/**
 *
 * @author hell_lock
 */
public interface UsersRepository extends CrudRepository<Users, String> {

    public List<Users> findByUsernameContaining(String search);

    public List<Users> findByEnabled(String search);

    public List<Users> findByEnabledTrue();

    public List<Users> findByEnabledFalse();

    public Users findByConfirmcode(String cibfirmcode);

    public Users findByUsername(String username);

    public Users findByUserDetailsEmail(String email);
}
