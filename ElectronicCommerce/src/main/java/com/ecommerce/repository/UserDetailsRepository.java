/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ecommerce.repository;

import com.ecommerce.entities.UserDetails;
import java.sql.Date;
import java.util.List;
import org.springframework.data.repository.CrudRepository;

/**
 *
 * @author hell_lock
 */
public interface UserDetailsRepository extends CrudRepository<UserDetails, String> {

    public List<UserDetails> findByFirstnameContainingOrLastnameContaining(String search, String search2);

    public List<UserDetails> findByPhoneContaining(String search);

    public List<UserDetails> findByDob(Date search);

    public List<UserDetails> findByEmail(String email);
}
