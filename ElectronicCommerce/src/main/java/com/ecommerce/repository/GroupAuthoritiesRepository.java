/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ecommerce.repository;

import com.ecommerce.entities.GroupAuthorities;
import java.util.List;
import org.springframework.data.repository.CrudRepository;

/**
 *
 * @author hell_lock
 */
public interface GroupAuthoritiesRepository
        extends CrudRepository<GroupAuthorities, String> {

    public List<GroupAuthorities> findByAuthorityRoleContaining(String search);
}
