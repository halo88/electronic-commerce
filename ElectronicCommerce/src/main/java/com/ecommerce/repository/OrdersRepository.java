/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ecommerce.repository;

import com.ecommerce.entities.Orders;
import java.util.List;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

/**
 *
 * @author PC
 */
@Repository
public interface OrdersRepository extends CrudRepository<Orders, Integer> {

    public List<Orders> findByUsersUsername(String username);
}
