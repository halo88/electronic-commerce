/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ecommerce.controller;

import com.ecommerce.entities.Authorities;
import com.ecommerce.entities.GroupAuthorities;
import com.ecommerce.entities.Manufactures;
import com.ecommerce.entities.Categories;
import com.ecommerce.entities.Orders;
import com.ecommerce.entities.Products;
import com.ecommerce.entities.UserDetails;
import com.ecommerce.entities.Users;
import com.ecommerce.repository.AuthoritiesRepository;
import com.ecommerce.repository.GroupAuthoritiesRepository;
import com.ecommerce.repository.ManufacturesRepository;
import com.ecommerce.repository.UserDetailsRepository;
import com.ecommerce.repository.UsersRepository;
import com.ecommerce.service.EService;
import java.util.ArrayList;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;
import com.ecommerce.repository.CategoriesRepository;
import com.ecommerce.repository.OrdersRepository;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestParam;

/**
 *
 * @author hell_lock
 */
@Controller
@RequestMapping(value = "/admin")
public class AdminController {

    @Autowired
    private UsersRepository usersRepo;
    @Autowired
    private GroupAuthoritiesRepository groupAuthRepo;
    @Autowired
    private AuthoritiesRepository authRepo;
    @Autowired
    private ManufacturesRepository manRepo;
    @Autowired
    private CategoriesRepository catRepo;
    @Autowired
    private UserDetailsRepository detailsRepo;
    @Autowired
    private EService eService;
    @Autowired
    private OrdersRepository ordersRepo;

    @RequestMapping(value = "manage-users", method = RequestMethod.GET)
    public ModelAndView showAllUsers() {
        ModelAndView mv = new ModelAndView("admin-manage-users");
        List<Users> list = (List<Users>) usersRepo.findAll();
        List<UserDetails> listUserDetail = (List<UserDetails>) detailsRepo.findAll();
        List<Authorities> listAuthor = (List<Authorities>) authRepo.findAll();

        mv.addObject("listUserDetail", listUserDetail);
        mv.addObject("listAuthor", listAuthor);
//        mv.addObject("title", "View all users");
        mv.addObject("list", list);
        return mv;
    }

    @RequestMapping(value = "/delete-user/{username}", method = RequestMethod.GET)
    public String deleteUser(@ModelAttribute(name = "username") String username) {
        usersRepo.delete(username);
        return "redirect:/admin/manage-users";
    }

    @RequestMapping(value = "/edit-user/{username}", method = RequestMethod.GET)
    public ModelAndView editUsers(@ModelAttribute("username") String username) {
        ModelAndView mv = new ModelAndView();
        Users user = usersRepo.findOne(username);
        List<GroupAuthorities> listGroupAuth = (List<GroupAuthorities>) groupAuthRepo.findAll();
        mv.addObject("listGroupAuth", listGroupAuth);
        mv.addObject("user", user);
        mv.setViewName("admin-edit-user-form");
        return mv;
    }

    @RequestMapping(value = "/edit-user/edit-users", method = RequestMethod.POST)
    public String doEdit(Users users, Model model) {
        String nameView = "redirect:/admin/edit-user";
        boolean result = eService.insertUpdateUsers(users);
        if (result != false) {
            nameView = "redirect:/admin/manage-users";
        } else {
//            model.addAttribute("products", products);
            model.addAttribute("message", "update product error.");
        }
        return nameView;
    }

//    @RequestMapping(value = "edit-users", method = RequestMethod.POST)
//    public ModelAndView doEditUser(Users users) {
//        ModelAndView mv = new ModelAndView();
//        String viewName;
//        if (eService.insertUpdateUsers(users)) {
//            viewName = "redirect:/admin/manage-users?message=Edit+User+Success";
//        } else {
//            viewName = "redirect:/admin/manage-users?message=Edit+User+FAILED";
//        }
//        mv.setViewName(viewName);
//        return mv;
//    }
    @RequestMapping(value = "search-users", method = RequestMethod.GET)
    public ModelAndView searchUsers(@ModelAttribute("search") String search,
            @ModelAttribute("searchType") int searchType) {
        ModelAndView mv = new ModelAndView("admin-manage-users");
        List<Users> listUsers = new ArrayList<>();
        String type = "";
        switch (searchType) {
            case 1:
                type = "Username";
                listUsers = eService.searchUsersByUsername(search);
                break;
            case 2:
                type = "Real Name";
                listUsers = eService.searchUsersByRealName(search);
                break;
            case 3:
                type = "Enabled";
                listUsers = usersRepo.findByEnabledTrue();
                break;
            case 4:
                type = "Phone";
                listUsers = eService.searchUsersByPhone(search);
                break;
            case 5:
                type = "Date of birth";
                listUsers = eService.searchUsersByDob(search);
                break;
            case 6:
                type = "Role";
                listUsers = eService.searchUsersByRole(search);
                break;
            case 7:
                type = "Disabled";
                listUsers = usersRepo.findByEnabledFalse();
                break;
            default:
                type = "Username";
                listUsers = eService.searchUsersByUsername(search);
                break;
        }
        mv.addObject("listUsers", listUsers);
        mv.addObject("title", "Result for " + type + " " + search);
        return mv;
    }

    @RequestMapping(value = "manage-manufactures", method = RequestMethod.GET)
    public ModelAndView manageManu() {
        ModelAndView mv = new ModelAndView("admin-manage-manufactures");
        List<Manufactures> listManu = (List<Manufactures>) manRepo.findAll();
        mv.addObject("title", "View all manufactures");
        mv.addObject("listManu", listManu);
        return mv;
    }

//AJAX BEGIN
    @RequestMapping(value = "edit-manu", method = RequestMethod.POST,
            produces = "text/plain;charset=UTF-8")
    @ResponseBody
    public String editMan2(@RequestBody Manufactures man) {
        manRepo.save(man);
        return man.getManufactureName();
    }

    @RequestMapping(value = "doedit-manu/{id}/{name}", method = RequestMethod.POST,
            produces = "text/plain;charset=UTF-8")
    @ResponseBody
    public String doEditManuAjax(@PathVariable("id") int id,
            @PathVariable("name") String newName) {
        Manufactures man = manRepo.findOne(id);
        man.setManufactureName(newName);
        manRepo.save(man);
        return man.getManufactureName();
    }

    @RequestMapping(value = "edit-type", method = RequestMethod.POST,
            produces = "text/plain;charset=UTF-8")
    @ResponseBody
    public String doEditTypeAjax(@RequestBody Categories cat) {
        return catRepo.save(cat).getCategoryName();
    }

//END AJAX
    @RequestMapping(value = "doremove-manu/{id}", method = RequestMethod.GET)
    public String doRemovetManuAjax(@PathVariable("id") int id) {
        List<Products> listProducts = manRepo.findOne(id).getLstProducts();
        for (Products pro : listProducts) {
            pro.setManufacture(manRepo.findByManufactureName("Other").get(0));
        }
        manRepo.delete(id);
        return "redirect:/admin/manage-manufactures";
    }

    @RequestMapping(value = "doremove-type/{id}", method = RequestMethod.GET)
    public String doRemovetTypeAjax(@PathVariable("id") int id) {
        List<Products> listProducts = catRepo.findOne(id).getLstProducts();
        for (Products pro : listProducts) {
            pro.setCategories(catRepo.findByCategoryName("Other").get(0));
        }
        catRepo.delete(id);
        return "redirect:/admin/manage-cate";
    }

    @RequestMapping(value = "edit-manufactures/insert-manufacture", method = RequestMethod.POST,
            produces = "text/plain;charset=UTF-8")
    public ModelAndView doInsertManu(Manufactures manu) {
        ModelAndView mv = new ModelAndView();
        manRepo.save(manu);
        mv.setViewName("redirect:/admin/manage-manufactures");
        return mv;
    }

    @RequestMapping(value = "manage-cate", method = RequestMethod.GET)
    public ModelAndView manageTypes() {
        ModelAndView mv = new ModelAndView("admin-manage-types");
        List<Categories> listTypes = (List<Categories>) catRepo.findAll();
        mv.addObject("title", "View all categories");
        mv.addObject("listTypes", listTypes);
        return mv;
    }

    @RequestMapping(value = "edit-types/insert-type", method = RequestMethod.POST)
    public ModelAndView doInsertType(Categories type) {
        ModelAndView mv = new ModelAndView();
        catRepo.save(type);
        mv.setViewName("redirect:/admin/manage-cate");
        return mv;
    }

    @RequestMapping(value = "setting", method = RequestMethod.GET)
    public ModelAndView adminSetting() {
        ModelAndView mv = new ModelAndView("admin-setting");
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        String username = auth.getName();
        List<Orders> listOrders = ordersRepo.findByUsersUsername(username);
        mv.addObject("listOrders", listOrders);
        mv.addObject("user", usersRepo.findOne(username));
        return mv;
    }

    @RequestMapping(value = "edit-personal", method = RequestMethod.POST)
    public ModelAndView editPersonalInfo(UserDetails userDetails) {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        String username = auth.getName();
        userDetails.setUsername(username);
        detailsRepo.save(userDetails);
        return new ModelAndView("redirect:/admin/setting");
    }

    @RequestMapping(value = "/change-profile-password", method = RequestMethod.GET)
    public String changeProfilePw() {
        return "change-password";
    }

    @RequestMapping(value = "/change-profile-password", method = RequestMethod.POST)
    public ModelAndView doChangeProfilePw(@RequestParam("currentpw") String currentpw,
            @RequestParam("password") String newPassword) {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        String username = auth.getName();
        Users user = usersRepo.findOne(username.toLowerCase());
        if (user.getPassword().equals(currentpw)) {
            user.setPassword(newPassword);
            usersRepo.save(user);
            return new ModelAndView("redirect:/admin/change-profile-password?mess=Password+changed!");
        } else {
            return new ModelAndView("redirect:/admin/change-profile-password?err=Current+password+wrong!");
        }
    }
}
