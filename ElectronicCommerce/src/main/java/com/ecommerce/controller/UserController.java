/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ecommerce.controller;

import com.ecommerce.entities.Products;
import com.ecommerce.entities.Users;
import com.ecommerce.repository.UserDetailsRepository;
import com.ecommerce.repository.UsersRepository;
import com.ecommerce.service.ProductsService;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

/**
 *
 * @author hell_lock
 */
@Controller
@RequestMapping(value = "user")
public class UserController {

    @Autowired
    private UsersRepository usersRepository;
    @Autowired
    private UserDetailsRepository detailsRepo;

    @RequestMapping(value = "setting", method = RequestMethod.GET)
    public ModelAndView sellerSetting() {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        String username = auth.getName();
        return new ModelAndView("admin-setting", "user", usersRepository.findOne(username));
    }

    @RequestMapping(value = "edit-personal", method = RequestMethod.POST)
    public ModelAndView editPersonalInfo(com.ecommerce.entities.UserDetails userDetails) {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        String username = auth.getName();
        userDetails.setUsername(username);
        detailsRepo.save(userDetails);
        return new ModelAndView("redirect:/user/setting");
    }
    @RequestMapping(value = "change-profile-password", method = RequestMethod.GET)
    public String changeProfilePw() {
        return "change-password";
    }

    @RequestMapping(value = "change-profile-password", method = RequestMethod.POST)
    public ModelAndView doChangeProfilePw(@RequestParam("currentpw") String currentpw,
            @RequestParam("password") String newPassword) {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        String username = auth.getName();
        Users user = usersRepository.findOne(username.toLowerCase());
        if (user.getPassword().equals(currentpw)) {
            user.setPassword(newPassword);
            usersRepository.save(user);
            return new ModelAndView("redirect:/user/change-profile-password?mess=Password+changed!");
        } else {
            return new ModelAndView("redirect:/user/change-profile-password?err=Current+password+wrong!");
        }
    }
}
