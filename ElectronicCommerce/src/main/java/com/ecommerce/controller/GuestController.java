/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ecommerce.controller;

import com.ecommerce.entities.OrderItems;
import com.ecommerce.entities.Orders;
import com.ecommerce.entities.Products;
import com.ecommerce.entities.Users;
import com.ecommerce.model.Cart;
import com.ecommerce.repository.AuthoritiesRepository;
import com.ecommerce.repository.GroupAuthoritiesRepository;
import com.ecommerce.repository.OrderItemsRepository;
import com.ecommerce.repository.OrdersRepository;
import com.ecommerce.repository.ProductsRepository;
import com.ecommerce.repository.UserDetailsRepository;
import com.ecommerce.repository.UsersRepository;
import com.ecommerce.service.EService;
import com.ecommerce.service.OrderService;
import com.ecommerce.service.ProductsService;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.security.authentication.AnonymousAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

/**
 *
 * @author hell_lock
 */
@Controller
public class GuestController {

    @Autowired
    private UsersRepository usersRepo;
    @Autowired
    private GroupAuthoritiesRepository groupAuthRepo;
    @Autowired
    private AuthoritiesRepository authRepo;
    @Autowired
    private ProductsService productsService;
    @Autowired
    private EService eService;
    @Autowired
    private ProductsRepository productsRepo;
    @Autowired
    private UserDetailsRepository userDetailsRepo;
    @Autowired
    private OrderService orderService;
    @Autowired
    private OrderItemsRepository orderItemsRepository;
    @Autowired
    private OrdersRepository ordersRepository;

//    @RequestMapping(value = "/home")
//    public String showHome() {
//        return "home";
//    }
    @RequestMapping(value = "/login")
    public ModelAndView showLogin() {
        String viewName;
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        if (auth != null
                && auth.isAuthenticated()
                && //when Anonymous Authentication is enabled
                !(auth instanceof AnonymousAuthenticationToken)) {
            viewName = "redirect:/home";
        } else {
            viewName = "login";
        }
        ModelAndView mv = new ModelAndView(viewName);
        mv.addObject("hideSearch", "hideSearch");
        return mv;
    }

    @RequestMapping(value = "register", method = RequestMethod.GET)
    public ModelAndView showRegisterForm() {
        ModelAndView mv = new ModelAndView("register-form");
        mv.addObject("hideSearch", "hideSearch");
        return mv;
    }

    @RequestMapping(value = "register", method = RequestMethod.POST)
    public ModelAndView doRegister(@Valid Users user, BindingResult result,
            HttpServletRequest req) {
        if (result.hasErrors()) {
            return new ModelAndView("register-form");
        }
        user.getUserDetails().setEmail(user.getUserDetails().getEmail().toLowerCase());
        if (usersRepo.findByUsername(user.getUsername()) != null) {
            return new ModelAndView("redirect:/register?err=Someone+has+used+this+username!");
        }
        if (userDetailsRepo.findByEmail(user.getUserDetails().getEmail()).size() != 0) {
            return new ModelAndView("redirect:/register?err=Someone+has+used+this+email!");
        }
        user.setUsername(user.getUsername().toLowerCase());
        user.getAuth().setUsername(user.getUsername());//we dont use auto_increment so we must do this
        user.getUserDetails().setUsername(user.getUsername());//we dont use auto_increment so we must do this
        user.setEnabled(false);
        String confirmcode = user.getUsername().hashCode() + "" + new Date().hashCode();
        user.setConfirmcode(confirmcode);
        usersRepo.save(user);
        String url = req.getRequestURL().toString();
        String link = url + "/confirm/" + confirmcode;
        eService.sendConfirmEmail(user.getUserDetails().getEmail(), "Confirm your account", link);
        return new ModelAndView("redirect:/login?mess=Register+success,+please+check+your+mailbox+to+confirm+your+account!");
    }

    @RequestMapping(value = "register/confirm/{code}")
    public ModelAndView confirmAcc(@PathVariable("code") String code) {
        Users user = usersRepo.findByConfirmcode(code);
        if (user != null) {
            user.setEnabled(true);
            user.setConfirmcode(null);
            usersRepo.save(user);
            if (user.isEnabled()) {
                return new ModelAndView("redirect:/login", "mess", "Account successfully confirmed!");
            } else {
                return new ModelAndView("redirect:/login", "error", "Error, your account was not confirmed!!");
            }
        } else {
            return new ModelAndView("redirect:/login", "error", "Invalid confirm code!");
        }
    }
//    @RequestMapping(value = "/home")
//    public ModelAndView showHomeDefault() {
//        ModelAndView mv = new ModelAndView();
//        List<Products> lst = productsService.findAllProducts();
//        int pageTotal = (lst.size() % 4) > 0 ? lst.size() / 4 + 1 : lst.size() / 4;
//        lst = productsService.getProductPage(lst, 1, 4);
//        List<String> listPage = new ArrayList<>();
//        for (int i = 1; i <= pageTotal; i++) {
//            listPage.add(i + "");
//        }
//        mv.addObject("listProduct", lst);
//        mv.addObject("listPage", listPage);
//        mv.addObject("page", 1);
//        mv.setViewName("home");
//        return mv;
//    }
//
//    @RequestMapping(value = "/home/{page}")
//    public ModelAndView showHome(@PathVariable(name = "page", required = false) int page) {
//        ModelAndView mv = new ModelAndView();
//        List<Products> lst = productsService.findAllProducts();
//        int pageTotal = (lst.size() % 4) > 0 ? lst.size() / 4 + 1 : lst.size() / 4;
//        lst = productsService.getProductPage(lst, page, 4);
//        List<String> listPage = new ArrayList<>();
//        for (int i = 1; i <= pageTotal; i++) {
//            listPage.add(i + "");
//        }
//        mv.addObject("page", page);
//        mv.addObject("listPage", listPage);
//        mv.addObject("listProduct", lst);
//        mv.setViewName("home");
//        return mv;
//    }

    @RequestMapping(value = "/home", method = RequestMethod.GET)
    public ModelAndView showHomeJPADefault(HttpSession session) {
        ModelAndView mv = new ModelAndView("home");
        if (session.getAttribute("sort") == null) {
            session.setAttribute("sort", "unitPrice");
        }
        PageRequest pr = new PageRequest(0, 8, Sort.Direction.ASC, "unitPrice");
        Page<Products> pageProduct = productsRepo.findAll(pr);
        List<Products> data = pageProduct.getContent();
        mv.addObject("page", 1);
        mv.addObject("totalItems", pageProduct.getTotalElements());
        mv.addObject("listProduct", data);
        return mv;
    }

    @RequestMapping(value = "/home/{page}/{sort}", method = RequestMethod.GET)
    public ModelAndView showHomeJPA(@PathVariable("page") int page,
            @PathVariable("sort") String sort, HttpSession session) {
        ModelAndView mv = new ModelAndView("home");
        session.setAttribute("sort", sort);
        PageRequest pr = new PageRequest(page - 1, 8, Sort.Direction.ASC, sort);
        Page<Products> pageProduct = productsRepo.findAll(pr);
        List<Products> data = pageProduct.getContent();
        mv.addObject("page", page);
        mv.addObject("totalItems", pageProduct.getTotalElements());
        mv.addObject("listProduct", data);
        return mv;
    }

    @RequestMapping(value = "/search-result/{page}/{sort}", method = RequestMethod.GET)
    public ModelAndView showSearchJPA(@PathVariable("page") int page,
            HttpSession session, @PathVariable("sort") String sort,
            @RequestParam(name = "search", defaultValue = "") String search) {
        ModelAndView mv = new ModelAndView("search-result");
        if (!search.equals("")) {
            session.setAttribute("searchkey", search);
        } else {
            search = (String) session.getAttribute("searchkey");
        }
        session.setAttribute("sort", sort);
        PageRequest pr = new PageRequest(page - 1, 8, Sort.Direction.ASC, sort);
        List<Products> data = productsRepo.findByProductNameContainingOrDescriptionContainingOrManufactureManufactureNameContainingOrCategoriesCategoryNameContaining(search, search, search, search, pr).getContent();
        int totalItems = data.size();
        mv.addObject("page", page);
        mv.addObject("totalItems", totalItems);
        mv.addObject("listProduct", data);
        return mv;
    }

//    functions for upload images, do not touch
//    @RequestMapping(value = "uploadform", method = RequestMethod.GET)
//    public String uploadForm() {
//        return "upload-form";
//    }
//
//    @RequestMapping(value = "/savefile", method = RequestMethod.POST)
//    public ModelAndView upload(@RequestParam CommonsMultipartFile file, HttpSession session) {
//        Properties p = new Properties();
//        String path = session.getServletContext().getRealPath("/WEB-INF/");
//        String filename = file.getOriginalFilename();
//        String extension = filename.split("\\.")[filename.split("\\.").length - 1]; //get file extension
//        String nametosave = "";
//        String projectdir = "";
//        String host = "";
//        boolean t = false;
//        FileInputStream in = null;
//        FileOutputStream out = null;
//        try {
//            in = new FileInputStream(path + "static-prop.properties");
//            p.load(in);//load the first time to get host dir
//            projectdir = p.getProperty("projectdir");
//            in = new FileInputStream(projectdir + "uploadfile.properties");
//            p.load(in);
//            nametosave = p.getProperty("imagecount");
//            host = projectdir + "ImageHost/";
//            p.replace("imagecount", Integer.parseInt(nametosave) + 1 + "");
//            out = new FileOutputStream(projectdir + "uploadfile.properties");
//            p.store(out, null);
//            t = t && true;
//        } catch (FileNotFoundException ex) {
//            ex.printStackTrace();
//        } catch (IOException | NumberFormatException ex) {
//            ex.printStackTrace();
//        } finally {
//            try {
//                if (in != null) {
//                    in.close();
//                }
//                if (out != null) {
//                    out.close();
//                }
//            } catch (IOException ex) {
//                Logger.getLogger(GuestController.class.getName()).log(Level.SEVERE, null, ex);
//            }
//        }
//        byte barr[] = file.getBytes();
//        BufferedOutputStream bout;
//        try {
//            filename = nametosave + "." + extension;
//            bout = new BufferedOutputStream(
//                    new FileOutputStream(host + filename));
//            bout.write(barr);
//            bout.flush();
//            bout.close();
//            t = t && true;
//        } catch (FileNotFoundException ex) {
//            ex.printStackTrace();
//        } catch (IOException ex) {
//            ex.printStackTrace();
//        }
//        ModelAndView mv = new ModelAndView();
//        mv.addObject("imagename", filename);
//        mv.addObject("host", host);
//        mv.setViewName("uploadsuccess");
//        return mv;
//    }
    @RequestMapping(value = "testproduct", method = RequestMethod.GET)
    public String uploadForm() {
        return "testproduct";
    }

    @RequestMapping(value = "/forgot-pass", method = RequestMethod.GET)
    public ModelAndView ForgotPass() {
        ModelAndView mv = new ModelAndView();
        mv.setViewName("forgotPassForm");
        return mv;
    }

    @RequestMapping(value = "/forgot-pass", method = RequestMethod.POST)
    public ModelAndView sendForgotPass(@RequestParam("email") String email, HttpServletRequest req) {
        ModelAndView mv = new ModelAndView();
        usersRepo.findByUserDetailsEmail(email);
        Date date = new Date();
        Users user = usersRepo.findByUserDetailsEmail(email);
        if (user == null) {
            mv.setViewName("redirect:/login?error=Account+does+not+exist!");
            return mv;
        }
        String hash = email.hashCode() + "" + date.hashCode();
        user.setConfirmcode(hash);
        usersRepo.save(user);
        String url = req.getRequestURL().toString() + "/" + hash;
        eService.sendConfirmEmail(email, "Password recovery", "Password recovery for username: " + user.getUsername()
                + "\nFollow this link to recover your account\n"
                + url);
        mv.setViewName("redirect:/login?mess=Check+your+mailbox+to+recover+your+account");
        return mv;
    }

    @RequestMapping(value = "/forgot-pass/{code}", method = RequestMethod.GET)
    public ModelAndView doChangePass(@PathVariable(name = "code") String code) {
        ModelAndView mv = new ModelAndView();
        Users user = usersRepo.findByConfirmcode(code);
        if (user == null) {
            mv.setViewName("redirect:/login?err=Broken+link!");
            return mv;
        }
        String username = user.getUsername();
        mv.addObject("username1", username);
        mv.setViewName("recover-password-form");
        return mv;
    }

    @RequestMapping(value = "forgot-pass/change-password", method = RequestMethod.POST)
    public ModelAndView ChangePass(@RequestParam("username") String username,
            @RequestParam("password") String password) {
        ModelAndView mv = new ModelAndView();
        Users user = usersRepo.findByUsername(username);
        user.setPassword(password);
        user.setEnabled(true);
        user.setConfirmcode(null);
        usersRepo.save(user);
        mv.setViewName("redirect:/login?mess=Account+successfully+recovered!");
        return mv;
    }

    @RequestMapping(value = "sendemail", method = RequestMethod.GET)
    public ModelAndView sendeEmail() {
        return new ModelAndView("testsendemail");
    }

    @RequestMapping(value = "sendemail", method = RequestMethod.POST)
    public ModelAndView sendEmail(@RequestParam("recipent") String recipent,
            @RequestParam("subject") String subject,
            @RequestParam("body") String body) {
        eService.sendConfirmEmail(recipent, subject, body);
        return new ModelAndView("redirect:/home");
    }

    @RequestMapping(value = "viewcart")
    public ModelAndView viewCart(HttpSession session) {
        ModelAndView mv = new ModelAndView("myorder");
        Orders cart = (Orders) session.getAttribute("cart");
        if (cart == null) {
            cart = new Orders();
            cart.setLstOrderItems(new ArrayList<OrderItems>());
            session.setAttribute("cart", cart);
        } else {

        }
        cart.calTotalprice();
        cart.setOrderdate(new Date());
        return mv;
    }

    @RequestMapping(value = "checkout")
    public ModelAndView checkout(HttpSession session) {
        ModelAndView mv = new ModelAndView();
        Orders cart = (Orders) session.getAttribute("cart");
        if (cart == null || cart.getLstOrderItems().isEmpty()) {
            mv.setViewName("redirect:/home?err=No items in cart!");
        } else {
            Authentication auth = SecurityContextHolder.getContext().getAuthentication();
            if (auth != null
                    && auth.isAuthenticated()
                    && //when Anonymous Authentication is enabled
                    !(auth instanceof AnonymousAuthenticationToken)) {
                mv.setViewName("redirect:/after-checkout-ok");
            } else {
                mv.setViewName("after-checkout");
            }
        }
        return mv;
    }

    @RequestMapping(value = "after-checkout-ok")
    public ModelAndView checkoutOK(HttpSession session, Users user) {
        ModelAndView mv = new ModelAndView();
        Orders cart = (Orders) session.getAttribute("cart");

        if (cart == null) {
            mv.setViewName("redirect:/home?err=No+items+in+cart!");
        } else {
            Authentication auth = SecurityContextHolder.getContext().getAuthentication();
            if (auth != null
                    && auth.isAuthenticated()
                    && //when Anonymous Authentication is enabled
                    !(auth instanceof AnonymousAuthenticationToken)) {
                user = usersRepo.findByUsername(auth.getName());
            } else {
                Users check = usersRepo.findByUsername(user.getUserDetails().getEmail());
                if (check == null) {
                    user.setUsername(user.getUserDetails().getEmail());
                    user.setPassword(user.getUserDetails().getEmail());
                    user.getUserDetails().setUsername(user.getUserDetails().getEmail());
                    user.setEnabled(true);
                    usersRepo.save(user);
                } else {
                    user = check;
                }
            }
            List<OrderItems> lstOI = cart.getLstOrderItems();
            for (OrderItems each : lstOI) {
                each.getProducts().
                        setStock(each.getProducts().getStock() - each.getQuantity());
            }
            cart.setLstOrderItems(lstOI);
            cart.setUsers(user);
            Orders order = ordersRepository.save(cart);
            session.removeAttribute("cart");
            String test = order.toString();
            eService.sendConfirmEmail(user.getUserDetails().getEmail(), "Order success!", order.toString());
            mv.setViewName("checkout-success");
        }
        return mv;
    }

    @RequestMapping(value = "/remove/{id}")
    public ModelAndView Remove(@ModelAttribute(name = "id") int id, HttpSession session) {
        ModelAndView mv = new ModelAndView();
        Orders cart = (Orders) session.getAttribute("cart");
        if (cart == null) {
            cart = new Orders();
            session.setAttribute("cart", cart);
            mv.setViewName("redirect:/home?err=No+items+in+cart!");
        } else {
            List<OrderItems> lstOI = cart.getLstOrderItems();
            for (OrderItems each : lstOI) {
                if (each.getProducts().getProductID() == id) {
                    lstOI.remove(each);
                    break;
                }
            }
            cart.setLstOrderItems(lstOI);
            session.setAttribute("cart", cart);
            mv.setViewName("redirect:/viewcart");
        }
        return mv;
    }

    @RequestMapping(value = "testvalidator", method = RequestMethod.GET)
    public String initForm(Model model) {
        Users user = new Users();
        model.addAttribute("user", user);
        return "testvalid";
    }

    @ResponseBody
    @RequestMapping(value = "add-to-cart", method = RequestMethod.POST)
    public String addCartAjax(@RequestBody Map<String, String> data, HttpSession session) {
        Orders cartOrder;
        if (session.getAttribute("cart") == null) {
            session.setAttribute("cart", new Orders());
            cartOrder = (Orders) session.getAttribute("cart");
        } else {
            cartOrder = (Orders) session.getAttribute("cart");
        }
        List<OrderItems> listCartItems = cartOrder.getLstOrderItems();
        if (listCartItems == null) {
            listCartItems = new ArrayList<>();
        }
        int productID = Integer.parseInt(data.get("productID"));
        int amount = Integer.parseInt(data.get("amount"));
        boolean addnew = true;
        for (int i = 0; i < listCartItems.size(); i++) {
            if (listCartItems.get(i).getProducts().getProductID() == productID) {
                listCartItems.get(i).setQuantity(listCartItems.get(i).getQuantity() + amount);
                addnew = false;
                break;
            }
        }
        if (addnew) {
            listCartItems.add(new OrderItems(0, amount, productsRepo.findOne(productID), cartOrder));
        }
        String username = SecurityContextHolder.getContext().getAuthentication().getName();
        Users user = usersRepo.findByUsername(username);
        cartOrder.setOrderdate(new Date());
        cartOrder.setStatus("Not delivered");
        cartOrder.setLstOrderItems(listCartItems);
        cartOrder.calTotalprice();
        cartOrder.setUsers(user);
        session.setAttribute("cart", cartOrder);
        StringBuffer result = new StringBuffer("");
        for (OrderItems i : listCartItems) {
            result.append("<li><a>"
                    + i.getProducts().getProductName()
                    + "&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp<span class='badge' style='position: absolute;right:5px'>" + i.getQuantity() + "</span>"
                    + "</a></li>");
        }
        result.append("<hr>\n"
                + "                        <li style=''>"
                + "<a style='padding-bottom:10px;padding-left:28%;color: #428bca;font-weight: bolder' href='" + session.getServletContext().getContextPath() + "/viewcart'>"
                + "Manage cart"
                + "</a></li>*" + cartOrder.getLstOrderItems().size());
        return result.toString();
    }

    @RequestMapping(value = "testvalidator2", method = RequestMethod.POST)
    public ModelAndView submitForm(@Valid Users user, BindingResult result) {
        ModelAndView mv = new ModelAndView();
        String returnVal = "redirect:/testvalidator?success=true";
        if (result.hasErrors()) {
            returnVal = "testvalid";
        }
        mv.setViewName(returnVal);
        return mv;
    }

    @RequestMapping(value = "view-order", method = RequestMethod.GET)
    @ResponseBody
    public String viewMyOrderAjax(@RequestParam("id") int id) {
        Orders order = ordersRepository.findOne(id);
        List<OrderItems> data = order.getLstOrderItems();
        StringBuffer result = new StringBuffer(
                "<table class='table table-bordered'>"
                + "<tr>"
                + "<th>Product</th>"
                + "<th>Quantity</th>"
                + "<th>Unitprice</th>"
                + "<th>Cost</th>"
                + "</tr>");

        for (OrderItems each : data) {
            result.append(""
                    + "<tr>"
                    + "<td>" + each.getProducts().getProductName() + "</td>"
                    + "<td>" + each.getQuantity() + "</td>"
                    + "<td>$" + each.getProducts().getUnitPrice() + "</td>"
                    + "<td>$" + each.getProducts().getUnitPrice() * each.getQuantity() + "</td>"
                    + "</tr>");
        }
        result.append("<tr>"
                + "<th></th>"
                + "<th></th>"
                + "<th>Total</th>"
                + "<td>$" + order.getTotalprice() + "</td>"
                + "</tr>");
        result.append("</table>");
        return result.toString();
    }

    @RequestMapping(value = {"admin/orders", "user/orders", "seller/orders"}, method = RequestMethod.GET)
    public ModelAndView viewOrders() {
        ModelAndView mv = new ModelAndView();
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        String username = auth.getName();
        List<Orders> listOrders = ordersRepository.findByUsersUsername(username);
        mv.addObject("listOrders", listOrders);
        mv.setViewName("view-all-orders");
        return mv;
    }

    @RequestMapping(value = "cancel-order/{id}", method = RequestMethod.GET)
    public ModelAndView doCancelOrders(@PathVariable("id") int id,
            @RequestParam(name = "role") String role) {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        String username = auth.getName();
        List<Orders> listOrders = ordersRepository.findByUsersUsername(username);
        for (Orders or : listOrders) {
            if (or.getOrderID() == id) {
                List<OrderItems> orItems = or.getLstOrderItems();
                for (OrderItems item : orItems) {
                    int stock = item.getProducts().getStock();
                    item.getProducts().setStock(stock + item.getQuantity());
                    OrderItems it = orderItemsRepository.save(item);
                    System.out.println("");
                }
                eService.sendConfirmEmail("halo88801@gmail.com", "[Order canceled] Order #" + id + " was canceled by user",
                        "The order #" + id + " was canceled by user!");
                or.setStatus("Canceled");
                ordersRepository.save(or);
                return new ModelAndView("redirect:/" + role + "/orders?mess=Order Canceled!");
            } else {
                return new ModelAndView("redirect:/" + role + "/orders?err=Error!");
            }
        }
        return null;
    }

    @RequestMapping(value = "/detailproduct/{id}", method = RequestMethod.GET)
    public ModelAndView ShowProductDetail(@ModelAttribute(name = "id") int id) {
        ModelAndView mv = new ModelAndView();
        Products pro = productsService.findProductsById(id);
        List<Products> listProduct = productsService.findAllProducts();
        List< Products> list = new ArrayList<>();
        int i = list.size();
        for (Products product : listProduct) {
            if (product.getProductID() != id) {
                if (i < 4) {
                    list.add(product);
                    i++;
                }
            }
        }
        mv.addObject("pro", pro);
        mv.addObject("list", list);
        mv.setViewName("detail-products");
        return mv;
    }
}
