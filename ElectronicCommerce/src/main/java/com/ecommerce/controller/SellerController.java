/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ecommerce.controller;

import com.ecommerce.entities.Manufactures;
import com.ecommerce.entities.Products;
import com.ecommerce.entities.Promotions;
import com.ecommerce.entities.Categories;
import com.ecommerce.entities.OrderItems;

import com.ecommerce.entities.Users;
import com.ecommerce.model.InformationProductsModel;
import com.ecommerce.repository.ManufacturesRepository;
import com.ecommerce.repository.PromotionsRepository;
import com.ecommerce.repository.UsersRepository;
import com.ecommerce.service.ProductsService;
import java.util.List;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.commons.CommonsMultipartFile;
import org.springframework.web.servlet.ModelAndView;
import com.ecommerce.repository.CategoriesRepository;
import com.ecommerce.repository.OrderItemsRepository;
import com.ecommerce.repository.ProductsRepository;
import com.ecommerce.repository.UserDetailsRepository;
import java.util.ArrayList;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;

/**
 *
 * @author PC
 */
@Controller
//@RequestMapping(value = "/seller")
public class SellerController {

    @Autowired
    private ProductsService productsService;
    @Autowired
    private UsersRepository usersRepository;
    @Autowired
    private ProductsRepository productsRepo;
    @Autowired
    private CategoriesRepository catRepo;
    @Autowired
    private ManufacturesRepository manufacturesRepositorytures;
    @Autowired
    private PromotionsRepository promotionsRepository;
    @Autowired
    private OrderItemsRepository orderItemsRepository;
    @Autowired
    private UserDetailsRepository detailsRepo;
    @Autowired
    private PromotionsRepository promoRepo;

    @RequestMapping(value = "seller/edit-personal", method = RequestMethod.POST)
    public ModelAndView editPersonalInfo(com.ecommerce.entities.UserDetails userDetails) {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        String username = auth.getName();
        userDetails.setUsername(username);
        detailsRepo.save(userDetails);
        return new ModelAndView("redirect:/seller/setting");
    }

    @RequestMapping(value = {"seller/add-product", "admin/add-product"}, method = RequestMethod.GET)
    public ModelAndView addProductForm() {
        ModelAndView mv = new ModelAndView();
        List<Categories> listCat = (List<Categories>) catRepo.findAll();
        List<Promotions> lstPromotions = (List<Promotions>) promotionsRepository.findAll();
        List<Manufactures> lstManufactures = (List<Manufactures>) manufacturesRepositorytures.findAll();
        mv.addObject("listCat", listCat);
        mv.addObject("lstPromotions", lstPromotions);
        mv.addObject("lstManufactures", lstManufactures);
        mv.addObject("addproduct", "addproduct");
        mv.setViewName("add-product-form");
        return mv;
    }

    @RequestMapping(value = "seller/insert-product", method = RequestMethod.POST)
    public String insertProduct(@RequestParam CommonsMultipartFile file,
            HttpSession session, Products product) {
        String url = "";
        if (file.isEmpty()) {
            url = "no-image.jpg";
        } else {
            url = productsService.doUpload(file, session);
        }
        java.sql.Date date = new java.sql.Date(new java.util.Date().getTime());
        product.setAdddate(date);
        product.setImageurl(url);
        product.setPromotions(promoRepo.save(product.getPromotions()));
        productsService.insertAndUpdateProduct(product);
        return "redirect:/seller/manage-products";
    }

//    @RequestMapping(value = "search", method = RequestMethod.POST)
//    public ModelAndView searchProducts(@ModelAttribute(name = "searchName") String name) {
//
//        ModelAndView mv = new ModelAndView();
//        List<Products> lst = productsService.findByNameProducts("%" + name);
//        mv.addObject("lstPro", lst);
//        mv.setViewName("home");
//        return mv;
//    }
//    @RequestMapping(value = "seller/manage-product")
//    public ModelAndView showProduct() {
//        ModelAndView mv = new ModelAndView();
//        List<Products> lst = productsService.findAllProducts();
//        mv.addObject("listProduct", lst);
//        mv.setViewName("manageProduct");
//        return mv;
//    }
    @RequestMapping(value = "seller/setting", method = RequestMethod.GET)
    public ModelAndView sellerSetting() {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        String username = auth.getName();
        return new ModelAndView("admin-setting", "user", usersRepository.findOne(username));
    }

//    @RequestMapping(value = "/detailproduct/{id}", method = RequestMethod.GET)
//    public ModelAndView ShowProductDetail(@ModelAttribute(name = "id") int id) {
//        ModelAndView mv = new ModelAndView();
//        Products pro = productsService.findProductsById(id);
//        List<Products> listProduct = productsService.findAllProducts();
//        List< Products> list = new ArrayList<>();
//        int i = list.size();
//        for (i = 0; i < 4; i++) {
//            list.add(listProduct.get(i));
//        }
//        mv.addObject("pro", pro);
//        mv.addObject("list", list);
//        mv.setViewName("detail-products");
//        return mv;
//    }
    @RequestMapping(value = {"seller/manage-products", "admin/manage-products"}, method = RequestMethod.GET)
    public ModelAndView dataTable() {
        ModelAndView mv = new ModelAndView("seller-manage-products");
        List<Products> list = productsRepo.findAll();
        mv.addObject("lstManufactures", manufacturesRepositorytures.findAll());
        mv.addObject("lstPromotions", promotionsRepository.findAll());
        mv.addObject("listCat", catRepo.findAll());
        mv.addObject("list", list);
        return mv;
    }

    @RequestMapping(value = {"seller/eedit-product", "admin/eedit-product"}, method = RequestMethod.POST)
    public String doEditProduct(@RequestParam(name = "file") CommonsMultipartFile file,
            HttpSession session, Products product, @RequestParam(name = "currentimage") String currentimage) {
        if (!file.isEmpty()) {
            String url = productsService.doUpload(file, session);
            product.setImageurl(url);
        } else {
            product.setImageurl(currentimage);
        }
        product.getDesc().setProductID(product.getProductID());
        Promotions pro = promoRepo.save(product.getPromotions());
        product.setPromotions(pro);
        productsService.insertAndUpdateProduct(product);
        return "redirect:/seller/manage-products";
    }

    @RequestMapping(value = "seller/delete-product/{id}", method = RequestMethod.GET)
    public ModelAndView doDeleteProduct(@PathVariable(name = "id") int id) {
        productsRepo.delete(id);
        return new ModelAndView("redirect:/seller/manage-products?mess=Deleted+one+product.");
    }

    @RequestMapping(value = "/seller/manage-order")
    public ModelAndView managerOrder() {
        ModelAndView mv = new ModelAndView();
        List<OrderItems> list = (List<OrderItems>) orderItemsRepository.findAll();
        mv.addObject("list", list);
        mv.setViewName("seller-manager-order");
        return mv;
    }

    @RequestMapping(value = "/seller/removeorder/{id}", method = RequestMethod.GET)
    public String RemoveOrder(@PathVariable(name = "id") int id) {
        orderItemsRepository.deleteNative(id);       
        return "redirect:/seller/manage-order";
    }

    @RequestMapping(value = "seller/change-profile-password", method = RequestMethod.GET)
    public String changeProfilePw() {
        return "change-password";
    }

    @RequestMapping(value = "seller/change-profile-password", method = RequestMethod.POST)
    public ModelAndView doChangeProfilePw(@RequestParam("currentpw") String currentpw,
            @RequestParam("password") String newPassword) {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        String username = auth.getName();
        Users user = usersRepository.findOne(username.toLowerCase());
        if (user.getPassword().equals(currentpw)) {
            user.setPassword(newPassword);
            usersRepository.save(user);
            return new ModelAndView("redirect:/seller/change-profile-password?mess=Password+changed!");
        } else {
            return new ModelAndView("redirect:/seller/change-profile-password?err=Current+password+wrong!");
        }
    }
}
