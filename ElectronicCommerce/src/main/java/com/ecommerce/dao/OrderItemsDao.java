/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ecommerce.dao;

import com.ecommerce.entities.OrderItems;
import com.ecommerce.entities.Orders;
import java.util.List;

/**
 *
 * @author PC
 */
public interface OrderItemsDao {
     public OrderItems insertAndUpdateOrder(OrderItems orderItems);
    
    public void delete(int id);
    
    public OrderItems findProductsById(int id);
    
    public  List<OrderItems> findAllOrderItems();
    
    public  List<OrderItems> findByNameUser();
    
    public  List<OrderItems> findByDate();
}
