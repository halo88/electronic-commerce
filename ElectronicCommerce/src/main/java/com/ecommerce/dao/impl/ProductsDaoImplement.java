/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ecommerce.dao.impl;

import com.ecommerce.dao.ProductsDao;
import com.ecommerce.entities.Categories;
import com.ecommerce.entities.Manufactures;
import com.ecommerce.entities.Products;
import com.ecommerce.repository.CategoriesRepository;
import com.ecommerce.repository.ManufacturesRepository;
import com.ecommerce.repository.ProductsRepository;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

/**
 *
 * @author PC
 */
@Repository
public class ProductsDaoImplement implements ProductsDao {

    @Autowired
    private ProductsRepository productsRepository;
    @Autowired
    private ManufacturesRepository manuRepo;
    @Autowired
    private CategoriesRepository catRepo;

    @Override
    public Products insertAndUpdateProduct(Products product) {
        return productsRepository.save(product);
    }

    @Override
    public void delete(int id) {
        productsRepository.delete(id);
    }

    @Override
    public Products findProductsById(int id) {
        return productsRepository.findOne(id);
    }

    @Override
    public List<Products> findAllProducts() {
        return (List<Products>) productsRepository.findAll();
    }

    @Override
    public List<Products> findByNameProducts(String name) {
        return productsRepository.findByProductNameStartingWith(name);
    }

    @Override
    public List<Products> findByUnitPriceProducts(double price) {
        return null;
    }

    @Override
    public List<Products> findByProductName(String name) {
        return productsRepository.findByProductNameContaining(name);
    }

    @Override
    public List<Manufactures> findByManufactureName(String name) {
        return manuRepo.findByManufactureNameContaining(name);
    }

    @Override
    public List<Categories> findByCategoryName(String name) {
        return catRepo.findByCategoryNameContaining(name);
    }

}
