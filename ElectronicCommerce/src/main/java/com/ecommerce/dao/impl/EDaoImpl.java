/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ecommerce.dao.impl;

import com.ecommerce.dao.EDao;
import com.ecommerce.entities.GroupAuthorities;
import com.ecommerce.entities.UserDetails;
import com.ecommerce.entities.Users;
import com.ecommerce.repository.AuthoritiesRepository;
import com.ecommerce.repository.GroupAuthoritiesRepository;
import com.ecommerce.repository.UserDetailsRepository;
import com.ecommerce.repository.UsersRepository;
import java.sql.Date;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

@Repository
public class EDaoImpl implements EDao {

    @Autowired
    private UsersRepository usersRepo;
    @Autowired
    private GroupAuthoritiesRepository groupAuthRepo;
    @Autowired
    private AuthoritiesRepository authRepo;
    @Autowired
    private UserDetailsRepository userDetailsRepo;

    @Override
    public boolean insertUpdateUsers(Users users) {
        return usersRepo.save(users) != null;
    }

    @Override
    public List<Users> searchUsersByUsername(String search) {
        return usersRepo.findByUsernameContaining(search);
    }

    @Override
    public List<Users> searchUsersByEnabled(String search) {
        return usersRepo.findByEnabled(search);
    }

    @Override
    public List<UserDetails> searchUserDetailsByRealName(String search) {
        return userDetailsRepo.findByFirstnameContainingOrLastnameContaining(search, search);
    }

    @Override
    public List<UserDetails> searchUserDetailsByPhone(String search) {
        return userDetailsRepo.findByPhoneContaining(search);
    }

    @Override
    public List<UserDetails> searchUserDetailsByDob(Date search) {
        return userDetailsRepo.findByDob(search);
    }

    @Override
    public GroupAuthorities searchGroupAuthoritiesByRole(String search) {
        if (groupAuthRepo.findByAuthorityRoleContaining(search).size() == 0) {
            return null;
        } else {
            return groupAuthRepo.findByAuthorityRoleContaining(search).get(0);
        }
    }

}
