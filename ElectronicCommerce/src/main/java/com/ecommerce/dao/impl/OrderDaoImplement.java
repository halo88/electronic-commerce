/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ecommerce.dao.impl;

import com.ecommerce.dao.OrderDao;
import com.ecommerce.entities.Orders;
import com.ecommerce.repository.OrdersRepository;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

/**
 *
 * @author PC
 */
@Repository
public class OrderDaoImplement implements OrderDao{
    
    @Autowired
    private OrdersRepository ordersRepository;

    @Override
    public Orders insertAndUpdateOrder(Orders orders) {
        return ordersRepository.save(orders);
    }

    @Override
    public void delete(int id) {
    }

    @Override
    public Orders findProductsById(int id) {
        return null;
    }

    @Override
    public List<Orders> findAllOrder() {
        return null;
    }

    @Override
    public List<Orders> findByNameUser() {
        return null;
    }

    @Override
    public List<Orders> findByDate() {
        return null;
    }
    
}
