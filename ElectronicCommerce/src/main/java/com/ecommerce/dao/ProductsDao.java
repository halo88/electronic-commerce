/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ecommerce.dao;

import com.ecommerce.entities.Categories;
import com.ecommerce.entities.Manufactures;
import com.ecommerce.entities.Products;
import java.util.List;

/**
 *
 * @author PC
 */
public interface ProductsDao {

    public Products insertAndUpdateProduct(Products products);

    public void delete(int id);

    public Products findProductsById(int id);

    public List<Products> findAllProducts();

    public List<Products> findByNameProducts(String name);

    public List<Products> findByUnitPriceProducts(double price);

    public List<Products> findByProductName(String name);

    public List<Manufactures> findByManufactureName(String name);

    public List<Categories> findByCategoryName(String name);
}
