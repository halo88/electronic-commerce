/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ecommerce.dao;

import com.ecommerce.entities.GroupAuthorities;
import com.ecommerce.entities.UserDetails;
import com.ecommerce.entities.Users;
import java.sql.Date;
import java.util.List;

/**
 *
 * @author hell_lock
 */
public interface EDao {

    public boolean insertUpdateUsers(Users users);

    public List<Users> searchUsersByUsername(String search);

    public List<Users> searchUsersByEnabled(String search);

    public List<UserDetails> searchUserDetailsByRealName(String search);

    public List<UserDetails> searchUserDetailsByPhone(String search);

    public List<UserDetails> searchUserDetailsByDob(Date search);

    public GroupAuthorities searchGroupAuthoritiesByRole(String search);

}
