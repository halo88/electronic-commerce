/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ecommerce.objects;

import com.ecommerce.entities.Manufactures;
import com.ecommerce.entities.Categories;
import java.util.List;

/**
 *
 * @author hell_lock
 */
public class FormListBinding {

    private List<Manufactures> listManu;
    private List<Categories> listTypes;

    public FormListBinding() {
    }

    public List<Manufactures> getListManu() {
        return listManu;
    }

    public void setListManu(List<Manufactures> listManu) {
        this.listManu = listManu;
    }

    public List<Categories> getListTypes() {
        return listTypes;
    }

    public void setListTypes(List<Categories> listTypes) {
        this.listTypes = listTypes;
    }
}
