-- phpMyAdmin SQL Dump
-- version 4.6.5.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jun 20, 2017 at 07:47 AM
-- Server version: 10.1.21-MariaDB
-- PHP Version: 7.1.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `ecommerce`
--

-- --------------------------------------------------------

--
-- Table structure for table `authorities`
--

CREATE TABLE `authorities` (
  `username` varchar(50) COLLATE utf8_vietnamese_ci NOT NULL,
  `authority` varchar(50) COLLATE utf8_vietnamese_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_vietnamese_ci;

--
-- Dumping data for table `authorities`
--

INSERT INTO `authorities` (`username`, `authority`) VALUES
('123', 'ROLE_ADMIN'),
('halo', 'ROLE_USER'),
('qqq', 'ROLE_USER'),
('test', 'ROLE_USER'),
('testadmin', 'ROLE_ADMIN'),
('testadmindsfa', 'ROLE_USER'),
('testadminf', 'ROLE_USER'),
('testadminn', 'ROLE_USER'),
('testreg', 'ROLE_USER'),
('testseller', 'ROLE_SELLER'),
('testUser', 'ROLE_USER'),
('zzz', 'ROLE_USER');

-- --------------------------------------------------------

--
-- Table structure for table `categories`
--

CREATE TABLE `categories` (
  `categoryID` int(11) NOT NULL,
  `categoryName` varchar(50) COLLATE utf8_vietnamese_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_vietnamese_ci;

--
-- Dumping data for table `categories`
--

INSERT INTO `categories` (`categoryID`, `categoryName`) VALUES
(1, 'Electronic'),
(2, 'Phonea'),
(3, 'Laptop'),
(4, 'Tablet'),
(6, 'Phablet'),
(7, 'Other');

-- --------------------------------------------------------

--
-- Table structure for table `group_authorities`
--

CREATE TABLE `group_authorities` (
  `authority_role` varchar(50) COLLATE utf8_vietnamese_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_vietnamese_ci;

--
-- Dumping data for table `group_authorities`
--

INSERT INTO `group_authorities` (`authority_role`) VALUES
('ROLE_ADMIN'),
('ROLE_SELLER'),
('ROLE_USER');

-- --------------------------------------------------------

--
-- Table structure for table `manufactures`
--

CREATE TABLE `manufactures` (
  `manufactureID` int(11) NOT NULL,
  `manufactureName` varchar(50) COLLATE utf8_vietnamese_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_vietnamese_ci;

--
-- Dumping data for table `manufactures`
--

INSERT INTO `manufactures` (`manufactureID`, `manufactureName`) VALUES
(1, 'Nokiaaa'),
(2, 'Acer'),
(3, 'Asus'),
(4, 'Sony'),
(9, 'Samsung'),
(10, 'Other'),
(12, 'Motorola');

-- --------------------------------------------------------

--
-- Table structure for table `orders`
--

CREATE TABLE `orders` (
  `orderID` int(11) NOT NULL,
  `username` varchar(50) COLLATE utf8_vietnamese_ci NOT NULL,
  `orderdate` date NOT NULL,
  `status` varchar(20) COLLATE utf8_vietnamese_ci DEFAULT NULL,
  `totalprice` double DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_vietnamese_ci;

--
-- Dumping data for table `orders`
--

INSERT INTO `orders` (`orderID`, `username`, `orderdate`, `status`, `totalprice`) VALUES
(1, 'testadmin', '2017-06-15', 'Delivered', 1570),
(2, 'testadmin', '2017-06-15', 'Delivered', 1200),
(3, 'testUser', '2017-06-18', '0', 1270);

-- --------------------------------------------------------

--
-- Table structure for table `order_items`
--

CREATE TABLE `order_items` (
  `id` int(11) NOT NULL,
  `orderID` int(11) NOT NULL,
  `productID` int(11) NOT NULL,
  `quantity` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_vietnamese_ci;

--
-- Dumping data for table `order_items`
--

INSERT INTO `order_items` (`id`, `orderID`, `productID`, `quantity`) VALUES
(1, 1, 12, 1),
(2, 2, 9, 4),
(3, 1, 7, 2),
(4, 3, 10, 1),
(5, 3, 12, 1);

-- --------------------------------------------------------

--
-- Table structure for table `productdescription`
--

CREATE TABLE `productdescription` (
  `productID` int(11) NOT NULL,
  `cpu` varchar(100) COLLATE utf8_vietnamese_ci DEFAULT NULL,
  `ram` varchar(100) COLLATE utf8_vietnamese_ci DEFAULT NULL,
  `chipset` varchar(100) COLLATE utf8_vietnamese_ci DEFAULT NULL,
  `hdd` varchar(100) COLLATE utf8_vietnamese_ci DEFAULT NULL,
  `optical` varchar(100) COLLATE utf8_vietnamese_ci DEFAULT NULL,
  `display` varchar(100) COLLATE utf8_vietnamese_ci DEFAULT NULL,
  `graphic` varchar(100) COLLATE utf8_vietnamese_ci DEFAULT NULL,
  `network` varchar(100) COLLATE utf8_vietnamese_ci DEFAULT NULL,
  `weight` varchar(100) COLLATE utf8_vietnamese_ci DEFAULT NULL,
  `other` varchar(500) COLLATE utf8_vietnamese_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_vietnamese_ci;

--
-- Dumping data for table `productdescription`
--

INSERT INTO `productdescription` (`productID`, `cpu`, `ram`, `chipset`, `hdd`, `optical`, `display`, `graphic`, `network`, `weight`, `other`) VALUES
(6, 'Intel core i5', '4GB DDR3L', 'Intel 8 Series Express', '1 TB', 'Bluetooth', '15.6 LED HD (1366 x 768) 16:9 Gloss', 'NVIDIA Geforce 920MX (2GB)', 'Wireless 802.11 b/g/n', '4kg', ''),
(7, 'Intel core i3', '4GB DDR3L', 'Intel 8 Series Express', '1 TB', 'DVDRW', '15.6 LED HD (1366 x 768) 16:9 Gloss', 'NVIDIA Geforce 920MX (2GB)', 'Wireless 802.11 b/g/n', '4kg', ''),
(8, 'Intel core i5', '4GB DDR3L', 'Intel 8 Series Express', '1 TB', 'DVDRW', '15.6 LED HD (1366 x 768) 16:9 Gloss', 'NVIDIA Geforce 920MX (2GB)', 'Wireless 802.11 b/g/n', '4kg', NULL),
(9, 'Intel core i5', '4GB DDR3L', 'Intel 8 Series Express', '1 TB', 'DVDRW', '15.6 LED HD (1366 x 768) 16:9 Gloss', 'NVIDIA Geforce 920MX (2GB)', 'Wireless 802.11 b/g/n', '4kg', NULL),
(10, 'Intel core i5', '4GB DDR3L', 'Intel 8 Series Express', '1 TB', 'DVDRW', '15.6 LED HD (1366 x 768) 16:9 Gloss', 'NVIDIA Geforce 920MX (2GB)', 'Wireless 802.11 b/g/n', '4kg', NULL),
(11, 'Intel core i5', '4GB DDR3L', 'Intel 8 Series Express', '1 TB', 'DVDRW', '15.6 LED HD (1366 x 768) 16:9 Gloss', 'NVIDIA Geforce 920MX (2GB)', 'Wireless 802.11 b/g/n', '4kg', NULL),
(12, 'Intel core i5', '4GB DDR3L', 'Intel 8 Series Express', '1 TB', 'DVDRW', '15.6 LED HD (1366 x 768) 16:9 Gloss', 'NVIDIA Geforce 920MX (2GB)', 'Wireless 802.11 b/g/n', '4kg', NULL),
(13, 'Intel core i5', '4GB DDR3L', 'Intel 8 Series Express', '1 TB', 'DVDRW', '15.6 LED HD (1366 x 768) 16:9 Gloss', 'NVIDIA Geforce 920MX (2GB)', 'Wireless 802.11 b/g/n', '4kg', NULL),
(14, 'Intel core i5', '4GB DDR3L', 'Intel 8 Series Express', '1 TB', 'DVDRW', '15.6 LED HD (1366 x 768) 16:9 Gloss', 'NVIDIA Geforce 920MX (2GB)', 'Wireless 802.11 b/g/n', '4kg', NULL),
(15, 'Intel core i5', '4GB DDR3L', 'Intel 8 Series Express', '1 TB', 'DVDRW', '15.6 LED HD (1366 x 768) 16:9 Gloss', 'NVIDIA Geforce 920MX (2GB)', 'Wireless 802.11 b/g/n', '4kg', NULL),
(16, 'Intel core i5', '4GB DDR3L', 'Intel 8 Series Express', '1 TB', 'DVDRW', '15.6 LED HD (1366 x 768) 16:9 Gloss', 'NVIDIA Geforce 920MX (2GB)', 'Wireless 802.11 b/g/n', '4kg', NULL),
(17, 'Intel core i5', '4GB DDR3L', 'Intel 8 Series Express', '1 TB', 'DVDRW', '15.6 LED HD (1366 x 768) 16:9 Gloss', 'NVIDIA Geforce 920MX (2GB)', 'Wireless 802.11 b/g/n', '4kg', ''),
(18, 'Intel core i3', '', '', '', '', '', '', '', '', ''),
(19, '', '', '', '500 GB', '', '', '', '', '', '');

-- --------------------------------------------------------

--
-- Table structure for table `products`
--

CREATE TABLE `products` (
  `productID` int(11) NOT NULL,
  `productName` varchar(200) COLLATE utf8_vietnamese_ci NOT NULL,
  `adddate` date NOT NULL,
  `description` text COLLATE utf8_vietnamese_ci,
  `unitPrice` double NOT NULL,
  `warranty` varchar(50) COLLATE utf8_vietnamese_ci NOT NULL,
  `stock` int(11) NOT NULL,
  `categoryID` int(11) NOT NULL,
  `manufactureID` int(11) NOT NULL,
  `promotionID` int(11) NOT NULL DEFAULT '1',
  `imageurl` varchar(100) COLLATE utf8_vietnamese_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_vietnamese_ci;

--
-- Dumping data for table `products`
--

INSERT INTO `products` (`productID`, `productName`, `adddate`, `description`, `unitPrice`, `warranty`, `stock`, `categoryID`, `manufactureID`, `promotionID`, `imageurl`) VALUES
(6, 'NOTEBOOK ACER AS ES1-131.C4GV-NX.MYKSV.001', '2017-05-25', NULL, 500, '1 year', 100, 1, 1, 8, '7.jpg'),
(7, 'NOTEBOOK ACER AS ES1-131.C0GP-NX.G17SV.001', '2017-05-25', NULL, 450, '1 year', 10, 1, 1, 3, '8.jpg'),
(8, 'NOTEBOOK LENOVO IP 110 14 - 80T60055VN', '2017-05-25', 'Intel Pentium N3710 (4*1.6GHz, 2MB L2 Cache, 14nm, 6W), Intel 8 Series Express, 4GB DDR3L BUS 1600Mhz, 500GB SATA, NO, 15.6 LED HD (1366 x 768) 16:9 Gloss, Intel HD GPU Graphics, Wireless 802.11 b/g/n, NIC 10/100 Mbps, Card Reader 5.1, Webcam 1.3M, Microphone, Bluetooth 4.0, HDMI, USB 3.0, 2.1kg, Pin 4 Cell, DOS, Màu Đen,', 470, '1 year', 10, 3, 1, 1, '9.jpg'),
(9, 'NOTEBOOK ASUS E502NA - GO035', '2017-05-25', 'Intel Celeron N3350(2*1.1GHz, 2MB Cache L2, 14nm, 6W), 2GB DDR3L BUS 1600Mhz, 500 GB SATA, NO, 15.6 (1366x768) LED HD, Intel HD Graphics, 802.11b/g/n 10/100Mbps, Card reader (Micro SD), Webcam 0.3, Microphone, USB 2.0, USB 3.0, HDMI, 1.6Kg, Pin 2 Cell 30 Wh, DOS,, Pin & Adapter báº£o hÃ nh 12 thÃ¡ng, MÃ u Xanh USB 3.1 Type C, USB 3.0, USB 2.0, HDMI', 300, '1 year', 10, 3, 1, 4, '10.jpg'),
(10, 'NOTEBOOK ACER AS ES1-533-C5TS', '2017-05-25', 'Intel Celerone Processor N3350, 4GB DDR3L BUS 1600Mhz, 500GB SATA, DVDRW, 15.6 LED HD (1366 x 768) 16:9 Gloss, Intel HD GPU Graphics, Wireless 802.11b/g/n Half Mini Card, Card Reader 5.1, Webcam HD, Microphone, Bluetooth, HDMI, USB 3.0, 2.3Kg, Pin 4cell, DOS, Black,', 600, '1 year', 10, 3, 1, 1, '11.jpg'),
(11, 'NOTEBOOK HP 14 -AM060TU-X1H09PA', '2017-05-25', 'Intel Pentium N3710(2*1.7GHz, 2MB Cache, 14nm, 6W), 4GB DDR3L BUS 1600Mhz, 500GB SATA, DVDSM, 14 LED HD (1366 x 768), Intel HD Graphics 5500, Wireless realtek 802.11 b/g/n, NIC 1 Gbps, Card Reader 5.1, Webcam 1.3M, Microphone, Bluetooth 4.0, HDMI, 2.0 Kg, Pin 4 Cell, DOS, MÃ u Báº¡c', 700, '1 year', 10, 3, 2, 1, '12.jpg'),
(12, 'NOTEBOOK LENOVO IP 110 15 80T700AYVN/DOS)', '2017-05-25', 'Intel Pentium N3710 (4*1.6GHz, 2MB L2 Cache, 14nm, 6W), Intel 8 Series Express, 4GB DDR3L BUS 1600Mhz, 500GB SATA, NO, 15.6 LED HD (1366 x 768) 16:9 Gloss, Intel HD GPU Graphics, Wireless 802.11 b/g/n, NIC 10/100 Mbps, Card Reader 5.1, Webcam 1.3M, Microphone, Bluetooth 4.0, HDMI, USB 3.0, 2.1kg, Pin 4 Cell, DOS', 670, '1 year', 10, 3, 1, 1, '13.jpg'),
(13, 'NOTEBOOK ACER ES1-533-P6ZS', '2017-05-25', 'Intel Pentium N4200 (4*1.7GHz, 2MB L2 Cache, 14nm, 6W), 4GB DDR3L BUS 1600Mhz, 500GB SATA, NO, 14 LED HD (1366 x 768) 16:9 Gloss, Intel HD GPU Graphics 4000, Wireless 802.11 b/g/n, NIC 10/100 Mbps, Card Reader 5.1, Webcam 1.3M, Microphone, Bluetooth 4.0, HDMI, USB 3.0, 1.9kg, Pin 4 Cell, DOS, MÃ u Ä?en,', 780, '1 year', 10, 3, 1, 1, '14.jpg'),
(14, 'NOTEBOOK HP 15 -AY071TU', '2017-05-25', 'Intel Pentium N3710(2*1.7GHz, 2MB Cache, 14nm, 6W), 4GB DDR3L BUS 1600Mhz, 500GB SATA, DVDSM, 15.6 LED HD (1366 x 768), Intel HD Graphics 5500, Wireless realtek 802.11 b/g/n, NIC 1 Gbps, Card Reader 5.1, Webcam 1.3M, Microphone, Bluetooth 4.0, HDMI, 2.2 Kg, Pin 4 Cell, DOS,', 560, '1 year', 10, 3, 1, 1, '15.jpg'),
(15, 'NOTEBOOK LENOVO IP 310 14ISK 80SL005TVN', '2017-05-25', 'Intel Core i3 6100U(4*2.3Ghz, 3MB cache,14nm, 15W), Intel 8 Series Express, 4 GB DDR4 bus 2133MHz, 1 Terabyte (1.000 GB), NO, 14 Full HD (1920 X 1080), Intel HD Graphics 520, Wireless 802.11 b/g/n, Card Reader , Webcam HD, Microphone, Bluetooth, HDMI, VGA, USB 3.0, 2.0Kg, Pin 2Cell 30WWH, Dos, MÃ u Ä?en, Thiáº¿t káº¿ má»?i nÄ?m 2016, 1 x USB 3.0, 2 x USB 2.0, 4-in-1 card reader (SD, SDHC, SDXC, MMC), HDMI, VGA', 700, '1 year', 10, 3, 1, 1, '16.jpg'),
(16, 'NOTEBOOK HP 15 AY538TU - 1AC62PA', '2017-05-25', 'Intel Core i3 6006U(4*2.0Ghz, 3MB cache,14nm, 15W), 4GB DDR4, 500GB SATA, DVDSM, 15.6 LED HD (1366 x 768), Intel HD Graphics 5500, Wireless realtek 802.11 b/g/n, NIC 1 Gbps, Card Reader 5.1, Webcam 1.3M, Microphone, Bluetooth 4.0, HDMI, 2.2 Kg, Pin 4 Cell, DOS,', 750, '1 year', 10, 3, 1, 1, '17.jpg'),
(17, 'NOTEBOOK ASUS X455LA - WX443D', '2017-05-25', NULL, 550, '1 year', 10, 1, 1, 9, '18.jpg'),
(18, 'abc', '2017-06-17', NULL, 5000000, '1 year', 100, 1, 1, 1, '29.jpg'),
(19, 'abc2', '2017-06-20', NULL, 500, '1 year', 100, 3, 1, 10, '31.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `promotions`
--

CREATE TABLE `promotions` (
  `promoID` int(11) NOT NULL,
  `promoName` varchar(50) COLLATE utf8_vietnamese_ci DEFAULT '',
  `promoDesc` int(11) NOT NULL DEFAULT '0',
  `startdate` date DEFAULT NULL,
  `enddate` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_vietnamese_ci;

--
-- Dumping data for table `promotions`
--

INSERT INTO `promotions` (`promoID`, `promoName`, `promoDesc`, `startdate`, `enddate`) VALUES
(1, 'no promotion', 0, '2017-05-01', '2017-05-31'),
(2, NULL, 0, NULL, NULL),
(3, '', 10, '2017-06-05', '2017-06-30'),
(4, '', 20, '2017-06-05', '2017-06-23'),
(5, NULL, 0, '2017-10-30', '2017-12-30'),
(6, NULL, 0, '2017-06-30', '2017-06-02'),
(7, NULL, 0, '2017-06-18', '2017-06-04'),
(8, NULL, 0, '2017-06-27', '2017-06-30'),
(9, NULL, 0, NULL, NULL),
(10, NULL, 0, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `userdetails`
--

CREATE TABLE `userdetails` (
  `username` varchar(50) COLLATE utf8_vietnamese_ci NOT NULL,
  `first_name` varchar(50) COLLATE utf8_vietnamese_ci DEFAULT NULL,
  `last_name` varchar(50) COLLATE utf8_vietnamese_ci DEFAULT NULL,
  `dateofbirth` date DEFAULT NULL,
  `address` text COLLATE utf8_vietnamese_ci,
  `phone` varchar(20) COLLATE utf8_vietnamese_ci DEFAULT NULL,
  `email` varchar(50) COLLATE utf8_vietnamese_ci DEFAULT NULL,
  `gender` varchar(10) COLLATE utf8_vietnamese_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_vietnamese_ci;

--
-- Dumping data for table `userdetails`
--

INSERT INTO `userdetails` (`username`, `first_name`, `last_name`, `dateofbirth`, `address`, `phone`, `email`, `gender`) VALUES
('123', '', '', NULL, '', '', NULL, NULL),
('qqq', NULL, NULL, NULL, 'dfsgdsf', NULL, 'halo2@gmail.com', NULL),
('testadmin', 'admin', 'adminlast', '1996-12-12', '100 abc Street, District xyz, qwe City', '123456', 'halo3@gmail.com', 'Mr'),
('testadmindsfa', NULL, NULL, NULL, 'asd', NULL, 'halo1@gmail.com', NULL),
('testadminf', NULL, NULL, NULL, 'aesdf', NULL, 'halo@gmail.com', NULL),
('testadminn', NULL, NULL, NULL, 'dsfsd', NULL, 'efw@gas', NULL),
('testreg', NULL, NULL, NULL, 'sdfsd', NULL, 'halo88801@gmail.com', NULL),
('testseller', 'testtt', 'seller', '2017-05-11', 'da nang', '1234565', '', 'Mr'),
('testUser', 'asd', 'aass', '2017-05-08', 'sdfvbg', '12356', NULL, 'Mr'),
('zzz', NULL, NULL, NULL, 'sdafsf', NULL, 'halo41@gmail.com', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `username` varchar(50) COLLATE utf8_vietnamese_ci NOT NULL,
  `password` varchar(50) COLLATE utf8_vietnamese_ci NOT NULL,
  `enabled` tinyint(1) NOT NULL DEFAULT '1',
  `confirmcode` varchar(100) COLLATE utf8_vietnamese_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_vietnamese_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`username`, `password`, `enabled`, `confirmcode`) VALUES
('123', '123', 1, NULL),
('234', '234', 1, NULL),
('halo', '123', 0, '3194940-1878880687'),
('qqq', '123', 0, '&112209-1937214778'),
('test', '123', 0, '3556498-1933803518'),
('testadmin', '123', 1, ''),
('testadmindsfa', '123', 1, NULL),
('testadminf', '123', 0, '2113631753-1935752943'),
('testadminn', '123', 0, '2113631761-1933267045'),
('testreg', '123456', 1, '-975180754-1389953415'),
('testseller', '123', 1, ''),
('testUser', '123', 1, ''),
('zzz', '123', 0, 'halo88801@gmail.com&121146-1937418729');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `authorities`
--
ALTER TABLE `authorities`
  ADD UNIQUE KEY `ix_auth_username` (`username`,`authority`),
  ADD KEY `FKpe26tp2xwv0hi9tqh1a9h6mpg` (`authority`);

--
-- Indexes for table `categories`
--
ALTER TABLE `categories`
  ADD PRIMARY KEY (`categoryID`);

--
-- Indexes for table `group_authorities`
--
ALTER TABLE `group_authorities`
  ADD PRIMARY KEY (`authority_role`);

--
-- Indexes for table `manufactures`
--
ALTER TABLE `manufactures`
  ADD PRIMARY KEY (`manufactureID`);

--
-- Indexes for table `orders`
--
ALTER TABLE `orders`
  ADD PRIMARY KEY (`orderID`),
  ADD KEY `username` (`username`);

--
-- Indexes for table `order_items`
--
ALTER TABLE `order_items`
  ADD PRIMARY KEY (`id`),
  ADD KEY `productID` (`productID`),
  ADD KEY `orderID` (`orderID`);

--
-- Indexes for table `productdescription`
--
ALTER TABLE `productdescription`
  ADD PRIMARY KEY (`productID`),
  ADD KEY `productID` (`productID`);

--
-- Indexes for table `products`
--
ALTER TABLE `products`
  ADD PRIMARY KEY (`productID`),
  ADD KEY `typeID` (`categoryID`),
  ADD KEY `manufactureID` (`manufactureID`),
  ADD KEY `promotionID` (`promotionID`);

--
-- Indexes for table `promotions`
--
ALTER TABLE `promotions`
  ADD PRIMARY KEY (`promoID`);

--
-- Indexes for table `userdetails`
--
ALTER TABLE `userdetails`
  ADD PRIMARY KEY (`username`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`username`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `categories`
--
ALTER TABLE `categories`
  MODIFY `categoryID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `manufactures`
--
ALTER TABLE `manufactures`
  MODIFY `manufactureID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;
--
-- AUTO_INCREMENT for table `orders`
--
ALTER TABLE `orders`
  MODIFY `orderID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `order_items`
--
ALTER TABLE `order_items`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `productdescription`
--
ALTER TABLE `productdescription`
  MODIFY `productID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;
--
-- AUTO_INCREMENT for table `products`
--
ALTER TABLE `products`
  MODIFY `productID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;
--
-- AUTO_INCREMENT for table `promotions`
--
ALTER TABLE `promotions`
  MODIFY `promoID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `authorities`
--
ALTER TABLE `authorities`
  ADD CONSTRAINT `FKpe26tp2xwv0hi9tqh1a9h6mpg` FOREIGN KEY (`authority`) REFERENCES `group_authorities` (`authority_role`),
  ADD CONSTRAINT `authorities_ibfk_1` FOREIGN KEY (`username`) REFERENCES `users` (`username`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_authorities_groupautho` FOREIGN KEY (`authority`) REFERENCES `group_authorities` (`authority_role`);

--
-- Constraints for table `orders`
--
ALTER TABLE `orders`
  ADD CONSTRAINT `FK5ul5qtyh6slfyq8i4jby7qecx` FOREIGN KEY (`username`) REFERENCES `users` (`username`),
  ADD CONSTRAINT `orders_ibfk_1` FOREIGN KEY (`username`) REFERENCES `users` (`username`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `order_items`
--
ALTER TABLE `order_items`
  ADD CONSTRAINT `FKmec45jmvys2e1638roc745ydb` FOREIGN KEY (`orderID`) REFERENCES `orders` (`orderID`),
  ADD CONSTRAINT `FKsdf7p197a1dlvfsya2xbvj1t7` FOREIGN KEY (`productID`) REFERENCES `products` (`productID`),
  ADD CONSTRAINT `order_items_ibfk_1` FOREIGN KEY (`productID`) REFERENCES `products` (`productID`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `order_items_ibfk_2` FOREIGN KEY (`orderID`) REFERENCES `orders` (`orderID`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `productdescription`
--
ALTER TABLE `productdescription`
  ADD CONSTRAINT `productdescription_ibfk_1` FOREIGN KEY (`productID`) REFERENCES `products` (`productID`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `products`
--
ALTER TABLE `products`
  ADD CONSTRAINT `FK8ana8fi9ys0uvvlics5jt3ou3` FOREIGN KEY (`promotionID`) REFERENCES `promotions` (`promoID`),
  ADD CONSTRAINT `FK979y2sbeujv82cgwu4oyrrx01` FOREIGN KEY (`manufactureID`) REFERENCES `manufactures` (`manufactureID`),
  ADD CONSTRAINT `FKew3j78rsewkvscurd15gb0ctx` FOREIGN KEY (`categoryID`) REFERENCES `categories` (`categoryID`),
  ADD CONSTRAINT `products_ibfk_2` FOREIGN KEY (`manufactureID`) REFERENCES `manufactures` (`manufactureID`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `products_ibfk_4` FOREIGN KEY (`categoryID`) REFERENCES `categories` (`categoryID`),
  ADD CONSTRAINT `products_ibfk_5` FOREIGN KEY (`promotionID`) REFERENCES `promotions` (`promoID`);

--
-- Constraints for table `userdetails`
--
ALTER TABLE `userdetails`
  ADD CONSTRAINT `userdetails_ibfk_1` FOREIGN KEY (`username`) REFERENCES `users` (`username`) ON DELETE CASCADE ON UPDATE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
